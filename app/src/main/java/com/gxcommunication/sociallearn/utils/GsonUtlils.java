package com.gxcommunication.sociallearn.utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/1/18.
 * 这个类主要的方法有:
 objectToJson-> String 把对象或数组转换成为 JSon格式的String
 jsonToBeanList-> Object 将 String 转换成 List<Bean>
 jsonToBean-> Object 将 String 转换成 Bean;
 json2b-> Object 将 String 转换成 Bean或者 List<Bean>,需要自行判断
 jsonToMap-> Map<?, ?>将 String 变成 Map
 */
public class GsonUtlils {
	/**
	 * 实现单例
	 */
	private static Gson gson = null;

	static {
		if (gson == null) {
			gson = new Gson();
		}
	}

	/**
	 * 隐藏默认的构造方法
	 */
	private GsonUtlils() {

	}

	/**
	 * 将对象转换成json格式
	 *
	 * @param ts
	 * @return
	 */
	public static String objectToJson(Object ts) {
		String jsonStr = null;
		if (gson != null) {
			jsonStr = gson.toJson(ts);
		}
		return jsonStr;
	}

	/**
	 * 返回cla 类型的list数组
	 *
	 * @param s
	 * @param cla
	 * @return
	 */
	public static <T extends Object> T jsonToBeanList(String s, Class<?> cla) {

		List<Object> ls = new ArrayList<>();
		JSONArray ss;
		try {
			ss = new JSONArray(s);
			for (int i = 0; i < ss.length(); i++) {
				String str = ss.getString(i);
				Object a = jsonToBean(str, cla);
				ls.add(a);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return (T) ls;
	}

	/**
	 * 将jsonStr转换成cl对象
	 *
	 * @param jsonStr
	 * @return
	 */
	public static <T extends Object> T jsonToBean(String jsonStr, Class<?> cl) {
		Object obj = null;
		if (gson != null) {
			if (!jsonStr.isEmpty())
				obj = gson.fromJson(jsonStr, cl);
		}
		return (T) obj;
	}

	/**
	 * 根据 给的 jsonStr 自动的生成对象或者数组.
	 * @param jsonStr
	 * @param classType
	 * @param <T>
	 * @return
	 */
	public static <T extends Object> T json2b(String jsonStr,  Class<?> classType) {


		if (jsonStr.trim().startsWith("[")) {
			return jsonToBeanList(jsonStr, classType);
		} else {
			return jsonToBean(jsonStr, classType);
		}


	}

	/**
	 * 将json格式转换成map对象
	 *
	 * @param jsonStr
	 * @return
	 */
	public static Map<?, ?> jsonToMap(String jsonStr) {
		Map<?, ?> objMap = null;
		if (gson != null) {
			java.lang.reflect.Type type = new com.google.gson.reflect.TypeToken<Map<?, ?>>() {
			}.getType();
			objMap = gson.fromJson(jsonStr, type);
		}
		return objMap;
	}

}
