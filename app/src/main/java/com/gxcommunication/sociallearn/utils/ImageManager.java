package com.gxcommunication.sociallearn.utils;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.gxcommunication.sociallearn.R;


/**
 * Created by jhz on 16/7/24.
 */
public class ImageManager {

	private Context mContext;
	private static ImageManager mImageManager;
	public static final String ANDROID_RESOURCE = "android.resource://";
	public static final String FOREWARD_SLASH = "/";
	public static final String HTTP_URL = "http://";
	// 将资源ID转为Uri
	public Uri resourceIdToUri(int resourceId) {
		return Uri.parse(ANDROID_RESOURCE + mContext.getPackageName() + FOREWARD_SLASH + resourceId);
	}

	public ImageManager(Context context) {
		this.mContext = context;
	}

	public static ImageManager getManager(Context context) {
		if (mImageManager == null) {
			mImageManager = new ImageManager(context.getApplicationContext());
		}
		return mImageManager;
	}


	/**
	 * 默认配置
	 *
	 * @return
	 */
	private RequestOptions getRequestOptionsNoDefault() {
		RequestOptions options = new RequestOptions()
				.centerCrop()
				.diskCacheStrategy(DiskCacheStrategy.NONE)
				.skipMemoryCache( true );
		return options;
	}

	private RequestOptions getRequestOptionsDefault() {
		RequestOptions options = new RequestOptions()
				.centerCrop()
				.placeholder(R.drawable.img_placeholder_loading)
				.error(R.drawable.img_placeholder_fail)
				.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
		return options;
	}

    private RequestOptions getRequestOptionsRound(int radius) {
        MultiTransformation transformation = new MultiTransformation(
                new CenterCrop(),
                new RoundedCorners(radius));

        RequestOptions options = new RequestOptions()
                .transform(transformation)
                .placeholder(R.mipmap.img_default)
                .error(R.mipmap.img_default_fail)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        return options;
    }

	/**
	 * 圆形图像
	 *
	 * @return
	 */
	private RequestOptions getRequestOptionsCircle() {
		RequestOptions options = new RequestOptions()
				.circleCrop()
				.placeholder(R.mipmap.img_default)
				.error(R.mipmap.img_default_fail)
				.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
		return options;
	}

	// 加载网络图片

	public void loadUrlImageNoDefault(String url, ImageView imageView) {
		Glide.with(mContext)
				.load(url)
				.apply(getRequestOptionsNoDefault())
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	public void loadUrlFitImage(String url, ImageView imageView) {
		RequestOptions options = new RequestOptions()
				.optionalFitCenter()
				.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
		Glide.with(mContext)
				.load(url)
				.apply(options)
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	public void loadUrlFitImage(String url,int defaultres, ImageView imageView) {
		RequestOptions options = new RequestOptions()
				.optionalFitCenter()
				.placeholder(defaultres)
				.error(defaultres)
				.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
		Glide.with(mContext)
				.load(url)
				.apply(options)
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	public void loadUrlImage(String url, ImageView imageView) {
		Glide.with(mContext)
				.load(url)
				.apply(getRequestOptionsDefault())
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	public void loadUrlImage(String url,int defaultres, ImageView imageView) {

		RequestOptions options = new RequestOptions()
				.centerCrop()
				.placeholder(defaultres)
				.error(defaultres)
				.diskCacheStrategy(DiskCacheStrategy.ALL);

		Glide.with(mContext)
				.load(url)
				.apply(options)
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	// 加载drawable图片
	public void loadResImage(int resId, ImageView imageView) {
		Glide.with(mContext)
				.load(resId)
				.apply(getRequestOptionsDefault())
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	public void loadResNoDefault(int resId, ImageView imageView) {
		Glide.with(mContext)
				.load(resId)
				.apply(getRequestOptionsNoDefault())
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	// 加载本地图片
	public void loadLocalImage(String path, ImageView imageView) {
		Glide.with(mContext)
				.load(path)
				.apply(getRequestOptionsDefault())
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	// 加载网络圆型图片
	public void loadCircleImage(String url, ImageView imageView) {
		Glide.with(mContext)
				.load(url)
				.apply(getRequestOptionsCircle())
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

	public void loadCircleImage(String url,int defaultres, ImageView imageView) {

		RequestOptions options = new RequestOptions()
				.circleCrop()
				.placeholder(defaultres)
				.error(defaultres)
				.diskCacheStrategy(DiskCacheStrategy.ALL);

		Glide.with(mContext)
				.load(url)
				.apply(options)
				.transition(new DrawableTransitionOptions())
				.into(imageView);
	}

    // 加载drawable圆型图片
    public void loadCircleResImage(int resId, ImageView imageView) {
        Glide.with(mContext)
                .load(resId)
				.apply(getRequestOptionsCircle())
				.transition(new DrawableTransitionOptions())
                .into(imageView);
    }

    public void loadRoundedImage(String url,int radius, ImageView imageView) {
        Glide.with(mContext)
                .load(url)
                .apply(getRequestOptionsRound(radius))
                .transition(new DrawableTransitionOptions())
                .into(imageView);
    }


    public void loadRoundedResImage(int resId,int radius, ImageView imageView) {
        Glide.with(mContext)
                .load(resId)
                .apply(getRequestOptionsRound(radius))
                .transition(new DrawableTransitionOptions())
                .into(imageView);
    }

}
