package com.gxcommunication.sociallearn.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gxcommunication.sociallearn.app.MyApplication;

/**
 * 描述 :
 * 备注:
 */
public class CommonSharePreferenceUtils {

    private static final String KEY_TOKEN = "key_token";
    private static final String USERS_UID = "uid";
    private static final String USERS_INFO = "user_info";
    private static final String USERS_STORE_ID = "user_store_id";
    private static final String USERS_JIM_NAME = "user_jim_id";

    private static final String IS_FIRST_OPEM_APP = "first_open";//是否第一次启动

    private static final String IS_FIRST_OPEM_USER_AGREEMENT = "first_open_user_agreement";//是否同意用户协议
    private static final String IS_CHANGE_USER = "IS_CANGE";//用于重新登录首页刷新
    private static final String IS_CHANGE_USER_PRODUCT_DETAIL = "IS_CANGE_PRODUCT_DETAIL";//用于重新登录商品详情页刷新

    private static final String SYSTEM_IP = "system_ip";//设置全局ip

    private static final String SYSTEM_THEME_INFO = "system_theme_info";//系统主题色，用于设置全局的一键置灰
    private static final String SYSTEM_MODE = "system_mode";
    public static final String SYSTEM_MODE_ALL = "system_all";
    public static final String WEIXIN_APP_REQ_TYPE = "weixin_type";//调起微信客户端获取用户信息的用途
    public static final String WEIXIN_APP_REQ_TYPE_BUND = "weixin_type_bund";//调起微信客户端获取用户信息的用途  用户 绑定


    public static SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getInstance());
    }

    /**
     * 检查是否登录
     *
     * @return
     */
    public static boolean isLogin() {

        if (getUserID().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 保存用户信息
     *
     * @param us
     */
//	public static void savaUserBean(UserInfoBean.ResultBean us) {
//		if (us == null){
//			ToastUtils.showShort("保存用户信息失败");
//			return;
//		}
//		getSharedPreferences().edit().putString(USERS_INFO, new Gson().toJson(us)).commit();
//		getSharedPreferences().edit().putString(USERS_UID, us.getId() + "").commit();
//
//	}

    /**
     * 获取保存的用户信息
     * @return
     */
//	public static UserInfoBean.ResultBean getUserBean() {
//		return new Gson().fromJson(getSharedPreferences().getString(USERS_INFO, null), UserInfoBean.ResultBean.class);
//	}

    /**
     * 清除用户信息
     */
    public static void clearUser() {
        getSharedPreferences().edit().remove(USERS_INFO).commit();
        getSharedPreferences().edit().remove(USERS_UID).commit();
        getSharedPreferences().edit().remove(KEY_TOKEN).commit();
    }

    /**
     * 用户店铺ID
     */
    public static void setUserStoreId(String token) {
        getSharedPreferences().edit().putString(USERS_STORE_ID, token).commit();
    }

    public static String getUserStoreId() {
        return getSharedPreferences().getString(USERS_STORE_ID, "");
    }


    /**
     * 用户极光IM userName
     */
    public static void setUserSJimName(String name) {
        getSharedPreferences().edit().putString(USERS_JIM_NAME, name).commit();
    }

    public static String getUsersJimName() {
        return getSharedPreferences().getString(USERS_JIM_NAME, "");
    }


    /**
     * 设置用户令牌，检测登录唯一性
     *
     * @param token
     */
    public static void setUserToken(String token) {
        getSharedPreferences().edit().putString(KEY_TOKEN, token).commit();
    }

    public static String getUserToken() {
        return getSharedPreferences().getString(KEY_TOKEN, "");
    }


    public static void setUserID(String id) {
        getSharedPreferences().edit().putString(USERS_UID, id).commit();
    }

    public static String getUserID() {
        return getSharedPreferences().getString(USERS_UID, "");//282f294a3dc3db012bc2bf9182870898
    }


    public static boolean getFirstOpenApp() {
        return getSharedPreferences().getBoolean(IS_FIRST_OPEM_APP, true);
    }

    public static void setFirstOpenAppToFalse() {
        getSharedPreferences().edit().putBoolean(IS_FIRST_OPEM_APP, false).commit();
    }

    public static boolean getFirstOpenUserAgreement() {
        return getSharedPreferences().getBoolean(IS_FIRST_OPEM_USER_AGREEMENT, true);
    }

    public static void setFirstOpenUserAgreementToFalse() {
        getSharedPreferences().edit().putBoolean(IS_FIRST_OPEM_USER_AGREEMENT, false).commit();
    }


    public static boolean isChangeUser() {
        return getSharedPreferences().getBoolean(IS_CHANGE_USER, false);
    }

    public static void setIsChangeUser(boolean isChangeUser) {
        getSharedPreferences().edit().putBoolean(IS_CHANGE_USER, isChangeUser).commit();
    }

    public static boolean isChangeUserProduct() {
        return getSharedPreferences().getBoolean(IS_CHANGE_USER_PRODUCT_DETAIL, false);
    }

    public static void setIsChangeUserProduct(boolean isChangeUser) {
        getSharedPreferences().edit().putBoolean(IS_CHANGE_USER_PRODUCT_DETAIL, isChangeUser).commit();
    }


    public static void setSystemMode(String mode) {
        getSharedPreferences().edit().putString(SYSTEM_MODE, mode).commit();
    }

    public static String getSystemMode() {
        return getSharedPreferences().getString(SYSTEM_MODE, SYSTEM_MODE_ALL);
    }

    public static void setSystemThemeInfo(boolean mode) {
        getSharedPreferences().edit().putBoolean(SYSTEM_THEME_INFO, mode).commit();
    }

    public static boolean getSystemThemeInfo() {
        return getSharedPreferences().getBoolean(SYSTEM_THEME_INFO, false);
    }

    public static void setWeixinAppReqType(String type) {
        getSharedPreferences().edit().putString(WEIXIN_APP_REQ_TYPE, type).commit();
    }

    public static String getWeixinAppReqType() {
        return getSharedPreferences().getString(WEIXIN_APP_REQ_TYPE, "");
    }

    public static void setSystemIp(String ip) {
        getSharedPreferences().edit().putString(SYSTEM_IP, ip).commit();
    }

    public static String getSystemIp() {
        return getSharedPreferences().getString(SYSTEM_IP, "");
    }


}
