package com.gxcommunication.sociallearn.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/*
 * @author Msquirrel
 */
public class DateUtils {

    private static SimpleDateFormat sf = null;

    /* 获取系统时间 格式为："yyyy/MM/dd " */
    public static String getCurrentDate() {
        Date d = new Date();
        sf = new SimpleDateFormat("yyyy-MM-dd");
        return sf.format(d);
    }

    public static String getCurrentDetialDate() {
        Date d = new Date();
        sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sf.format(d);
    }

    /* 时间戳转换成字符窜 */
    public static String getDateToString(long time) {
        Date d = new Date(time);
        sf = new SimpleDateFormat("yyyy-MM-dd");
        return sf.format(d);
    }

    /* 时间戳转换成字符窜 */
    public static String getDateToDetialTime(long time) {
        Date d = new Date(time);
        sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sf.format(d);
    }

    /* 将字符串转为时间戳 */
    public static long getStringToDate(String time) {
        sf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        try {
            date = sf.parse(time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date.getTime();
    }

    /* 将字符串转为时间戳 */
    public static long getStringToDateDetail(String time) {
        sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        try {
            date = sf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    /**
     * 以友好的方式显示时间
     * 
     * @param sdate
     * @return
     */
    public static String friendlyTime(String sdate) {
        Date date = new Date();
        try {
            sf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            date = sf.parse(sdate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String ftime = "";
        Calendar cal = Calendar.getInstance();
        // 判断是否是相同
        String curDate = getDateToString(cal.getTimeInMillis());
        String paramDate = getDateToString(date.getTime());
        if (curDate.equals(paramDate)) {
            int hour = (int) ((cal.getTimeInMillis() - date.getTime()) / 3600000);
            if (hour == 0) {
                ftime = Math.max(
                        (cal.getTimeInMillis() - date.getTime()) / 60000, 1)
                        + "分钟";
            } else {
                ftime = hour + "小时";
            }
            return ftime;
        }

        long lt = date.getTime() / 86400000;
        long ct = cal.getTimeInMillis() / 86400000;
        int days = (int) (ct - lt);
        if (days == 0) {
            int hour = (int) ((cal.getTimeInMillis() - date.getTime()) / 3600000);
            if (hour == 0)
                ftime = Math.max(
                        (cal.getTimeInMillis() - date.getTime()) / 60000, 1)
                        + "分钟";
            else
                ftime = hour + "小时";
        } else if (days == 1) {
            String timeStr = date.getHours() + ":" + date.getMinutes();
            ftime = "昨天 " + timeStr;
        } else if (days == 2) {
            String timeStr = date.getHours() + ":" + date.getMinutes();
            ftime = "前天 " + timeStr;
        } else if (days > 2 && days <= 10) {
            String timeStr = date.getHours() + ":" + date.getMinutes();
            ftime = days + "天前 " + timeStr;
        } else if (days > 10) {
            ftime = sdate;
        }
        return ftime;
    }
}
