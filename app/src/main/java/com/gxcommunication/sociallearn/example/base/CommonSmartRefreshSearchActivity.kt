package com.hxc.toolslibrary.example.base


import androidx.recyclerview.widget.LinearLayoutManager
import com.gxcommunication.sociallearn.R
import com.gxcommunication.sociallearn.base.BaseActivity
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener
import kotlinx.android.synthetic.main.activity_smartrefresh_material.*

/**
 * @author hxc
 * 通用刷新库 - 带搜索框
 */
class CommonSmartRefreshSearchActivity : BaseActivity() {


    var page = 1
    var pageSize = 20

    //	@Override
    //	protected void onCreate(Bundle savedInstanceState) {
    //		super.onCreate(savedInstanceState);
    //		setContentView(R.layout.activity_smartrefresh_material_search);
    //		ButterKnife.bind(this);
    //		init();
    //		//super.initHeadActionBar();
    //	}

    override fun getLayoutId(): Int {
        return R.layout.activity_smartrefresh_material_search
    }

    override fun init() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        //		mRecyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        //		refreshLayout.setEnableLoadmore(false);
        refreshLayout.setOnRefreshListener { }

        refreshLayout.setOnRefreshLoadmoreListener(object : OnRefreshLoadmoreListener {
            override fun onLoadmore(refreshlayout: RefreshLayout) {

            }

            override fun onRefresh(refreshlayout: RefreshLayout) {

            }
        })
    }

}
