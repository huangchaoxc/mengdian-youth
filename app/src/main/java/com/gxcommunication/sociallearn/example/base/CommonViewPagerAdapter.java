package com.gxcommunication.sociallearn.example.base;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by hxc on 2017/5/4.
 * 通用tab适配器
 */

public class CommonViewPagerAdapter extends FragmentPagerAdapter {
	private String[] arrayTitle = new String[]{"全部", "进行中", "已完结"};
	private List<Fragment> mList;
	public CommonViewPagerAdapter(FragmentManager fm, List<Fragment> data) {
		super(fm);
		mList = data;
	}

	public CommonViewPagerAdapter(FragmentManager fm, String[] arrTitle, List<Fragment> data) {
		super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
		arrayTitle = arrTitle;
		mList = data;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return arrayTitle[position];
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Fragment getItem(int position) {
		return mList.get(position);
	}

}