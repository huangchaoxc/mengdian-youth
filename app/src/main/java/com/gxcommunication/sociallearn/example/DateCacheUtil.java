package com.gxcommunication.sociallearn.example;

import android.content.Context;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyAnswerDetailBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyAnswerDetailResListBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyShareInfoBean;

import java.util.ArrayList;
import java.util.List;

public class DateCacheUtil {


    public static List<RequestResBean> getRequestList(int count){

        List<RequestResBean> list = new ArrayList<>();
        if (count == 0) count = 10;
        for (int i = 0;i < count ; i++){
            list.add(new RequestResBean());
        }
        return list;
    }

    public static List<String> getStringList(int count){
        List<String> list = new ArrayList<>();
        if (count == 0) count = 10;
        for (int i = 0;i < count ; i++){
            list.add("测试" + i);
        }
        return list;
    }

    public static List<StudyAnswerDetailResListBean> getStudyAnswerResDetail(Boolean status){

        List<StudyAnswerDetailResListBean> list = new ArrayList<>();
        if (status){
            StudyAnswerDetailResListBean item1 = new StudyAnswerDetailResListBean("","张三","20","2021-5-3 10：10",
                    "检修不同于新站建设，工作流程、规章纪律可以说是近乎苛刻，这次连工作牌上都有芯片定位，监控每个人状态位置。","1",0);
            item1.isItemAccept = true;
            list.add(item1);


            StudyAnswerDetailResListBean item2 = new StudyAnswerDetailResListBean("","黄花","","2021-5-3 15：00",
                    "因为是部分带电检修，而且第一次共同执行检修工作。为了合作顺利，我们主动出击，先从一些最琐碎的工作开始，比如搭设实验环境、区域仪器设置、记录数据等，很快大家都融入到了工作之中。","1",0);
            list.add(item2);

            StudyAnswerDetailResListBean item3 = new StudyAnswerDetailResListBean("","白树","","2021-5-3 10：10",
                    "对于检修人员来说每一项工作都责任重大，特别是带电检修，万一碰到运行回路，万一有个连片错误影响回路，万一有一处松动节点不通，万一……我们的工作就是确保没有这些万一。","1",0);
            list.add(item3);

        }else {

            StudyAnswerDetailResListBean item2 = new StudyAnswerDetailResListBean("","黄花","","2021-5-3 15：00",
                    "因为是部分带电检修，而且第一次共同执行检修工作。为了合作顺利，我们主动出击，先从一些最琐碎的工作开始，比如搭设实验环境、区域仪器设置、记录数据等，很快大家都融入到了工作之中。","1",1);
            list.add(item2);

            StudyAnswerDetailResListBean item3 = new StudyAnswerDetailResListBean("","白树","","2021-5-3 10：10",
                    "对于检修人员来说每一项工作都责任重大，特别是带电检修，万一碰到运行回路，万一有个连片错误影响回路，万一有一处松动节点不通，万一……我们的工作就是确保没有这些万一。","1",1);
            list.add(item3);

            StudyAnswerDetailResListBean item1 = new StudyAnswerDetailResListBean("","张三","20","2021-5-3 10：10",
                    "检修不同于新站建设，工作流程、规章纪律可以说是近乎苛刻，这次连工作牌上都有芯片定位，监控每个人状态位置。","1",1);
            list.add(item1);



        }



        return list;
    }

    public static List<StudyAnswerDetailResListBean> getMyStudyAnswerDetail0(){

        List<StudyAnswerDetailResListBean> list = new ArrayList<>();
        StudyAnswerDetailResListBean item1 = new StudyAnswerDetailResListBean("","张三","20","2021-5-3 10：10",
                "检修不同于新站建设，工作流程、规章纪律可以说是近乎苛刻，这次连工作牌上都有芯片定位，监控每个人状态位置。","1",0);
            list.add(item1);

        StudyAnswerDetailResListBean item2 = new StudyAnswerDetailResListBean("","黄花","","2021-5-3 15：00",
                "因为是部分带电检修，而且第一次共同执行检修工作。为了合作顺利，我们主动出击，先从一些最琐碎的工作开始，比如搭设实验环境、区域仪器设置、记录数据等，很快大家都融入到了工作之中。","1",1);
        list.add(item2);

        StudyAnswerDetailResListBean item3 = new StudyAnswerDetailResListBean("","白树","","2021-5-3 10：10",
                "对于检修人员来说每一项工作都责任重大，特别是带电检修，万一碰到运行回路，万一有个连片错误影响回路，万一有一处松动节点不通，万一……我们的工作就是确保没有这些万一。","1",1);
        list.add(item3);

        return list;
    }

    public static List<StudyAnswerDetailResListBean> getMyStudyAnswerDetail1(){

        List<StudyAnswerDetailResListBean> list = new ArrayList<>();
        StudyAnswerDetailResListBean item1 = new StudyAnswerDetailResListBean("","陆云","20","2021-5-5 9：10",
                "电压环路是开放的，在测试现场没有强烈的电场干扰。在这种情况下，由于放大器输入的差模电压基本上为0，" +
                        "因此仪器显示的测试值接近于0。如果测试人员具有足够的现场测试经验，则可以推断出仪器是例外。排除仪器电压回路测试线后，可获得最终的准确测试结果;","1",0);
        list.add(item1);

        StudyAnswerDetailResListBean item2 = new StudyAnswerDetailResListBean("","赵丽","","2021-5-6 11：16",
                "电压电路接触不良。在大多数情况下，长期操作后，断路器的接线端子在接线盒的外观上会形成氧化膜或油膜。" +
                        "将回路电阻表的电压测试夹钳夹到这样的端子块上是可能的。如果接触不良，则电压测试线夹本身也将具有正接触电阻。首先将接触电阻值与电压采样环路的内部电阻值进行比较，" +
                        "这将对测试结果产生重大影响。","1",1);
        list.add(item2);

        StudyAnswerDetailResListBean item3 = new StudyAnswerDetailResListBean("","孙伟","","2021-5-7 14：30",
                "电压电路断开或连接不良(电路断开时可见接触电阻R1无限大)，并且在测试现场存在强烈的电磁干扰，例如母线已充电。此时，" +
                        "已充电的母线以空气为介质通过电容器，从而对测试仪产生干扰。对于两条电压测试线，由于干扰，差模电压会出现在环路测试仪的电压采集线的两端。",
                "1",1);
        list.add(item3);

        return list;
    }

    public static List<StudyAnswerDetailBean> getStudyAnswerDetail(){

        List<StudyAnswerDetailBean> list = new ArrayList<>();
        StudyAnswerDetailBean item1 = new StudyAnswerDetailBean("变电运行与维修","什么是感性、容性和阻性电阻?","白华子","20","2021-5-1","0",1);
        StudyAnswerDetailBean item2 = new StudyAnswerDetailBean("变电运行与维修","回路电阻测试仪的现场测试中会出现什么问题?","白华子","20","2021-5-2","0",1);
        StudyAnswerDetailBean item3 = new StudyAnswerDetailBean("变电运行与维修","高压开关柜KYN28A-12是什么结构？各个字母数字又代表什么意思？","白华子","20","2021-5-5","0",1);
        StudyAnswerDetailBean item4 = new StudyAnswerDetailBean("变电运行与维修","电压互感器在变比合适的情况下，反过来就能当电流互感器吗？","白华子","30","2021-5-13","0",1);

        list.add(item1);
        list.add(item2);
        list.add(item3);
        list.add(item4);

        return list;
    }


    public static List<StudyAnswerDetailBean> getMyStudyAnswerDetail(){

        List<StudyAnswerDetailBean> list = new ArrayList<>();
        StudyAnswerDetailBean item1 = new StudyAnswerDetailBean("变电运行与维修","什么是感性、容性和阻性电阻?","白华子","20","2021-5-1","1",0);
        StudyAnswerDetailBean item2 = new StudyAnswerDetailBean("变电运行与维修","回路电阻测试仪的现场测试中会出现什么问题?","白华子","20","2021-5-1","1",1);
        StudyAnswerDetailBean item3 = new StudyAnswerDetailBean("变电运行与维修","高压开关柜KYN28A-12是什么结构？各个字母数字又代表什么意思？","白华子","20","2021-5-1","1",1);

        list.add(item3);
        list.add(item2);
        list.add(item1);

        return list;
    }

    public static List<StudyShareInfoBean> getMyStudyShareList(Context context){

        List<StudyShareInfoBean> list = new ArrayList<>();
        StudyShareInfoBean item1 = new StudyShareInfoBean("变电运行与维修","当电网“邂逅”数字孪生技术", context.getResources().getString(R.string.share_item_content_str_1),"张伟","2021-5-3");
        StudyShareInfoBean item2 = new StudyShareInfoBean("变电运行与维修","地刀和快速地刀的区别",context.getResources().getString(R.string.share_item_content_str_2),"白桦子","2021-5-5" );
        StudyShareInfoBean item3 = new StudyShareInfoBean("变电运行与维修","电磁式电压互感器引发铁磁谐振原因及消谐措施分析",context.getResources().getString(R.string.share_item_content_str_3),"黄忠","2021-5-8");

        list.add(item3);
        list.add(item2);
        list.add(item1);

        return list;
    }

    public static List<StudyShareInfoBean> getMyStudyShareSendList(Context context){

        List<StudyShareInfoBean> list = new ArrayList<>();
        StudyShareInfoBean item1 = new StudyShareInfoBean("变电运行与维修","当电网“邂逅”数字孪生技术", context.getResources().getString(R.string.share_item_content_str_1),"张伟","2021-5-3");
        StudyShareInfoBean item2 = new StudyShareInfoBean("变电运行与维修","地刀和快速地刀的区别",context.getResources().getString(R.string.share_item_content_str_2),"白桦子","2021-5-5" );
        StudyShareInfoBean item3 = new StudyShareInfoBean("变电运行与维修","电磁式电压互感器引发铁磁谐振原因及消谐措施分析",context.getResources().getString(R.string.share_item_content_str_3),"黄忠","2021-5-8");

        list.add(item1);
        list.add(item2);

        return list;
    }


}
