package com.hxc.toolslibrary.example.base

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gxcommunication.sociallearn.R
import com.gxcommunication.sociallearn.base.BaseActivity
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener
import kotlinx.android.synthetic.main.activity_smartrefresh_material.*


/**
 * @author hxc
 * 通用刷新库
 */
 open class CommonSmartRefreshActivity : BaseActivity() {

    open var page = 1
    open var pageSize = 20

    protected lateinit var mRecyclerView : RecyclerView
    protected lateinit var mrRefreshLayout : SmartRefreshLayout

    override fun getLayoutId(): Int {
        return R.layout.activity_smartrefresh_material
    }

    override fun init() {
        initHeadActionBar()

        mRecyclerView = findViewById(R.id.recyclerView)
        mrRefreshLayout = findViewById(R.id.refreshLayout)

        mRecyclerView.layoutManager = LinearLayoutManager(this)

        refreshLayout.setOnRefreshLoadmoreListener(object : OnRefreshLoadmoreListener {
            override fun onLoadmore(refreshlayout: RefreshLayout) {

            }

            override fun onRefresh(refreshlayout: RefreshLayout) {

            }
        })

    }

    fun getRecyclerView(): RecyclerView {
        return mRecyclerView
    }

    fun getRefreshLayout(): SmartRefreshLayout {

        return mrRefreshLayout
    }

}
