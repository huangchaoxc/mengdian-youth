package com.gxcommunication.sociallearn.example.contract;


import com.gxcommunication.sociallearn.base.BaseView;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.base.presenter.IBasePresenter;

public class ExampleContract  {
    /**
     * 处理返回值需求
     */
    public interface View extends BaseView {
        void getInfoRes(RequestResBean bean);
    }

    /**
     * 处理逻辑需求
     */
    public interface Presenter extends IBasePresenter {
        //检查是否需要支付
        void getInfo(String id);
    }
}
