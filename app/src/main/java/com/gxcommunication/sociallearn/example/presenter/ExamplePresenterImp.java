package com.gxcommunication.sociallearn.example.presenter;


import com.gxcommunication.sociallearn.base.presenter.BasePresenterImpl;
import com.gxcommunication.sociallearn.example.contract.ExampleContract;

public class ExamplePresenterImp extends BasePresenterImpl<ExampleContract.View> implements  ExampleContract.Presenter{



    public ExamplePresenterImp(ExampleContract.View view) {
        super(view);
    }


    @Override
    public void getInfo(String id) {

    }
}
