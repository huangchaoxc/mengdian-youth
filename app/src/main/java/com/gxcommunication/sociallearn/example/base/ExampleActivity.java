package com.gxcommunication.sociallearn.example.base;

import android.os.Bundle;
import android.telecom.Call;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.hxc.toolslibrary.example.base.CommonSmartRefreshActivity;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;


/**
 * Created by hxc on 2017/9/20.
 */

public class ExampleActivity extends CommonSmartRefreshActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
		initData();
	}


	public void initView() {

		getRecyclerView().setLayoutManager(new LinearLayoutManager(this));
		getRefreshLayout().setEnableLoadmore(false);
		getRefreshLayout().setOnRefreshListener(new OnRefreshListener() {
			@Override
			public void onRefresh(RefreshLayout refreshlayout) {

			}
		});

	}

	private void initData(){

	}

	private void getInfo(){

//		RequestCallback<RequestResBean> callback = new RequestCallback<RequestResBean>() {
//
//			@Override
//			public void onBefore(Request request, int id) {
//				super.onBefore(request, id);
//			}
//
//			@Override
//			public void onError(Call call, Exception e, int id) {
//				showToast("获取信息失败");
//			}
//
//			@Override
//			public void onResponse(RequestResBean response, int id) {
//
//			}
//		};

//        OkHttpUtils
//                .post()
//                .url(Api.baseUrl)
//                .addParams("method", "TZGGApi_getTZGGlist")
//                .addParams("page", page)
//                .addParams("limit", pageNum)
//                .addHeader("Content-type", "application/x-www-form-urlencoded")
//                .build()
//                .execute(callback);
//
//        OkHttpUtils
//                .get()
//                .url(Api.baseUrl + "android/version/check")
//                .build()
//                .execute(callback);

	}


	private void retrofit(){
//        ApiStore.createApi(BaseApi.class)
//                .getOrderInfo(uid,id)
//                .compose(RxUtils.rxSchedulerHelper())
//                .subscribe(new HttpObserver<OrderDetailBean>() {
//                    @Override
//                    public void onNext(OrderDetailBean baseResp) {
//
//                        if (baseResp.getCode().equals(Api.REQUEST_SUCCESS_CODE_STSTE)) {
//                            mView.orderInfo(baseResp);
//                        } else {
//                            mView.onError(baseResp.getMsg());
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        mView.onError(e.getMessage());
//                    }
//                });
    }


}
