package com.hxc.toolslibrary.example.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gxcommunication.sociallearn.R
import com.gxcommunication.sociallearn.base.BaseFragment
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener
import kotlinx.android.synthetic.main.fragment_common_refresh_no_head.*


/**
 * Created by hxc on 2017/4/14.
 */

open class CommonSmartNoHeadFragment : BaseFragment() {

    protected lateinit var mRefreshLayout: SmartRefreshLayout

    protected lateinit var mRecyclerView: RecyclerView

    private var mView: View? = null
    var page = 1
    var pageSize = 20

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_common_refresh_no_head, null)

        }
        return mView
    }

    override fun initView() {

        mRefreshLayout = view!!.findViewById(R.id.refreshLayout)
        mRecyclerView = view!!.findViewById(R.id.recyclerView)

        mRecyclerView?.layoutManager = LinearLayoutManager(activity)
    }


    override fun lazyLoad() {
//        initListener()
        initData()
    }

    private fun initListener() {

//        refreshLayout.refreshFooter = ClassicsFooter(activity!!).setSpinnerStyle(SpinnerStyle.Scale)

        refreshLayout.setOnRefreshLoadmoreListener(object : OnRefreshLoadmoreListener {
            override fun onLoadmore(refreshlayout: RefreshLayout) {
                page += 1

            }

            override fun onRefresh(refreshlayout: RefreshLayout) {
                page = 1
            }
        })


        recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity!!,
                DividerItemDecoration.VERTICAL
            )
        )

        recyclerView.itemAnimator = DefaultItemAnimator()

    }


    protected fun initData() {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (mView!!.parent != null && mView != null) {
            (mView!!.parent as ViewGroup).removeView(mView)
        }
    }
}
