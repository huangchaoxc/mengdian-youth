package com.hxc.toolslibrary.example.base

import android.view.View
import android.view.ViewGroup
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gxcommunication.sociallearn.R
import com.gxcommunication.sociallearn.base.BaseActivity
import kotlinx.android.synthetic.main.activity_common_recylerview.*


/**
 * @author hxc
 */
class CommonRecylerViewActivity : BaseActivity(), SwipeRefreshLayout.OnRefreshListener {

    var mAdapter: BaseQuickAdapter<*, *>? = null
    var emptyView: View? = null
    var errorView: View? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_common_recylerview
    }

    override fun init() {
        initView()
    }

    //    @Override
    //	protected void onCreate(Bundle savedInstanceState) {
    //		super.onCreate(savedInstanceState);
    //		setContentView(R.layout.activity_common_recylerview);
    //		ButterKnife.bind(this);
    //		initView();
    //	}

    override fun initView() {

     swipeLayout.setOnRefreshListener(this)

    }

    override fun onRefresh() {

    }

    fun setErrorView(resid: Int) {
        errorView = layoutInflater.inflate(resid, recylerView.parent as ViewGroup, false)
    }

    fun setEmptyView(resid: Int) {
        emptyView = layoutInflater.inflate(resid, recylerView.parent as ViewGroup, false)
    }
}
