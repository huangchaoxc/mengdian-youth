package com.hxc.toolslibrary.example.base


import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.gxcommunication.sociallearn.R
import com.gxcommunication.sociallearn.base.BaseActivity
import com.gxcommunication.sociallearn.example.base.CommonViewPagerAdapter
import kotlinx.android.synthetic.main.activity_common_tab.*
import java.util.*


/**
 * @author hxc
 */
class CommonTabActivity : BaseActivity() {

    private var mPagerAdapter: CommonViewPagerAdapter? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_common_tab
    }

    override fun init() {
        initHeadActionBar()
        initData()
        initTabLayout()
    }

    //    @Override
    //	protected void onCreate(Bundle savedInstanceState) {
    //		super.onCreate(savedInstanceState);
    //		setContentView(R.layout.activity_common_tab);
    //		ButterKnife.bind(this);
    //		initHeadActionBar();
    //		initData();
    //		initTabLayout();
    //	}

    override fun initHeadActionBar() {
        super.initHeadActionBar()
        setHeadActionTitle("XXXX")
    }

    private fun initData() {
        val list = ArrayList<Fragment>()
        mPagerAdapter = CommonViewPagerAdapter(supportFragmentManager, list)

        viewPager.adapter = mPagerAdapter
    }


    private fun initTabLayout() {
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout.tabMode = TabLayout.MODE_FIXED
        tabLayout.setupWithViewPager(viewPager)
        //		tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
        //			@Override
        //			public void onTabSelected(TabLayout.Tab tab) {
        //
        //			}
        //
        //			@Override
        //			public void onTabUnselected(TabLayout.Tab tab) {
        //
        //			}
        //
        //			@Override
        //			public void onTabReselected(TabLayout.Tab tab) {
        //
        //			}
        //		});
    }

}
