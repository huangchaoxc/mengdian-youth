package com.gxcommunication.sociallearn.example.base;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.member_activity.bean.MemberActiListBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class ExampleAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public ExampleAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.head_action_left_bar, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
		//helper.setText(R.id.tvTitle,"");
	}
}
