package com.gxcommunication.sociallearn.app;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;

/**
 * 创建人: hxc on 2016/11/9
 * 描述 :
 * 备注:
 */

public class MyApplication extends Application {
    public static int unReadMessageCount = 0;
    public static Application mApplication;

	@Override
    public void onCreate() {
        super.onCreate();

        /**
         * 初始化Fresco图片框架
         */
//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .connectTimeout(30000L, TimeUnit.MILLISECONDS)
//                .readTimeout(30000L, TimeUnit.MILLISECONDS)
//                .addInterceptor(new LoggerInterceptor("SafetySupervision"))
//                .hostnameVerifier(new HostnameVerifier()
//                {
//                    @Override
//                    public boolean verify(String hostname, SSLSession session)
//                    {
//                        return true;
//                    }
//                })
//                .build();
//        OkHttpUtils.initClient(okHttpClient);
//
//		Utils.init(this);
		mApplication = this;

        //在使用SDK各组件之前初始化context信息，传入ApplicationContext
        try {
            SDKInitializer.initialize(this);
            SDKInitializer.setCoordType(CoordType.BD09LL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




//	@Override
//	protected void attachBaseContext(Context base) {
//		super.attachBaseContext(base);
//		MultiDex.install(base);
//	}

    public static Application getInstance() {
        return mApplication;
    }

    /**
     * 获取App安装包信息
     *
     * @return
     */
    public PackageInfo getPackageInfo() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace(System.err);
        }
        if (info == null)
            info = new PackageInfo();
        return info;
    }

}
