package com.gxcommunication.sociallearn.config;

import android.os.Environment;

import com.gxcommunication.sociallearn.utils.FileUtils;


/**
 * 创建人：hx
 * 描述：放置一些全局变量
 */
public class ConfigUtils {
	public static String appSharePreferenceName = "ShengQian";
	public static String apkName = "萌动xx.apk";
	public static String cachePath = "/ShengQian/"; // SD卡 缓存目录
	public static String imgPath = "/image/"; // SD卡 图片缓存目录
	public static String SDcardPath = Environment.getExternalStorageDirectory().getAbsolutePath() + cachePath; // SD卡主目录
	public static String SDcardDocumentPath = SDcardPath + "/Document/"; // SD卡主目录

	public static String cameraimagepath = SDcardPath + "cameratemp.jpg";
	public static String pickimagepath = SDcardPath + "picktemp.jpg";
	public static String avatar = SDcardPath + "avatar.jpg";

	public static String shareImageTemName =  "share_product_tem.jpg";
	public static String shareImageName =  "share_product.jpg";
	public static String shareQRImageName =  "share_qr_product.jpg";
	public static String JPUSH_APP_KEY =  "2062c0bd40eae760d8704204";
	public static String JPUSH_APP_LOGIN_TAG =  "sqyd_jpush_";
	public static boolean DEBUG = false;


	public static void checkSystemFile(){
		try {
//			File file = new File(
//					ConfigUtils.SDcardPath);
//			if (!file.exists()) {
//				file.mkdirs();
//			}
//
//			File file1 = new File(
//					ConfigUtils.SDcardDocumentPath);
//			if (!file1.exists()) {
//				file1.mkdirs();
//			}
//
//			File file2 = new File(
//					ConfigUtils.SDcardPath + imgPath);
//			if (!file.exists()) {
//				file.mkdirs();
//			}

			FileUtils.createOrExistsDir(ConfigUtils.SDcardPath);
			FileUtils.createOrExistsDir(ConfigUtils.SDcardDocumentPath);
			FileUtils.createOrExistsDir(ConfigUtils.SDcardPath + imgPath);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


/*
PrinterShare的MIME:
"application/pdf"
"text/html"
"text/plain"
"image/png"
"image/jpeg"
"application/msword" - .doc
"application/vnd.openxmlformats-officedocument.wordprocessingml.document" - .docx
"application/vnd.ms-excel" - .xls
"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" - .xlsx
"application/vnd.ms-powerpoint" - .ppt
"application/vnd.openxmlformats-officedocument.presentationml.presentation" - .pptx
 */

}
