package com.gxcommunication.sociallearn.base.presenter;


import com.gxcommunication.sociallearn.base.BaseView;

/**
 * ClassName: BasePresenterBasePresenterImpl<p>
 * Author:oubowu<p>
 * Fuction: 代理的基类实现<p>
 * CreateDate:2016/2/14 1:45<p>
 * UpdateUser:<p>
 * UpdateDate:<p>
 */
public abstract class BasePresenterImpl<VIEW extends BaseView> implements IBasePresenter {

  //  protected Subscription mSubscription;
    protected VIEW mView;

    public BasePresenterImpl(VIEW view) {
        mView = view;
    }


    @Override
    public void detachView() {

    }

    protected VIEW getView() {
        return mView;
    }
}
