package com.gxcommunication.sociallearn.base;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.widget.progress.MyProgressDialog;
import com.gxcommunication.sociallearn.manage.AppManager;
import com.gxcommunication.sociallearn.utils.ToastUtils;

import butterknife.ButterKnife;

/**
 * @author hxc activity基类
 */
public abstract class BaseActivity extends AppCompatActivity implements OnClickListener, BaseView {
    public TextView head_title, head_action_title, head_action_both_title, head_action_both_righttitle;
    public ImageView head_action_backimage, head_action_both_back, head_action_both_rightiv;
    public ProgressBar head_progressbar, head_action_progressBar, head_action_both_progressBar;
    private MyProgressDialog mDialog;

    protected abstract int getLayoutId();

    protected abstract void init();

    public <T extends View> T $(int resid) {

        return findViewById(resid);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 添加Activity到堆栈
        if (getLayoutId() != 0) {
            setContentView(getLayoutId());
        }

        ButterKnife.bind(this);
        AppManager.getAppManager().addActivity(this);

        init();


    }

    public void initView() {
        head_title = (TextView) findViewById(R.id.head_title);
        head_progressbar = (ProgressBar) findViewById(R.id.head_progressBar);

    }

    public void initHeadActionBar() {
        head_action_title = (TextView) findViewById(R.id.head_action_title);
        head_action_backimage = (ImageView) findViewById(R.id.head_action_backimage);
        head_action_backimage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
        head_action_progressBar = (ProgressBar) findViewById(R.id.head_action_left_progressBar);
    }

    public void initHeadActionBothTitle() {
        head_action_both_title = (TextView) findViewById(R.id.head_action_both_title);
        head_action_both_righttitle = (TextView) findViewById(R.id.head_action_both_righttitle);
        head_action_both_back = (ImageView) findViewById(R.id.head_action_both_backimage);
        head_action_both_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_push_down_in,
                        R.anim.activity_push_down_out);
            }
        });
        head_action_both_progressBar = (ProgressBar) findViewById(R.id.head_action_both_progressBar);
    }

    public void initHeadActionBothImage() {
        head_action_both_title = (TextView) findViewById(R.id.head_action_both_title);
        head_action_both_back = (ImageView) findViewById(R.id.head_action_both_backimage);
        head_action_both_rightiv = (ImageView) findViewById(R.id.head_action_both_rightimage);
        head_action_both_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.activity_push_down_in,
                        R.anim.activity_push_down_out);
            }
        });
        head_action_both_progressBar = (ProgressBar) findViewById(R.id.head_action_both_progressBar);
    }

    public void setHeadTitle(String title) {
        head_title.setText("" + title);
    }

    public void setHeadActionTitle(String title) {
        head_action_title.setText("" + title);
    }

    public void setHeadActionBothTitle(String title) {
        head_action_both_title.setText("" + title);
    }

    public void setHeadActionBothTitle(String title, String rightTitle) {
        head_action_both_title.setText("" + title);
        head_action_both_righttitle.setText("" + rightTitle);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            default:
                break;
        }
    }

    @Override
    public void onEmpty(Object tag) {

    }

    public void showToast(String msg) {
        try {
//            Utils.toast(this, msg, ToastMsg.SUCCESS);
            ToastUtils.showShort(this,msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //进度条显示
    @Override
    public void showProgress() {
        try {
            if (mDialog == null) {
                mDialog = new MyProgressDialog(BaseActivity.this);
                mDialog.setCanceledOnTouchOutside(true);
            }
            if (mDialog.isShowing()) return;
            mDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //隐藏
    @Override
    public void hideProgress() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public void showErroView(int ico, String msg) {

    }

    @Override
    public void showErroView(String msg) {

    }

    @Override
    public void hideErroView() {

    }

    /**
     * 显示提示消息
     *
     * @param msg
     * @param listener
     */
    protected void showDiglog(String msg, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info).setTitle("提示")
                .setMessage(msg)
                .setPositiveButton(getString(R.string.str_ok), listener)
                .setNegativeButton(getString(R.string.str_cancel), null)
                .create().show();
    }

    protected void showItemDiglog(String title, String[] strings, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info).setTitle(title)
                .setItems(strings, listener)
                .setNegativeButton(getString(R.string.str_cancel), null)
                .create().show();
    }

    private long lastClickTime;

    protected synchronized boolean isFastClick() {
        long time = System.currentTimeMillis();
        if (time - lastClickTime < 500) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    public boolean isEmptyStringIntent(String key) {
        if (getIntent().getStringExtra(key) == null) {
            return true;
        } else {
            return false;
        }
    }

    public String getIntentStringValue(String key) {
        return getIntent().getStringExtra(key);
    }

    public void showShortToast(String msg) {
        ToastUtils.showShort(this, msg);
    }

    /**
     * 检查剪贴板是否有需要弹窗的提示搜券
     */
    @Override
    protected void onResume() {
        super.onResume();
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (isShowSearchDialog){
//                    openZhiNengSearch();
//                }
//            }
//        },500);
//        try {
//
//        } catch (Exception e) {
//        }
    }

    @Override
    protected void onDestroy() {

        try {

            hideProgress();

        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();

    }

    /**
     * Instances of static inner classes do not hold an implicit
     * reference to their outer class.
     * 因为静态的内部类不会持有外部类的引用，所以不会导致外部类实例的内存泄露
     */

    private final MyHandler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        //  private final WeakReference<BaseActivity> mActivity;

        public MyHandler(BaseActivity activity) {
            //  mActivity = new WeakReference<BaseActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

        }
    }


}
