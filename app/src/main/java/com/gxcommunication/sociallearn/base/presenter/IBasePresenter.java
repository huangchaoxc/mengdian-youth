package com.gxcommunication.sociallearn.base.presenter;

/**
 * ClassName: IBasePresenter<p>
 * Author:oubowu<p>
 * Fuction: 代理的基类<p>
 * CreateDate:2016/2/14 1:45<p>
 * UpdateUser:<p>
 * UpdateDate:<p>
 */
public interface IBasePresenter {


    /**
     * 解绑 View
     */
    void detachView();

}
