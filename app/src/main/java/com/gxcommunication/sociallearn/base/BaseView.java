package com.gxcommunication.sociallearn.base;

/**
 * ClassName: BaseView<p>
 * Author:oubowu<p>
 * Fuction: 视图基类<p>
 * CreateDate:2016/2/14 1:41<p>
 * UpdateUser:<p>
 * UpdateDate:<p>
 */
public interface BaseView {
    /**
     * 显示提示消息
     */
    void showToast(String msg);
    /**
     * 显示加载框
     */
    void showProgress();
    /**
     * 隐藏加载框
     */
    void hideProgress();

    /**
     * 空数据
     */
    void onEmpty(Object tag);

    /**
     * 显示错误的view试图,带图标
     * @param msg
     */
    void showErroView(int ico, String msg);

    void showErroView(String msg);
    /**
     * 隐藏错误视图
     */
    void hideErroView();


}
