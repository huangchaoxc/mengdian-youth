package com.hxc.toolslibrary.base

import com.gxcommunication.sociallearn.base.BaseView
import com.gxcommunication.sociallearn.base.presenter.BasePresenterImpl


/**
 * @author hxc
 * @date 2019/10/20
 * @desc BaseMvpFragment
 */
  abstract class BaseMvpFragment<V : BaseView, P : BasePresenterImpl<V>>() : LogFragment(), BaseView {

    /**
     * Presenter
     */
    protected var mPresenter: P? = null


    protected abstract fun createPresenter(): P


    override fun initView() {
        mPresenter = createPresenter()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter?.detachView()
        this.mPresenter = null
    }



}