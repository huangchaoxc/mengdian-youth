package com.gxcommunication.sociallearn.base.bean;/**
 * Created by hxc on 2016/8/15.
 */

/**
 * 请求返回通用实体类
 * Created by hxc on 2016/8/15.
 */
public class BaseBean {
    /**
     * code : 200
     * msg : 删除成功
     * result : {}
     */

    public transient  int code ; //父类和子类有相同字段的时候Gson解析会报错，添加 transient 字段就可以了
    public transient  String msg = "";

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String message) {
        this.msg = message;
    }
}
