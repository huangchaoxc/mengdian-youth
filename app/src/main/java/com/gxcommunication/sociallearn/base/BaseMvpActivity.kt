package com.hxc.toolslibrary.base

import com.gxcommunication.sociallearn.base.BaseActivity
import com.gxcommunication.sociallearn.base.BaseView
import com.gxcommunication.sociallearn.base.presenter.BasePresenterImpl


/**
 * @author hxc
 * @date 2019/10/20
 * @desc BaseMvpActivity
 */

abstract class BaseMvpActivity< V : BaseView, P : BasePresenterImpl<V>> : BaseActivity(), BaseView {

    /**
     * Presenter
     */
    @JvmField
    protected var mPresenter: P? = null

    protected abstract fun createPresenter(): P

    override fun init() {
        mPresenter = createPresenter()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter?.detachView()
        this.mPresenter = null
    }



}