package com.gxcommunication.sociallearn.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


/**
 *  联系人:hxc on 2016/7/28
 *  描 述 :所有fragment的基类
 *  备注:V4包
 */
public abstract  class BaseFragment extends Fragment implements BaseView {

    /**
     * 视图是否加载完毕
     */
    private boolean isViewPrepare = false;
    /**
     * 数据是否加载过了
     */
    protected boolean hasLoadData = false;


    /**
     * 初始化控件，注意kotlin中不能在onCreateView中进行控件绑定
     */
    protected abstract void initView();

    /**
     * 懒加载
     */
    protected abstract void lazyLoad();

    protected  int getLayoutResId(){
        return  0;
    }
    /**
     *  在 FragmentTransaction 调用commit的时候调用此方法,用户切换fragment时需要刷新数据使用
     * @param hidden
     */
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
//        LogHelper.e("--------onHiddenChanged---------");
        if (!hidden){//
         //   updataRequest();
        }
    }

    /**
     *  配合viewpager的时候控制显示和隐藏的时候触发的方法 setUserVisibleHint ——> onCreateView ——> onResume .....
     */
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            lazyLoadDataIfPrepared();
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (getLayoutResId() != 0){
            return inflater.inflate(getLayoutResId(), container, false);
        }
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isViewPrepare = true;
        initView();
        lazyLoadDataIfPrepared();
    }

    private void lazyLoadDataIfPrepared() {
        if (isViewPrepare && !hasLoadData) {
            lazyLoad();
            hasLoadData = true;
        }
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @Override
    public void showToast(String msg) {
        if (getBaseActivity() == null) return;
        getBaseActivity().showToast(msg);
    }

    @Override
    public void showProgress() {
        if (getBaseActivity() == null) return;
        getBaseActivity().showProgress();
    }

    @Override
    public void hideProgress() {
        if (getBaseActivity() == null) return;
        getBaseActivity().hideProgress();
    }

    @Override
    public void onEmpty(Object tag) {

    }

    @Override
    public void showErroView(int ico, String msg) {
        if (getBaseActivity() == null) return;
        getBaseActivity().showErroView(ico, msg);
        getBaseActivity().hideProgress();
    }

    @Override
    public void showErroView(String msg) {
        if (getBaseActivity() == null) return;
        getBaseActivity().showErroView(msg);
    }

    @Override
    public void hideErroView() {
        if (getBaseActivity() == null) return;
        getBaseActivity().hideErroView();
    }


    @Override
    public void onPause() {
        super.onPause();
        hideErroView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getBaseActivity() == null) return;
        getBaseActivity().hideErroView();
    }
}
