package com.gxcommunication.sociallearn.base;

import android.os.Bundle;

import androidx.annotation.Nullable;


/**
 * Created by cmd on 2018/7/14.
 * 当viewpager和fragment嵌套使用
 */

public abstract class LazyBaseFragment extends BaseFragment {

    private boolean mInitSuccess = false;//判断是否初始化view
    protected boolean mIsFirstVisible = true;//判断是否是第一次加载，如果加载失败，可以在实现类设置为true，当再次显示时可以重新加载


    /**
     * 当viewpager和fragment嵌套使用的时候，切换fragment 页面的时候调用此方法，需要借助setUserVisibleHint来进行数据预加载显示
     */
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        prepareLoadData(isVisibleToUser);
//
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mInitSuccess = true;

    }

    private void prepareLoadData(boolean isVisibleToUser){
        if (isVisibleToUser && mInitSuccess && mIsFirstVisible){//当前显示，切初始化之后再加载数据
            mIsFirstVisible = false;
            //获取数据

        }
    }



}
