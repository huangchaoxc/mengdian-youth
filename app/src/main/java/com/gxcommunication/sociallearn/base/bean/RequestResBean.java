package com.gxcommunication.sociallearn.base.bean;/**
 * Created by hxc on 2016/8/15.
 */

/**
 * 请求返回通用实体类
 * Created by hxc on 2016/8/15.
 */
public class RequestResBean extends BaseBean {
    /**
     * code : 400
     * msg : 删除成功
     * result : {}
     */

    private int code;
    private String msg ="";

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
