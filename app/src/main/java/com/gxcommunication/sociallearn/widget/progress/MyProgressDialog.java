package com.gxcommunication.sociallearn.widget.progress;


/**
 * Created by hxc on 2016/8/14.
 */

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import com.gxcommunication.sociallearn.R;


/**
 * Created by hxc on 2016/8/14.
 */
public class MyProgressDialog extends AlertDialog {

    MyCircleProgressBar mProgress;
    public MyProgressDialog(Context context) {
        super(context, R.style.MyDialogStyle);
    }

    public MyProgressDialog(Context context, int theme) {
        super(context, R.style.MyDialogStyle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_mycircleprogressbar);
        mProgress = (MyCircleProgressBar) findViewById(R.id.myCircleProgressBar);
        mProgress.setColorSchemeResources(R.color.color_head,R.color.cpb_blue_dark,R.color.cpb_red_dark);
//        WindowManager.LayoutParams lp= this.getWindow().getAttributes();
//        lp.width = getContext().getResources().getDimensionPixelSize(R.dimen.height_72); // 宽度
//        lp.height =  getContext().getResources().getDimensionPixelSize(R.dimen.height_72); // 高度
//        lp.alpha=1f;
//        getWindow().setAttributes(lp);
    }

//    public  CircleProgressBar getProgress(){
//        return mProgress;
//    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

}
