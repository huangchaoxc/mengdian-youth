package com.gxcommunication.sociallearn.api;



import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface ServiceApi {



    /**
     *  根据参数名获取配置参数
     */
//    @GET(QueryUrl.SYSTEM_CONFIG_SET)
//    Observable<SystemConfigInfoBean> getSystemConfigInfo(@Query("para_key") String para_key);
//
//
//    /**
//     *  启动页  导航页
//     */
//    @GET(QueryUrl.SPLASH_GUIDE)
//    Observable<SplashGuideBean> getSplashGuideInfo();
//
//    /**
//     *  启动页  启动广告
//     */
//    @GET(QueryUrl.SPLASH_ADVERT)
//    Observable<SplashAdvertBean> getSplashAdvertInfo();

    /**
     *
    @FormUrlEncoded
    @POST(QueryUrl.SEARCH_HOT_SAVE_DATA)
    Observable<RequestResBean> saveSearchHot(@Field("keyWord") String keyWord);

    //获取商品详情
    @FormUrlEncoded
    @POST(QueryUrl.zhuanProductDetail1)
    Observable<ProductInfoBean> zhuanProductDetail(@FieldMap Map<String, Object> allParam);


    @GET
    @Streaming
    Observable<ResponseBody> downloadImg(@Url String imgUrl);
     */

    /**
     *  图片下载
     * @param imgUrl
     * @return
     */
    @GET
    @Streaming
    Observable<ResponseBody> downloadImg(@Url String imgUrl);
}
