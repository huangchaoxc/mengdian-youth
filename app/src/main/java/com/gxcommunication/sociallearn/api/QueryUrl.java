package com.gxcommunication.sociallearn.api;


import com.gxcommunication.sociallearn.http.service.ApiConfig;

/**
 *  网络请求 地址
 */
public interface QueryUrl {

    String HOST = ApiConfig.BASE_URL;
    String WEB = "html/";

    /**
     *  根据参数名获取配置参数
     */
    String SYSTEM_CONFIG_SET = "systemConfig/getValueByParaKey";

    /**
     *  启动页  导航页
     */
    String SPLASH_GUIDE = "indexShow/startSetupImgList";

    /**
     * 启动页  启动广告
     */
    String SPLASH_ADVERT = "indexShow/showStartAdviertisement";
    /**
     * 根据system_config表显示或者隐藏某个功能
     */
    String PLASH_SHOW_MODULE = "indexShow/showFlagBySystemConfig";
    /**
     * 首页 顶部模块
     */
    String HOME_HEAD_DATA = "indexShow/indexPageBannerShow";

    /**
     * 首页 广告活动
     */
    String HOME_HEAD_ACTIVI_DATA = "indexShow/indexActivityShow";

    /**
     * 首页 主题
     */
    String HOME_ACTIVITY_THEME = "appTheme/getCurrentAppThemeList";



}
