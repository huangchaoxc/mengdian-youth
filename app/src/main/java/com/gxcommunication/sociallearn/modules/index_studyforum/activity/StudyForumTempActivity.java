package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyForumAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.hxc.toolslibrary.example.base.CommonSmartRefreshActivity;

/**
 *  
 * @author hxc
 * 学创论坛 栏目
 */
public class StudyForumTempActivity extends CommonSmartRefreshActivity {
	private StudyForumAdapter mNewsAdapter;

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_stu;
//    }

    @Override
    protected void init() {
    	super.init();
		initView();
    }

	
	public void initView() {
        head_action_title.setText("学创论坛");
		mNewsAdapter = new StudyForumAdapter(this, DateCacheUtil.getRequestList(6));
		mRecyclerView.setAdapter(mNewsAdapter);

		mNewsAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
				if (position == 3 || position == 4){
					IntentUtils.startActivity(StudyForumTempActivity.this, StudyAnswerActivity.class);
				}else if (position == 1){
					IntentUtils.startActivity(StudyForumTempActivity.this,StudyStoreActivity.class);
				}

			}
		});
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}
	
	


}
