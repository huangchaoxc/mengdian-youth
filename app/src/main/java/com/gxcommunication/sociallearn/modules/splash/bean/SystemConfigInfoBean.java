package com.gxcommunication.sociallearn.modules.splash.bean;


import com.gxcommunication.sociallearn.base.bean.BaseBean;

/**
 * 根据参数名获取配置参数
 */

public class SystemConfigInfoBean  extends BaseBean {


    /**
     * success : true
     * message : 查询成功
     * code : 200
     * timestamp : 1609809803689
     * result : {"id":486271,"para_key":"grayColor","para_value":"yes","status":1,"team":"","endTime":null,"info":"一键置灰"}
     * count : 0
     */

    private boolean success;
    private String message;
    private int code;
    private long timestamp;
    private ResultBean result;
    private int count;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static class ResultBean {
        /**
         * id : 486271
         * para_key : grayColor
         * para_value : yes
         * status : 1   0代表不可用， 1代表可用
         * team :
         * endTime : null
         * info : 一键置灰
         */

        private int id;
        private String para_key;
        private String para_value;
        private int status;
        private String team;
        private String info;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPara_key() {
            return para_key;
        }

        public void setPara_key(String para_key) {
            this.para_key = para_key;
        }

        public String getPara_value() {
            return para_value;
        }

        public void setPara_value(String para_value) {
            this.para_value = para_value;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getTeam() {
            return team;
        }

        public void setTeam(String team) {
            this.team = team;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }
}
