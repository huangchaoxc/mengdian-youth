package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;


import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *  首页——分类--左侧分类
 */

public class FirstMenuAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    private int checkPos = 0;

	public FirstMenuAdapter(Context context, List<RequestResBean> arrays) {
		super(R.layout.item_study_store_firstmenu, arrays);
		mContext = context;
	}

    public void setCheckPos(int pos) {
        checkPos = pos;
        notifyDataSetChanged();
    }

    public int  getCheckPos(){

        return checkPos;
    }


    @Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
        helper.setText(R.id.tvTitle, "其他");
		if (helper.getLayoutPosition() == 0)helper.setText(R.id.tvTitle, "全部");
		if (helper.getLayoutPosition() == 1)helper.setText(R.id.tvTitle, "技能鉴定");
		if (helper.getLayoutPosition() == 2)helper.setText(R.id.tvTitle, "普考知识");
		if (helper.getLayoutPosition() == 3)helper.setText(R.id.tvTitle, "专业知识");

        if (checkPos == helper.getAdapterPosition()) {
            helper.setTextColor(R.id.tvTitle, mContext.getResources().getColor(R.color.white));
            helper.setVisible(R.id.tagView,true);
            helper.setBackgroundRes(R.id.tvTitle,R.drawable.user_info_item_round_bg);
        } else {
            helper.setGone(R.id.tagView,false);
            helper.setTextColor(R.id.tvTitle, mContext.getResources().getColor(R.color.color_text_gray));

            helper.setBackgroundRes(R.id.tvTitle,R.drawable.round_gray_8_bg);
        }
	}
}
