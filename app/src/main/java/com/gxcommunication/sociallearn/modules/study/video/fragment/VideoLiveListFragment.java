package com.gxcommunication.sociallearn.modules.study.video.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VideoLiveListAdapter;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VideoMainListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName VideoMainShowFragment.java
 * @Description 直播列表
 * @createTime 2021年05月05日 11:34
 */
public class VideoLiveListFragment extends BaseFragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    VideoLiveListAdapter mAdapter;
    private View mView;

    public static VideoLiveListFragment getInstance() {
        VideoLiveListFragment fragment = new VideoLiveListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.common_list, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }


    @Override
    protected void initView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter =  new VideoLiveListAdapter(getActivity(), DateCacheUtil.getRequestList(6));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void lazyLoad() {

    }
}
