package com.gxcommunication.sociallearn.modules.study.video.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;

import butterknife.ButterKnife;

/**
 * 视频详情-视频评论
 */
public class VideoCommentFragment extends BaseFragment {

    private View mView;

    private static final String ARG_VIDEOID = "params_video_id";
    public static VideoCommentFragment getInstance(String videoId) {
        VideoCommentFragment fragment = new VideoCommentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_VIDEOID, videoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_video_comment, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }


    @Override
    protected void initView() {

    }

    @Override
    protected void lazyLoad() {

    }
}
