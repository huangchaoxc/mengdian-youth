package com.gxcommunication.sociallearn.modules.user.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 用户模块 新手任务  新手培训考核
 */
public class UserNewhandLearnActivity extends BaseActivity {


    @BindView(R.id.item_learn_type)
    TextView itemLearnType;
    @BindView(R.id.item_learn_title_tv)
    TextView itemLearnTitleTv;
    @BindView(R.id.item_learn_question_01)
    Button itemLearnQuestion01;
    @BindView(R.id.item_learn_question_02)
    Button itemLearnQuestion02;
    @BindView(R.id.item_learn_question_03)
    Button itemLearnQuestion03;
    @BindView(R.id.item_learn_question_04)
    Button itemLearnQuestion04;
    @BindView(R.id.item_learn_question_result_iv)
    ImageView itemLearnQuestionResultIv;
    @BindView(R.id.item_learn_res_tv)
    TextView itemLearnResTv;
    @BindView(R.id.item_learn_res_hint_tv)
    TextView itemLearnResHintTv;
    @BindView(R.id.action_next_btn)
    TextView actionNextBtn;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_newhand_learn;
    }


    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		setHeadActionTitle(" 新手培训考核");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick({R.id.action_nexttime_btn, R.id.item_learn_question_01, R.id.item_learn_question_02, R.id.item_learn_question_03, R.id.item_learn_question_04, R.id.action_next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.action_nexttime_btn:
                break;
            case R.id.item_learn_question_01:
                break;
            case R.id.item_learn_question_02:
                break;
            case R.id.item_learn_question_03:
                break;
            case R.id.item_learn_question_04:
                break;
            case R.id.action_next_btn:
                break;
        }
    }
}
