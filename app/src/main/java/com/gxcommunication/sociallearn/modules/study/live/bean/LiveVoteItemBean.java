package com.gxcommunication.sociallearn.modules.study.live.bean;

public class LiveVoteItemBean {

    public LiveVoteItemBean(String title, int status) {
        this.title = title;
        this.statusCode = status;
    }

    public String voteId;
    public String userId;
    public String title;
    public int statusCode;//0 未完成 1 已完成
    public String createTime;

}
