package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.adapter.SimulationMeterToolsAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 *
 * 场景模拟列  仪表和工具准备
 */
public class ScenarioSimulationMeterToolsActivity extends BaseActivity {

    @BindView(R.id.scenario_simulation_title_tv)
    TextView scenarioSimulationTitleTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.scenario_simulation_tips_tv)
    TextView scenarioSimulationTipsTv;
    @BindView(R.id.scenario_simulation_action_tv)
    TextView scenarioSimulationActionTv;

    SimulationMeterToolsAdapter mAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_danger_analyse;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("仪表和工具准备");

        scenarioSimulationTitleTv.setText("请根据实际工作需要，准备一起仪表和工器具");
        scenarioSimulationTipsTv.setText("提示：确认仪器仪表和工具器准备完毕，进入及时资料的准备工作。");
        scenarioSimulationActionTv.setText("提交");

        mAdapter = new SimulationMeterToolsAdapter(this,DateCacheUtil.getRequestList(10));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick(R.id.scenario_simulation_action_tv)
    public void onViewClicked() {
        IntentUtils.startActivity(this,ScenarioSimulationTechnologyActivity.class);
        finish();
    }

    /**
     * 监听返回--是否退出程序
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean flag = true;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 是否退出应用
            showToast("是否要");

            showDiglog("是否要退出模拟实训？", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            flag = super.onKeyDown(keyCode, event);
        }
        return flag;
    }
}
