package com.gxcommunication.sociallearn.modules.study.video.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyAnswersSendActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioListerAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.gxcommunication.sociallearn.widget.BetterSpinner;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author hxc
 * 视频课程
 */
public class VideoCourseActivity extends BaseActivity {


    @BindView(R.id.category_pop_tv)
    TextView categoryPoptv;

    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView studyCategotyRecyclerview;
    @BindView(R.id.study_recyclerview)
    RecyclerView studyRecyclerview;

    StudyGridCategoryAdapter mSecondCategoryAdapter;
    VedioListerAdapter vedioListerAdapter;

    @BindView(R.id.ed_search)
    EditText edSearch;

    @BindView(R.id.sort_hot_iv)
    ImageView sortHotIv;
    @BindView(R.id.sort_new_iv)
    ImageView sortNewIv;

    private StudyCategoryPopupWindow mCategoryPopupWindow;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_courese;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBothTitle();
        setHeadActionBothTitle("视频课堂","");

        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(10));
        studyCategotyRecyclerview.setLayoutManager(new GridLayoutManager(this,5));
        studyCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);

        vedioListerAdapter=new VedioListerAdapter(this, DateCacheUtil.getStringList(20));
        studyRecyclerview .setLayoutManager(new LinearLayoutManager(this));
        studyRecyclerview.setAdapter(vedioListerAdapter);

        mSecondCategoryAdapter.setSelectItem(0);
        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });

        mSecondCategoryAdapter.setSelectItem(0);

        vedioListerAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(VideoCourseActivity.this, VideoDetailsActivity.class);
            }
        });

        categoryPoptv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopupwinds();
            }
        });

        mCategoryPopupWindow = new StudyCategoryPopupWindow(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

    }



    private void showPopupwinds() {
        mCategoryPopupWindow.showAsDropDown(categoryPoptv);

    }


    @OnClick({R.id.sort_hot_iv, R.id.sort_new_iv})
    public void onViewSortClicked(View view) {
        switch (view.getId()) {
            case R.id.sort_hot_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_select);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_normal);
                break;
            case R.id.sort_new_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_normal);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_select);
                break;
        }
    }



}
