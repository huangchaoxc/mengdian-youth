
package com.gxcommunication.sociallearn.modules.immersive.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ListView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.utils.DateUtils;

import java.util.List;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName ScenarioSimulationAdapter.java
 * @Description 场景模拟实训
 * @createTime 2021年05月08日 21:49
 */
public class SimulationMeterToolsAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public SimulationMeterToolsAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_simulation_metertools_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        helper.setText(R.id.item_meter_tools_title_tv,"安全帽").
        setText(R.id.item_meter_tools_num_tv,"" + item.getCode() + 1);

        helper.getView(R.id.item_meter_tools_sub_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getCode() > 0){
                    item.setCode(item.getCode() -1);
                    helper.setText(R.id.item_meter_tools_num_tv,"" + item.getCode() );
                }else {

                }
            }
        });

        helper.getView(R.id.item_meter_tools_add_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setCode(item.getCode() + 1);
                helper.setText(R.id.item_meter_tools_num_tv,"" + item.getCode() );
            }
        });


    }
}
