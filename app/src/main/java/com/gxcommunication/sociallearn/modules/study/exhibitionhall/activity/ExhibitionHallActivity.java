package com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.adapter.ExhibitionHallListAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * 创新展览馆
 */
public class ExhibitionHallActivity extends BaseActivity {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;


    @BindView(R.id.exhibition_categoty_item_01)
    TextView exhibitionCategotyItem01;
    @BindView(R.id.exhibition_categoty_item_02)
    TextView exhibitionCategotyItem02;
    @BindView(R.id.exhibition_categoty_item_03)
    TextView exhibitionCategotyItem03;

    ExhibitionHallListAdapter exhibitionHallListAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exhibition_hall;
    }

    @Override
    protected void init() {
        initHeadActionBar();
        setHeadActionTitle("创新成果");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        refreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {

            }

            @Override
            public void onRefresh(RefreshLayout refreshlayout) {

            }
        });

        exhibitionHallListAdapter = new ExhibitionHallListAdapter(this, DateCacheUtil.getRequestList(6));

        recyclerView.setAdapter(exhibitionHallListAdapter);

        exhibitionHallListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(ExhibitionHallActivity.this, ExhibitionHallResultListActivity.class);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.exhibition_categoty_item_01, R.id.exhibition_categoty_item_02, R.id.exhibition_categoty_item_03})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exhibition_categoty_item_01:
                selectFirstCategory(R.id.exhibition_categoty_item_01);
                break;
            case R.id.exhibition_categoty_item_02:
                selectFirstCategory(R.id.exhibition_categoty_item_02);
                break;
            case R.id.exhibition_categoty_item_03:
                selectFirstCategory(R.id.exhibition_categoty_item_03);
                break;
        }
    }

    private void selectFirstCategory(int resid){

        if (resid == R.id.exhibition_categoty_item_01){//技能鉴定
            exhibitionCategotyItem01.setBackgroundColor(getResources().getColor(R.color.color_head));
            exhibitionCategotyItem02.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            exhibitionCategotyItem03.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
        }else  if (resid == R.id.exhibition_categoty_item_02){//安规
            exhibitionCategotyItem01.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            exhibitionCategotyItem02.setBackgroundColor(getResources().getColor(R.color.color_head));
            exhibitionCategotyItem03.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
        } else if (resid == R.id.exhibition_categoty_item_03){//普考
            exhibitionCategotyItem01.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            exhibitionCategotyItem02.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            exhibitionCategotyItem03.setBackgroundColor(getResources().getColor(R.color.color_head));
        }
    }
}
