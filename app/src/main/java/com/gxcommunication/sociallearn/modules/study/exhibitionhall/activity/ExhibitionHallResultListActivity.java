package com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.adapter.ExhibitionHallResultAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.gxcommunication.sociallearn.widget.BetterSpinner;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创新展览馆（成果列表）
 */
public class ExhibitionHallResultListActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;


    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.sort_spinner)
    BetterSpinner sortSpinner;

    @BindView(R.id.exhibition_categoty_item_01)
    TextView exhibitionCategotyItem01;
    @BindView(R.id.exhibition_categoty_item_02)
    TextView exhibitionCategotyItem02;
    @BindView(R.id.exhibition_categoty_item_03)
    TextView exhibitionCategotyItem03;

    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView studyCategotyRecyclerview;


    @BindView(R.id.category_pop_tv)
    TextView categoryPopTv;
    @BindView(R.id.exhibition_categoty_item_detail_spinner)
    Spinner exhibitionCategotyItemDetailSpinner;

    @BindView(R.id.sort_hot_iv)
    ImageView sortHotIv;
    @BindView(R.id.sort_new_iv)
    ImageView sortNewIv;

    private StudyCategoryPopupWindow mCategoryPopupWindow;
    private StudyGridCategoryAdapter mSecondCategoryAdapter;

    private ExhibitionHallResultAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exhibition_hall_result_list;
    }

    @Override
    protected void init() {
        initHeadActionBar();
        setHeadActionTitle("创新成果");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        refreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {

            }

            @Override
            public void onRefresh(RefreshLayout refreshlayout) {

            }
        });

        mAdapter = new ExhibitionHallResultAdapter(this, DateCacheUtil.getRequestList(6));

        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(ExhibitionHallResultListActivity.this, ExhibitionHallDetailsActivity.class);
            }
        });

        loadSpinner();

        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(10));
        studyCategotyRecyclerview.setLayoutManager(new GridLayoutManager(this, 3));
        studyCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);

        mCategoryPopupWindow = new StudyCategoryPopupWindow(this);

        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });
        mSecondCategoryAdapter.setSelectItem(0);
    }


    private void loadSpinner() {

        String[] list = getResources().getStringArray(R.array.sort_sponner);

        ArrayAdapter<String> sortAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        sortSpinner.setAdapter(sortAdapter);

        sortSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LogHelper.e("-----------" + position);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.category_pop_tv)
    public void onViewClicked() {

        mCategoryPopupWindow.showAsDropDown(studyCategotyRecyclerview);
    }

//    public static List<String> getStringList(int count){
//        List<String> list = new ArrayList<>();
//        if (count == 0) count = 10;
//        for (int i = 0;i < count ; i++){
//            list.add("变电检查" + i);
//        }
//        return list;
//    }

    private void selectFirstCategory(int resid) {

        if (resid == R.id.exhibition_categoty_item_01) {//技能鉴定
            exhibitionCategotyItem01.setBackgroundResource(R.drawable.common_btn_bg_select);
            exhibitionCategotyItem02.setBackgroundResource(R.drawable.common_btn_bg);
            exhibitionCategotyItem03.setBackgroundResource(R.drawable.common_btn_bg);

        } else if (resid == R.id.exhibition_categoty_item_02) {//安规
            exhibitionCategotyItem01.setBackgroundResource(R.drawable.common_btn_bg);
            exhibitionCategotyItem02.setBackgroundResource(R.drawable.common_btn_bg_select);
            exhibitionCategotyItem03.setBackgroundResource(R.drawable.common_btn_bg);

        } else if (resid == R.id.exhibition_categoty_item_03) {//普考
            exhibitionCategotyItem01.setBackgroundResource(R.drawable.common_btn_bg);
            exhibitionCategotyItem02.setBackgroundResource(R.drawable.common_btn_bg);
            exhibitionCategotyItem03.setBackgroundResource(R.drawable.common_btn_bg_select);

        }
    }

    @OnClick({R.id.exhibition_categoty_item_01, R.id.exhibition_categoty_item_02, R.id.exhibition_categoty_item_03})
    public void onViewCategoryClicked(View view) {
        switch (view.getId()) {
            case R.id.exhibition_categoty_item_01:
                selectFirstCategory(R.id.exhibition_categoty_item_01);
                break;
            case R.id.exhibition_categoty_item_02:
                selectFirstCategory(R.id.exhibition_categoty_item_02);
                break;
            case R.id.exhibition_categoty_item_03:
                selectFirstCategory(R.id.exhibition_categoty_item_03);
                break;
        }
    }

    @OnClick({R.id.sort_hot_iv, R.id.sort_new_iv})
    public void onViewSortClicked(View view) {
        switch (view.getId()) {
            case R.id.sort_hot_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_select);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_normal);
                break;
            case R.id.sort_new_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_normal);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_select);
                break;
        }
    }
}
