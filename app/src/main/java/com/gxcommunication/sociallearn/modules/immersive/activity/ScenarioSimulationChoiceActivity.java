package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 *
 * 场景模拟 流程
 */
public class ScenarioSimulationChoiceActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_choice;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("变电站正常巡视");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }



    @OnClick({R.id.scenario_simulation_tv_01, R.id.scenario_simulation_tv_02, R.id.scenario_simulation_tv_03, R.id.scenario_simulation_tv_04})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.scenario_simulation_tv_01://确定人员
                IntentUtils.startActivity(this,ScenarioSimulationPersonActivity.class);
                break;
            case R.id.scenario_simulation_tv_02://准备工作
                IntentUtils.startActivity(this,ScenarioSimulationPrepareActivity.class);
                break;
            case R.id.scenario_simulation_tv_03://开始巡视

                break;
            case R.id.scenario_simulation_tv_04://巡视结束

                break;
        }
    }

    /**
     * 监听返回--是否退出程序
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean flag = true;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 是否退出应用
           showToast("是否要");

           showDiglog("是否要退出模拟实训？", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int which) {
                   finish();
               }
           });
        } else {
            flag = super.onKeyDown(keyCode, event);
        }
        return flag;
    }

}
