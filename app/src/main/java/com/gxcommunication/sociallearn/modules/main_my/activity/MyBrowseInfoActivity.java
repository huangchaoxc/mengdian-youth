package com.gxcommunication.sociallearn.modules.main_my.activity;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hxc
 * 我的浏览
 */
public class MyBrowseInfoActivity extends BaseActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_issue;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("我的浏览");
        recyclerview.setLayoutManager(new LinearLayoutManager(this));

        initTabLayout();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

	private void initTabLayout() {
		tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
		tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
		tabLayout.addTab(tabLayout.newTab().setText("创新展览馆"));
		tabLayout.addTab(tabLayout.newTab().setText("自我评定"));
		tabLayout.addTab(tabLayout.newTab().setText("学创论坛"));
		tabLayout.addTab(tabLayout.newTab().setText("身临其境"));

		tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});
	}
}
