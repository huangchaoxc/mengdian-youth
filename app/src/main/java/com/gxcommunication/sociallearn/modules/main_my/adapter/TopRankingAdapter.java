package com.gxcommunication.sociallearn.modules.main_my.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 个人中心 top排行
 */

public class TopRankingAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public TopRankingAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_top_ranking_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
		helper.setText(R.id.item_top_ranking_rank_tv,helper.getAdapterPosition() + 1 + "");

		if (helper.getLayoutPosition() == 0){
			helper.setBackgroundRes(R.id.item_top_ranking_rank_tv,R.mipmap.ic_top_ranking_1);
		}else if (helper.getLayoutPosition() == 1){
			helper.setBackgroundRes(R.id.item_top_ranking_rank_tv,R.mipmap.ic_top_ranking_2);
		}else if (helper.getLayoutPosition() == 2){
			helper.setBackgroundRes(R.id.item_top_ranking_rank_tv,R.mipmap.ic_top_ranking_3);
		}else {
			helper.getView(R.id.item_top_ranking_rank_tv).setBackground(null);
		}
	}
}
