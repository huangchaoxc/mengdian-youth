package com.gxcommunication.sociallearn.modules.main_index.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.main_index.bean.HomeIndexBannerListBean;
import com.gxcommunication.sociallearn.utils.ImageManager;

import java.util.List;


/**
 * Created by hxc on 2017/5/8.
 * <p>
 * 首页 学习 九宫格适配器
 */

public class HomeGuideTypeAdapter extends BaseQuickAdapter<HomeIndexBannerListBean.ResultBean, BaseViewHolder> {
    private Context mContext;

    public HomeGuideTypeAdapter(Context context, List<HomeIndexBannerListBean.ResultBean> data) {
        super(R.layout.home_head_guide_type_item, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, HomeIndexBannerListBean.ResultBean item) {

        switch (helper.getAdapterPosition()) {
            case 0:
                helper.setText(R.id.tv_item_all_fenlei, "学习任务");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_01, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 1:
                helper.setText(R.id.tv_item_all_fenlei, "创新成果");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_02, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 2:
                helper.setText(R.id.tv_item_all_fenlei, "视频课程");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_03, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 3:
                helper.setText(R.id.tv_item_all_fenlei, "直播课堂");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_live, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 4:
                helper.setText(R.id.tv_item_all_fenlei, "学创论坛");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_04, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 5:
                helper.setText(R.id.tv_item_all_fenlei, "身临其境");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_05, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 6:
                helper.setText(R.id.tv_item_all_fenlei, "名师堂");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_06, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 7:
                helper.setText(R.id.tv_item_all_fenlei, "知识库");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_07, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 8:
                helper.setText(R.id.tv_item_all_fenlei, "学习小组");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_08, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 9:
                helper.setText(R.id.tv_item_all_fenlei, "自我提升");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_09, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 10:
                helper.setText(R.id.tv_item_all_fenlei, "PK竞技");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_10, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 11:
                helper.setText(R.id.tv_item_all_fenlei, "排行榜");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_rank, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 12:
                helper.setText(R.id.tv_item_all_fenlei, "成长足迹");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_11, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 13:
                helper.setText(R.id.tv_item_all_fenlei, "卡券");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_card, helper.getView(R.id.iv_item_all_fenlei));
                break;
        }


    }
}
