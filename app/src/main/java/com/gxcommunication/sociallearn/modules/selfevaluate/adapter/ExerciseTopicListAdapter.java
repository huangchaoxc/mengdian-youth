package com.gxcommunication.sociallearn.modules.selfevaluate.adapter;

import android.content.Context;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 自我评定 问题列表
 */

public class ExerciseTopicListAdapter extends BaseMultiItemQuickAdapter<StudyTopicMultiItemBean, BaseViewHolder> {

    private boolean isShowResultHint = false;//是否显示提示按钮，如果是学习和考试没有提示按钮，练习的情况下有

    public void setShowResultHint(boolean showResultHint) {
        isShowResultHint = showResultHint;
    }

    public ExerciseTopicListAdapter(Context context, List<StudyTopicMultiItemBean> data) {
        super(data);
        mContext = context;

        addItemType(StudyTopicMultiItemBean.Topic_Radio_Type, R.layout.item_study_topic_radio_activity);
        addItemType(StudyTopicMultiItemBean.Topic_Multiple_Type, R.layout.item_study_topic_multiple_activity);
        addItemType(StudyTopicMultiItemBean.Topic_Judge_Type, R.layout.item_study_topic_judge_activity);
        addItemType(StudyTopicMultiItemBean.Topic_Compute_Type, R.layout.item_study_topic_radio_activity);
    }

    @Override
    protected void convert(BaseViewHolder helper, StudyTopicMultiItemBean item) {
        helper.setText(R.id.item_learn_pos_tv, "第" + helper.getLayoutPosition() + 1 + "题");//

        switch (helper.getItemViewType()) {
            case StudyTopicMultiItemBean.Topic_Radio_Type://单选题目

                break;
            case StudyTopicMultiItemBean.Topic_Multiple_Type://多选题目

                break;
            case StudyTopicMultiItemBean.Topic_Judge_Type://判断题

                break;
            case StudyTopicMultiItemBean.Topic_Compute_Type://计算题
                helper.setText(R.id.item_learn_type_tv, "计算题");

                break;
        }

        int pos = helper.getLayoutPosition() + 1;
        helper.setText(R.id.item_learn_pos_tv,"第" + pos+ "题");


        helper.addOnClickListener(R.id.item_topic_next_iv);

        helper.setGone(R.id.item_topic_res_hint_tv,true);

        helper.getView(R.id.item_topic_res_hint_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.setGone(R.id.item_topic_res_tv,true);
            }
        });

    }
}
