package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAdapter;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 学创论坛 子栏目  你问我答   下 我的收藏和我的问题
 */
public class MyStudyAnswerCollectActivity extends BaseActivity {

    public static String INTENT_KEY_TYPE = "key_type"; //根据type 判断 进入的时候是选择加载 我的收藏还是我发布的问题， 0是收藏 1是 问题

    @BindView(R.id.my_study_collect)
    TextView myStudyCollect;
    @BindView(R.id.my_study_issue)
    TextView myStudyIssue;
    @BindView(R.id.study_recyclerview)
    RecyclerView studyRecyclerview;

	StudyAnswerAdapter mAdapter;

	@Override
    protected int getLayoutId() {
        return R.layout.activity_my_study_collect_issue;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("你问我答");

		int selectType = getIntent().getIntExtra(INTENT_KEY_TYPE,0);
        studyRecyclerview .setLayoutManager(new LinearLayoutManager(this));

		if (selectType == 0){
            myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_head));
            myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            mAdapter = new StudyAnswerAdapter(this, DateCacheUtil.getStudyAnswerDetail());
            studyRecyclerview.setAdapter(mAdapter);

        }else {
            myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_head));

            mAdapter = new StudyAnswerAdapter(this, DateCacheUtil.getMyStudyAnswerDetail());
            studyRecyclerview.setAdapter(mAdapter);

        }


        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                IntentUtils.startActivityForString(MyStudyAnswerCollectActivity.this,StudyAnswerDetailActivity.class,StudyAnswerDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mAdapter.getItem(position)));

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.my_study_collect, R.id.my_study_issue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.my_study_collect:// 选择收藏
				myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_head));
				myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                mAdapter.setNewData( DateCacheUtil.getStudyAnswerDetail());
                break;
            case R.id.my_study_issue:// 选择我发布的问题
				myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
				myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_head));
                mAdapter.setNewData( DateCacheUtil.getMyStudyAnswerDetail());
                break;
        }
    }
}
