package com.gxcommunication.sociallearn.modules.study.video.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoDetailsActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoPlayActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.List;


/**
 * 视频标签卡
 * Created by hxc on 2017/8/7
 */

public class VedioFirstCatalogueAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public VedioFirstCatalogueAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_vedio_first_catalogue, data);
        mContext = context;
    }
    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        helper.setText(R.id.catalogue_name,"第"+ helper.getLayoutPosition() + 1 +"章  电力系统基础知识" );
        RecyclerView recyclerView = helper.getView(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        VedioCatalogueAdapter secondAdapter = new VedioCatalogueAdapter(mContext, DateCacheUtil.getRequestList(5));
        recyclerView.setAdapter(secondAdapter);

        secondAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                IntentUtils.startActivity(mContext, VideoPlayActivity.class);
            }
        });
    }
	/**
	 * 获取格式化内容
	 * @param id
	 * @param formats
	 * @return
	 */
    private String getFormat(int id,Object ...formats){
		return mContext.getResources().getString(id, formats);
	}
}
