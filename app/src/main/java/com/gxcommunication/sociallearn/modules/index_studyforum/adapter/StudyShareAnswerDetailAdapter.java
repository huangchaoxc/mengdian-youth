package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyAnswerDetailResListBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 学创论坛 你问我答 经验分享  问答详情 列表
 */

public class StudyShareAnswerDetailAdapter extends BaseQuickAdapter<StudyAnswerDetailResListBean, BaseViewHolder> {

	public StudyShareAnswerDetailAdapter(Context context, List<StudyAnswerDetailResListBean> data) {
		super(R.layout.item_study_answer_detail_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, StudyAnswerDetailResListBean item) {
		helper.setText(R.id.item_user_nickname_tv,item.userName);//用户名
		helper.setText(R.id.item_share_detail_time_tv,item.createTime);//时间

		helper.setText(R.id.item_share_detail_content_tv,item.content);//回答内容

		helper.setText(R.id.item_share_detail_award_tv,"奖励：" + item.awardScore + "分");//奖励分数

		if (item.isItemAccept){//模拟显示采纳，显示采纳按钮
			helper.setGone(R.id.item_share_detail_award_iv,true);
			helper.setGone(R.id.item_share_detail_award_tv,true);

		}else {
			helper.setGone(R.id.item_share_detail_award_iv,false);
			helper.setGone(R.id.item_share_detail_award_tv,false);

		}

		helper.setGone(R.id.action_btn,false);//非作者隐藏采纳操作按钮

	}
}
