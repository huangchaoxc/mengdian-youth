package com.gxcommunication.sociallearn.modules.study.video.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * 视频标签卡
 * Created by hxc on 2017/8/7
 */

public class VedioCatalogueAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public VedioCatalogueAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_vedio_catalogue, data);
        mContext = context;
    }
    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        helper.setText(R.id.catalogue_name,"第"+ helper.getLayoutPosition() + 1 +"节 电力系统基本概念" );
    }
	/**
	 * 获取格式化内容
	 * @param id
	 * @param formats
	 * @return
	 */
    private String getFormat(int id,Object ...formats){
		return mContext.getResources().getString(id, formats);
	}
}
