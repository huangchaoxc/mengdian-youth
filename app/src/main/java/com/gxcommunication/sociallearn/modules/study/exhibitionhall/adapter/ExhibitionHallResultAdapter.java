package com.gxcommunication.sociallearn.modules.study.exhibitionhall.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * 结果页列表适配器
 * Created by hxc on 2017/8/7
 */

public class ExhibitionHallResultAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {



    public ExhibitionHallResultAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_exhibition_hall_result, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

    }

	/**
	 * 获取格式化内容
	 * @param id
	 * @param formats
	 * @return
	 */
    private String getFormat(int id,Object ...formats){
		return mContext.getResources().getString(id, formats);
	}
}
