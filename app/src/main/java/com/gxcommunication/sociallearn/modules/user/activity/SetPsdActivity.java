package com.gxcommunication.sociallearn.modules.user.activity;


import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *  修改密码
 */
public class SetPsdActivity extends BaseActivity implements View.OnClickListener {


    @BindView(R.id.edit_phone)
    EditText editPhone;
    @BindView(R.id.edit_old_password)
    EditText editOldPassword;
    @BindView(R.id.edit_new_password)
    EditText editNewPassword;
    @BindView(R.id.edit_new_password_sure)
    EditText editNewPasswordSure;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_psd;
    }

    @Override
    protected void init() {
        findViews();
        initData();
    }

    protected void findViews() {
//        super.initHeadActionBar();
//        head_action_title.setText("设置登录密码");


    }

    protected void initData() {


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

    }



    /**
     * 登录
     */
    private void login() {
        if (!StringUtils.vertifyPhone(this, editPhone.getText().toString())) {
            showToast("手机号不能为空！");
            return;
        }

//        String invitationCode = this.mEtYaoQingMa.getText().toString().trim();
//        if (TextUtils.isEmpty(invitationCode)) {
//            showToast("邀请码不能为空！");
//            return;
//        }
//
//        String psd = this.mEtPsd.getText().toString().trim();
//        if (TextUtils.isEmpty(psd)) {
//            showToast("密码不能为空！");
//            return;
//        }
//        psd = StringUtils.getMD5(psd);
//
//        Map<String, String> allParam = new HashMap<>();
//        allParam.put("phone", editPhone.getText().toString());
//        allParam.put("password", psd);
//        allParam.put("invitationCode", invitationCode);

//        NetWork.getInstance()
//                .setTag(Qurl.improveUserInfo)
//                .getApiService(LoginApi.class)
//                .improveUserInfo(allParam)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new RxObserver<UsersBean>(SetPsdActivity.this) {
//
//                    @Override
//                    public void onSuccess(UsersBean result) {
//                        if (result == null) {
//                            return;
//                        }
//                        try {
//                            UserInfoBean userInfoBean = new UserInfoBean();
//                            userInfoBean.setIsLogin(true);
//                            userInfoBean.setId(result.user.id);
//                            userInfoBean.setIsActiveStatus(result.user.isAgencyStatus);
//                            userInfoBean.setSafeToken(result.user.safeToken);
//                            userInfoBean.setPhone(result.user.phone);
//                            userInfoBean.setPhoto(result.user.photo);
//                            userInfoBean.setWxPhoto(result.user.weixinPhoto);
//                            userInfoBean.setWxName(result.user.weixinName);
//                            userInfoBean.setInvitationCode(result.user.invitationCode);
//                            userInfoBean.setFirstLoginFlag(result.user.firstLoginFlag == 0 ? true : false);
//                            UserHelper.getInstence().saveUserInfo(userInfoBean);
//
//                            //登录成功，发送通知刷新界面
//                            EventBus.getDefault().post(Constant.LOGIN_SUCCESS);
//                            finish();
//                        }catch (Exception e){
//                            toast(e.toString());
//                        }
//
//
//                    }
//                });
    }


    @OnClick({R.id.btn_verify, R.id.action_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_verify://验证密码

                break;
            case R.id.action_btn://确认修改

                break;
        }
    }
}
