package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 申请直播
 */

public class LiveApplyActivity extends BaseActivity {


    @BindView(R.id.input_title)
    EditText inputTitle;
    @BindView(R.id.input_time)
    EditText inputTime;
    @BindView(R.id.input_content)
    EditText inputContent;
    @BindView(R.id.btn_apply)
    Button btnApply;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_apply;
    }

    @Override
    protected void init() {
        super.initHeadActionBar();
        setHeadActionTitle("申请直播");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.input_time, R.id.btn_apply})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.input_time:
                this.selectDate();
                break;
            case R.id.btn_apply:
                break;
        }
    }

    private void selectDate() {
        Calendar calendar = Calendar.getInstance();
        @SuppressLint("WrongConstant") DatePickerDialog dialog = new DatePickerDialog(this, (datePicker, year, month, day) -> {
            String desc = String.format("%d.%d.%d", year, month + 1, day);
            selectTime(desc);
        },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MARCH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }
    private void selectTime(String date){
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog dialog = new TimePickerDialog(this, (timePicker, hour, minute) -> {
            inputTime.setText( String.format("%s  %d:%d", date, hour,minute));
        },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true);
        dialog.show();
    }
}