package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoPlayActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 案例分析详情
 */
public class CaseAnalyseDetailActivity extends BaseActivity {

    @BindView(R.id.iv_open_video)
    ImageView ivOpenVideo;
    @BindView(R.id.t_info)
    TextView tInfo;
    @BindView(R.id.t_exhibition)
    TextView tExhibition;
    @BindView(R.id.t_result)
    TextView tResult;
    @BindView(R.id.tv_content)
    TextView tvContent;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_details;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("倒闸操作擅自解锁");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.iv_open_video, R.id.t_info, R.id.t_exhibition, R.id.t_result})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_open_video:
				IntentUtils.startActivity(CaseAnalyseDetailActivity.this, VideoPlayActivity.class);
                break;
            case R.id.t_info:

                break;
            case R.id.t_exhibition:

                break;
            case R.id.t_result:

                break;
        }
    }
}
