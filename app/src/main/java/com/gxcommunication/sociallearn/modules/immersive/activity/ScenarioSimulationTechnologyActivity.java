package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.adapter.SimulationMeterToolsAdapter;
import com.gxcommunication.sociallearn.modules.immersive.adapter.SimulationTechnologyAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author hxc
 *
 * 场景模拟列 技术资料
 */
public class ScenarioSimulationTechnologyActivity extends BaseActivity {

    @BindView(R.id.scenario_simulation_title_tv)
    TextView scenarioSimulationTitleTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.scenario_simulation_tips_tv)
    TextView scenarioSimulationTipsTv;
    @BindView(R.id.scenario_simulation_action_tv)
    TextView scenarioSimulationActionTv;


    SimulationTechnologyAdapter mAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_danger_analyse;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("技术资料");

        scenarioSimulationTitleTv.setText("请准备如下技术资料：");
        scenarioSimulationTipsTv.setText("提示：确认相关的技术资料后，开始进行危险点分析和预防措施。");
        scenarioSimulationActionTv.setText("提交");


        mAdapter = new SimulationTechnologyAdapter(this, DateCacheUtil.getRequestList(10));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick(R.id.scenario_simulation_action_tv)
    public void onViewClicked() {
        IntentUtils.startActivity(this,ScenarioSimulationDangerAnalyseActivity.class);
        finish();
    }

}
