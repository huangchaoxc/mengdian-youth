package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 考试须知
 */
public class ExamRoomRemindActivity extends BaseActivity {


    @BindView(R.id.exam_room_plate_tv)
    TextView examRoomPlateTv;//专业
    @BindView(R.id.exam_room_category_tv)
    TextView examRoomCategoryTv;//分类
    @BindView(R.id.exam_room_level_tv)
    TextView examRoomLevelTv;//等级
    @BindView(R.id.exam_room_work_type_tv)
    TextView examRoomWorkTypeTv;//工种

    @BindView(R.id.exam_room_radio_tv)
    TextView examRoomRadioTv;
    @BindView(R.id.exam_room_multple_tv)
    TextView examRoomMultpleTv;
    @BindView(R.id.exam_room_judge_tv)
    TextView examRoomJudgeTv;
    @BindView(R.id.exam_room_compute_tv)
    TextView examRoomComputeTv;
    @BindView(R.id.exam_room_collect_tv)
    TextView examRoomCollectTv;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_exam_room_remind;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("考试须知");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.exam_remind_btn)
    public void onViewClicked() { //开始考试
        IntentUtils.startActivity(this, ExamTopicActivity.class);
    }

    @OnClick({R.id.exam_room_error_tv, R.id.exam_room_collect_tv, R.id.exam_room_record_tv, R.id.exam_room_rank_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exam_room_error_tv:
                IntentUtils.startActivity(ExamRoomRemindActivity.this, StudyRoomHistoryTopicActivity.class);
                break;
            case R.id.exam_room_collect_tv:
                IntentUtils.startActivity(ExamRoomRemindActivity.this, StudyRoomHistoryTopicActivity.class);
                break;
            case R.id.exam_room_record_tv:
                IntentUtils.startActivity(ExamRoomRemindActivity.this, ExamRoomHistoryActivity.class);
                break;
            case R.id.exam_room_rank_tv:
                IntentUtils.startActivity(ExamRoomRemindActivity.this, ExamRankingActivity.class);
                break;
        }
    }
}
