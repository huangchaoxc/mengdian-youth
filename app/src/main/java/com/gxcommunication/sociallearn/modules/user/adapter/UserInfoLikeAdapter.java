package com.gxcommunication.sociallearn.modules.user.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class UserInfoLikeAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public UserInfoLikeAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_user_info_like_layout, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
//		helper.setText(R.id.tvTitle,"");
	}
}
