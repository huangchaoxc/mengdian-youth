package com.gxcommunication.sociallearn.modules.main_index;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.modules.immersive.activity.ImmersiveMainActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyForumActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyStoreActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyTaskActivity;
import com.gxcommunication.sociallearn.modules.main_index.adapter.HomeGuideTypeAdapter;
import com.gxcommunication.sociallearn.modules.main_index.adapter.HomeIndexNewsAdapter;
import com.gxcommunication.sociallearn.modules.main_index.bean.HomeIndexBannerListBean;
import com.gxcommunication.sociallearn.modules.main_index.bean.HomeIndexNewsListBean;
import com.gxcommunication.sociallearn.modules.main_my.activity.TopRankingActivity;
import com.gxcommunication.sociallearn.modules.main_pk.activity.PKRoomActivity;
import com.gxcommunication.sociallearn.modules.main_pk.activity.PKRoomSchemaActivity;
import com.gxcommunication.sociallearn.modules.main_teacher.activity.ApplyTeacherActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.SelfEvaluateActivity;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity.ExhibitionHallActivity;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity.ExhibitionHallResultListActivity;
import com.gxcommunication.sociallearn.modules.study.live.activity.LiveCourseActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoCourseActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoTabListActivity;
import com.gxcommunication.sociallearn.modules.user.activity.LoginActivity;
import com.gxcommunication.sociallearn.modules.user.activity.RegisterActivity;
import com.gxcommunication.sociallearn.utils.BannerGlideImageResLoader;
import com.gxcommunication.sociallearn.utils.ImageManager;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.scwang.smartrefresh.layout.util.DensityUtil;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/8/14.
 * 首页 学习
 */

public class MainIndexFragment extends BaseFragment {
    @BindView(R.id.home_head_banner)
    Banner homeHeadBanner;
    @BindView(R.id.home_head_program_rv)
    RecyclerView homeHeadProgramRv;
    @BindView(R.id.home_head_news_rv)
    RecyclerView homeHeadNewsRv;
    @BindView(R.id.home_index_tabLayout)
    TabLayout homeIndexTabLayout;
    private View mView;

    private HomeIndexNewsAdapter mNewsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_main_index, null);
        }
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mView.getParent() != null && mView != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
    }

    @Override
    protected void initView() {
        initTabLayout();

        mView.findViewById(R.id.home_game_pay_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startActivity(getActivity(), PKRoomActivity.class);

            }
        });
    }

    @Override
    protected void lazyLoad() {
        loadBanner();
        loadProgramInfo();
        loadNewsInfo();
        showDialog();
    }


//    @OnClick({R.id.home_head_message_iv})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//
//            case R.id.home_head_message_iv:
//                IntentUtils.startActivity(getActivity(), RegisterActivity.class);
//                break;
//
//        }
//    }

    private void initTabLayout() {

        homeIndexTabLayout.addTab(homeIndexTabLayout.newTab().setText("专业技能"));
        homeIndexTabLayout.addTab(homeIndexTabLayout.newTab().setText("思想政治"));
        homeIndexTabLayout.addTab(homeIndexTabLayout.newTab().setText("生活情感"));

        homeIndexTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        homeIndexTabLayout.setTabMode(TabLayout.MODE_FIXED);

        homeIndexTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void loadBanner() {

        List<Object> bannerList = new ArrayList<>();
        bannerList.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2275380823,3645675973&fm=26&gp=0.jpg");
        bannerList.add(R.mipmap.main_banner);
        bannerList.add(R.drawable.ic_circle_user_default);
        bannerList.add(R.mipmap.main_banner);
        bannerList.add(R.mipmap.main_banner);

        homeHeadBanner.isAutoPlay(true);//是否自动滚动
        homeHeadBanner.setDelayTime(5000);//滚动间隔时间

        homeHeadBanner.setImageLoader(new BannerGlideImageResLoader());
        homeHeadBanner.setImages(bannerList);
        homeHeadBanner.start();

        homeHeadBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {

            }
        });
//        ImageManager.getManager(getActivity()).loadUrlImage("https://dss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3566088443,3713209594&fm=26&gp=0.jpg",);
    }

    private void loadProgramInfo() {
        List<HomeIndexBannerListBean.ResultBean> dataList = new ArrayList<>();
        for (int i = 0; i < 14; i++) {

            dataList.add(new HomeIndexBannerListBean.ResultBean());
        }

        homeHeadProgramRv.setLayoutManager(new GridLayoutManager(getActivity(), 5));
        HomeGuideTypeAdapter madapter = new HomeGuideTypeAdapter(getActivity(), dataList);
        homeHeadProgramRv.setAdapter(madapter);

        madapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                switch (position) {
                    case 0://学习任务
                        IntentUtils.startActivity(getActivity(), StudyTaskActivity.class);
                        break;
                    case 1://创新成果
//                        IntentUtils.startActivity(getActivity(), ExhibitionHallActivity.class);
                        IntentUtils.startActivity(getActivity(), ExhibitionHallResultListActivity.class);
                        break;
                    case 2://视频课程
//                        IntentUtils.startActivity(getActivity(), VideoTabListActivity.class); 旧的视频课程
                        IntentUtils.startActivity(getActivity(), VideoCourseActivity.class);
                        break;
                    case 3://直播课程
                        IntentUtils.startActivity(getActivity(), LiveCourseActivity.class);
                        break;
                    case 4://学创论坛
                        IntentUtils.startActivity(getActivity(), StudyForumActivity.class);
                        break;
                    case 5://身临其境
                        IntentUtils.startActivity(getActivity(), ImmersiveMainActivity.class);
                        break;
                    case 6://名师堂
                        IntentUtils.startActivity(getActivity(), ApplyTeacherActivity.class);
                        break;
                    case 7://知识库
                        IntentUtils.startActivity(getActivity(), StudyStoreActivity.class);
                        break;
                    case 8://学习小组
//                        IntentUtils.startActivity(getActivity(), SelfEvaluateActivity.class);

                    case 9://自我提升
                        IntentUtils.startActivity(getActivity(), SelfEvaluateActivity.class);
                        break;
                    case 10://PK
                        IntentUtils.startActivity(getActivity(), PKRoomActivity.class);
                        break;
                    case 11://top
                        IntentUtils.startActivity(getActivity(), TopRankingActivity.class);
                        break;
                }
            }
        });
    }


    private void loadNewsInfo() {
        List<HomeIndexNewsListBean.ResultBean> dataList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {

            dataList.add(new HomeIndexNewsListBean.ResultBean());
        }
        mNewsAdapter = new HomeIndexNewsAdapter(getActivity(), dataList);
        homeHeadNewsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        homeHeadNewsRv.setAdapter(mNewsAdapter);
    }


    private void showDialog() {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_home_index_layout, null);
        AlertDialog builder = new AlertDialog.Builder(getActivity(), R.style.NobackDialog).create();
        builder.setView(view);
        builder.setCancelable(true);
        view.findViewById(R.id.iv_colose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        builder.show();
    }

}
