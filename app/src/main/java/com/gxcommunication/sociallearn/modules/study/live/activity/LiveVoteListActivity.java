package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveVoteListAdapter;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteItemBean;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/9/20.
 */

public class LiveVoteListActivity extends BaseActivity {

	public static final String INTENT_KEY_IS_HOST = "is_host";//是否为主持人

	private Boolean isLiveHost = false;

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.action_layout)
    LinearLayout actionLayout;

	LiveVoteListAdapter mAdapter;

    public static List<LiveVoteItemBean> getRequestList(){

        List<LiveVoteItemBean> list = new ArrayList<>();
        list.add(new LiveVoteItemBean("吃肉会不会胖？",0));
        list.add(new LiveVoteItemBean("最喜欢吃的蔬菜",0));
        list.add(new LiveVoteItemBean("团建活动的选择",1));
        return list;
    }

    public static LiveVoteCreateAndDetailBean getRequestDetail1(){

        LiveVoteCreateAndDetailBean mLiveVoteCreateBean = new LiveVoteCreateAndDetailBean();
        mLiveVoteCreateBean.title = "吃肉会不会胖？";
        mLiveVoteCreateBean.content = "肉肉这么好吃，能否多吃呢";
        mLiveVoteCreateBean.voteModeCode = 0;
        mLiveVoteCreateBean.statusCode = 0;
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("会长胖的"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("不会长胖的，都是蛋白质"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("与我无关！看心情吃"));
        return mLiveVoteCreateBean;
    }

    public static LiveVoteCreateAndDetailBean getRequestDetail2(){

        LiveVoteCreateAndDetailBean mLiveVoteCreateBean = new LiveVoteCreateAndDetailBean();
        mLiveVoteCreateBean.title = "最喜欢吃的蔬菜？";
        mLiveVoteCreateBean.content = "多吃蔬菜身体健康";
        mLiveVoteCreateBean.voteModeCode = 1;
        mLiveVoteCreateBean.statusCode = 0;
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("带绿叶的蔬菜：空心菜、菠菜、生菜等"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("富含纤维的南瓜、白菜、花菜"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("根菜类的胡萝卜、红薯、土豆、芋头等等"));
        return mLiveVoteCreateBean;
    }

    public static LiveVoteCreateAndDetailBean getRequestDetail3(){

        LiveVoteCreateAndDetailBean mLiveVoteCreateBean = new LiveVoteCreateAndDetailBean();
        mLiveVoteCreateBean.title = "团建活动的选择？";
        mLiveVoteCreateBean.content = "团队活动有益身心";
        mLiveVoteCreateBean.voteModeCode = 0;
        mLiveVoteCreateBean.statusCode = 1;
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("户外活动登山、踏青、郊游农家乐等等"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("团体聚餐：KTV、烧烤、野炊"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("户外拓展，团队游戏"));
        mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean("不想去，我就想静静的待着什么也不干！！！"));
        return mLiveVoteCreateBean;
    }

	@Override
    protected int getLayoutId() {
        return R.layout.activity_live_vote_list;
    }

    @Override
    protected void init() {
        super.initHeadActionBar();
        setHeadActionTitle("投票");

		isLiveHost = getIntent().getBooleanExtra(INTENT_KEY_IS_HOST,false);
		if (isLiveHost){//主持人
			actionLayout.setVisibility(View.VISIBLE);
		}else {
			actionLayout.setVisibility(View.GONE);
		}

        mAdapter = new LiveVoteListAdapter(this, getRequestList());
		recyclerview.setLayoutManager(new LinearLayoutManager(this));
		recyclerview.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position == 0){//模拟单选
                    IntentUtils.startActivityForString(LiveVoteListActivity.this, LiveVoteProceedDetailActivity.class,
                            LiveVoteProceedDetailActivity.INTENT_KEY_CONTENT, GsonUtlils.objectToJson(getRequestDetail1()));
                }else  if (position == 1){//模拟多选
                    IntentUtils.startActivityForString(LiveVoteListActivity.this, LiveVoteProceedDetailActivity.class,
                            LiveVoteProceedDetailActivity.INTENT_KEY_CONTENT, GsonUtlils.objectToJson(getRequestDetail2()));
                }else{//已完成的情况下，进入投票结果页面
                    IntentUtils.startActivityForString(LiveVoteListActivity.this, LiveVoteResultActivity.class,
                            LiveVoteResultActivity.INTENT_KEY_CONTENT, GsonUtlils.objectToJson(getRequestDetail3()));
                }
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {
        IntentUtils.startActivity(LiveVoteListActivity.this,LiveVoteCreateActivity.class);
    }


}
