package com.gxcommunication.sociallearn.modules.user.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 */
public class UserNewhandFriendDeclarationPreviewActivity extends BaseActivity {


    @BindView(R.id.user_info_nickname_tv)
    TextView userInfoNicknameTv;//用户昵称
    @BindView(R.id.user_info_userinfo_tv)
    TextView userInfoTv;//个人信息
    @BindView(R.id.user_info_tag_tv)
    TextView userInfoTagTv;// 标签
    @BindView(R.id.user_info_head_iv)
    ImageView userInfoHeadIv;//头像
    @BindView(R.id.user_newhand_introl_edit)
    TextView userNewhandIntrolTv;//交友宣言

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_newhand_friend_declaration_preview;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        setHeadActionTitle("交友宣言预览");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {
    }
}
