package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAnswerDetailAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 *
 * 学创论坛 子栏目  你问我答 评论
 */
public class StudyAnswersCommentActivity extends BaseActivity {

    StudyShareAnswerDetailAdapter mAdapter;
    @BindView(R.id.study_share_category_tv)
    TextView studyShareCategoryTv; //栏目名称
    @BindView(R.id.study_share_status_tv)
    TextView studyShareStatusTv; // 状态，已完结 未完结
    @BindView(R.id.study_share_award_tv)
    TextView studyShareAwardTv; // 悬赏分数
    @BindView(R.id.study_share_title_tv)
    TextView studyShareTitleTv; // 问答标题
    @BindView(R.id.study_share_username_tv)
    TextView studyShareUsernameTv; // 用户名
    @BindView(R.id.study_share_time_tv)
    TextView studyShareTimeTv; // 回答时间
    @BindView(R.id.study_share_read_tv)
    TextView studyShareReadTv; // 阅读数
    @BindView(R.id.study_share_star_tv)
    TextView studyShareStarTv; // 点赞数
    @BindView(R.id.study_share_commit_tv)
    TextView studyShareCommitTv;// 回复数量
    @BindView(R.id.study_share_collect_tv)
    TextView studyShareCollectTv;// 收藏数量
    @BindView(R.id.send_message_edit)
    EditText sendMessageEdit;// 评论内容

    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_answers_comment;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("提交问答");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick(R.id.action_btn)
    public void onViewClicked() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
