package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.MyStudyAnswerCollectActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveCourseAdapter;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoDetailsActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 视频课程
 */
public class LiveCourseActivity extends BaseActivity {

    private int Select_MODE_TYPE = 0; //  0是正在直播 1是马上开播 2直播结束


    @BindView(R.id.head_action_both_rightimage)
    ImageView head_action_both_rightimage;
    @BindView(R.id.head_action_both_add_iv)
    ImageView head_action_both_add_iv;

    @BindView(R.id.category_pop_tv)
    TextView categoryPoptv;

    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView studyCategotyRecyclerview;
    @BindView(R.id.study_recyclerview)
    RecyclerView studyRecyclerview;

    StudyGridCategoryAdapter mSecondCategoryAdapter;
    LiveCourseAdapter mAdapter;

    @BindView(R.id.ed_search)
    EditText edSearch;

    @BindView(R.id.sort_hot_iv)
    ImageView sortHotIv;
    @BindView(R.id.sort_new_iv)
    ImageView sortNewIv;

    @BindView(R.id.live_courese_living_tv)
    TextView liveCoureseLivingTv;
    @BindView(R.id.live_courese_preparelive_tv)
    TextView liveCouresePrepareliveTv;
    @BindView(R.id.live_courese_endlive_tv)
    TextView liveCoureseEndliveTv;

    private StudyCategoryPopupWindow mCategoryPopupWindow;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_courese;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBothTitle();
        setHeadActionBothTitle("直播课程", "");

        head_action_both_add_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startActivity(LiveCourseActivity.this, LiveApplyActivity.class);
            }
        });

        head_action_both_rightimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startActivity(LiveCourseActivity.this, MyStudyAnswerCollectActivity.class);
            }
        });


        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(10));
        studyCategotyRecyclerview.setLayoutManager(new GridLayoutManager(this, 5));
        studyCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);

        mAdapter = new LiveCourseAdapter(this, DateCacheUtil.getRequestList(2));
        studyRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        studyRecyclerview.setAdapter(mAdapter);

        mSecondCategoryAdapter.setSelectItem(0);
        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });

        mSecondCategoryAdapter.setSelectItem(0);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (position != 0){
                    showToast("暂未开放直播课程，敬请期待");
                    return;
                }

                if (Select_MODE_TYPE == 0) {

                    Intent  intent = new Intent(LiveCourseActivity.this, LiveDetailsActivity.class);
                    intent.putExtra(LiveDetailsActivity.INTENT_KEY_STATUS, 1);
                     startActivity(intent);
                } else if (Select_MODE_TYPE == 1) {
                    Intent  intent = new Intent(LiveCourseActivity.this, LiveDetailsActivity.class);
                    intent.putExtra(LiveDetailsActivity.INTENT_KEY_STATUS, 2);
                    startActivity(intent);

                } else if (Select_MODE_TYPE == 2) {

                    Intent  intent = new Intent(LiveCourseActivity.this, LiveDetailsActivity.class);
                    intent.putExtra(LiveDetailsActivity.INTENT_KEY_STATUS, 3);
                    startActivity(intent);
                }
            }
        });

        categoryPoptv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopupwinds();
            }
        });

        mCategoryPopupWindow = new StudyCategoryPopupWindow(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

    }


    private void showPopupwinds() {
        mCategoryPopupWindow.showAsDropDown(categoryPoptv);

    }


    @OnClick({R.id.sort_hot_iv, R.id.sort_new_iv})
    public void onViewSortClicked(View view) {
        switch (view.getId()) {
            case R.id.sort_hot_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_select);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_normal);
                break;
            case R.id.sort_new_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_normal);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_select);
                break;
        }
    }


    private void changSelectBarStyle() {

        if (Select_MODE_TYPE == 0) {
            liveCoureseLivingTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            liveCouresePrepareliveTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            liveCoureseEndliveTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);

        } else if (Select_MODE_TYPE == 1) {
            liveCoureseLivingTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            liveCouresePrepareliveTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            liveCoureseEndliveTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);

        } else if (Select_MODE_TYPE == 2) {
            liveCoureseLivingTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            liveCouresePrepareliveTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            liveCoureseEndliveTv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.live_courese_living_tv, R.id.live_courese_preparelive_tv, R.id.live_courese_endlive_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.live_courese_living_tv:
                Select_MODE_TYPE = 0;
                changSelectBarStyle();
                break;
            case R.id.live_courese_preparelive_tv:
                Select_MODE_TYPE = 1;
                changSelectBarStyle();
                break;
            case R.id.live_courese_endlive_tv:
                Select_MODE_TYPE = 2;
                changSelectBarStyle();
                break;
        }
    }
}
