package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.base.CommonViewPagerAdapter;
import com.gxcommunication.sociallearn.modules.study.live.fragment.LiveAnchorFragment;
import com.gxcommunication.sociallearn.modules.study.live.fragment.LiveCommentFragment;
import com.gxcommunication.sociallearn.modules.study.live.fragment.LiveIntroduceFragment;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * hgc
 * （学习课堂）视频详情页
 */
public class LiveDetailsActivity extends BaseActivity {

    private final static String[] TABS = new String[]{"详情", "互动", "主播"};

    public final static String INTENT_KEY_STATUS = "key_status";//直播状态

    @BindView(R.id.live_detail_top_pic_iv)
    ImageView liveDetailTopPicIv;
    @BindView(R.id.live_detail_open_tips_iv)
    TextView liveDetailOpenTipsTv;
    @BindView(R.id.live_detail_open_time_iv)
    TextView liveDetailOpenTimeTv;
    @BindView(R.id.live_detail_open_layout)
    LinearLayout liveDetailOpenLayout;

    private int Live_Status = 1;// 1 正在直播 ，2 即将开始 3 ，直播结束

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_details;
    }

    @Override
    protected void init() {
        initTab();
    }

    private void initTab() {
        super.initHeadActionBar();
        setHeadActionTitle("课程详情");

        Live_Status = getIntent().getIntExtra(INTENT_KEY_STATUS, 1);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(LiveIntroduceFragment.getInstance(""));//这里可以传入课程id，然后进入详情根据id获取课程的详细信息
        fragments.add(LiveCommentFragment.getInstance(""));
        fragments.add(LiveAnchorFragment.getInstance(""));


        for (int i = 0; i < TABS.length; i++) {
            tabLayout.addTab(tabLayout.newTab());
        }
        viewPager.setAdapter(new CommonViewPagerAdapter(getSupportFragmentManager(), TABS, fragments));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);


        loadLiveStatInfo();

    }

    private void loadLiveStatInfo() {
        if (Live_Status == 3) { // 直播结束
            liveDetailOpenLayout.setVisibility(View.VISIBLE);
            liveDetailOpenTipsTv.setVisibility(View.GONE);

            liveDetailTopPicIv.setVisibility(View.GONE);

            liveDetailOpenTimeTv.setText("直播已结束，敬请期待下一场课程！");
        } else if (Live_Status == 2) { // 直播未开始
            liveDetailOpenLayout.setVisibility(View.VISIBLE);
            liveDetailTopPicIv.setVisibility(View.GONE);
        } else {
            liveDetailOpenLayout.setVisibility(View.GONE);
            liveDetailTopPicIv.setVisibility(View.VISIBLE);
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.live_detail_top_pic_iv)
    public void onViewClicked() {

        IntentUtils.startActivity(this, LiveMeetingPlayerActivity.class);

    }
}