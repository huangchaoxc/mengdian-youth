package com.gxcommunication.sociallearn.modules.splash;


import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.splash.bean.SplashAdvertBean;
import com.gxcommunication.sociallearn.utils.ImageManager;

import java.util.Timer;
import java.util.TimerTask;

public class AdverActivity extends BaseActivity implements View.OnClickListener {

    public static final String INTENT_KEY_DATA = "KEY_DATA";
    private TextView mTvAdverTimer;
    private int count = 5;
    private Timer mTimer;
    private SplashAdvertBean.ResultBean mMAdverStringBean;
    private ImageView mIvAdver;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_adver;
    }

    @Override
    protected void init() {
        findViews();
        initData();
    }

    protected void findViews() {
        count = 4;
        mIvAdver = $(R.id.iv_adver);
        mTvAdverTimer = findViewById(R.id.tv_adver_timer);
        mTvAdverTimer.setOnClickListener(this);
        findViewById(R.id.ll_adver_timer).setOnClickListener(this);
        mIvAdver.setOnClickListener(this);
    }

    protected void initData() {
        mMAdverStringBean = (SplashAdvertBean.ResultBean) getIntent().getSerializableExtra(INTENT_KEY_DATA);
        ImageManager.getManager(this).loadUrlFitImage(mMAdverStringBean.getAdImage(),mIvAdver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (count >= 0) {
                            mTvAdverTimer.setText(count + "s");
                        }
                        count--;
                        if (count < 1) {
                            startActivity();
                        }
                    }
                });
            }
        }, 0, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }


    private void startActivity() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }

//        Intent intent = new Intent(this, MainFragmentTabActivity.class);
//        startActivity(intent);
//        finish();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ll_adver_timer) {
            startActivity();
        } else if (id == R.id.iv_adver)  {

            if (mTimer != null) {
                mTimer.cancel();
                mTimer = null;
            }
//            Intent intent = new Intent(AdverActivity.this,MainFragmentTabActivity.class);
//            startActivity(intent);
            if (mMAdverStringBean.getType() == 0) {
//                ARouter.getInstance().build(ARoutersConstant.PATH_COMMOM_WEB)
//                        .withString("loadUrl", mMAdverStringBean.getAdUrl())
//                        .withInt(AppConstant.TYPE, 4)
//                        .navigation(this, new NavigationCallback() {
//                            @Override
//                            public void onFound(Postcard postcard) {
//
//                            }
//
//                            @Override
//                            public void onLost(Postcard postcard) {
//
//                            }
//
//                            @Override
//                            public void onArrival(Postcard postcard) {
//                                finish();
//                            }
//
//                            @Override
//                            public void onInterrupt(Postcard postcard) {
//
//                            }
//                        });
            } else if (mMAdverStringBean.getType() == 1) {


            }

        }
    }
}
