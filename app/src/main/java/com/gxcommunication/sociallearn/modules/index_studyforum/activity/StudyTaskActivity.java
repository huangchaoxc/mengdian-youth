package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyTaslListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 老师主页  领取任务
 */
public class StudyTaskActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    StudyTaslListAdapter mAdapter;
    @BindView(R.id.study_task_everyday)
    TextView studyTaskEveryday;
    @BindView(R.id.study_task_growup)
    TextView studyTaskGrowup;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_task;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        initHeadActionBar();
        setHeadActionTitle("学习任务");
        mAdapter = new StudyTaslListAdapter(this, DateCacheUtil.getRequestList(10));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @OnClick({R.id.study_task_everyday, R.id.study_task_growup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.study_task_everyday://默认选中每日任务 颜色深的是选中
                studyTaskEveryday.setBackgroundColor(getResources().getColor(R.color.color_head));
                studyTaskGrowup.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                break;
            case R.id.study_task_growup:

                studyTaskEveryday.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                studyTaskGrowup.setBackgroundColor(getResources().getColor(R.color.color_head));
                break;
        }
    }
}
