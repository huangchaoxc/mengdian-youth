package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.modules.main_pk.adapter.PKRoomAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.SelfEvaluateAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.widget.BetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * PK  竞技场
 */
public class PKRoomActivity extends BaseActivity {

    @BindView(R.id.pk_recyclerview)
    RecyclerView pkRecyclerview;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.study_profession_tv)
    BetterSpinner studyProfessionSpinner;// 专业
    @BindView(R.id.study_work_type_tv)
    BetterSpinner studyWorkTypeSpinner;//工种

    @BindView(R.id.study_level_tv)
    BetterSpinner studyLevelpinner;//等级

    PKRoomAdapter mAdapter;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_pk_room;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("PK 竞技场");

        mAdapter = new PKRoomAdapter(this, DateCacheUtil.getRequestList(9));
        pkRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        pkRecyclerview.setAdapter(mAdapter);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab().setText("技能鉴定"));
        tabLayout.addTab(tabLayout.newTab().setText("安规"));
        tabLayout.addTab(tabLayout.newTab().setText("公共安全"));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {

                } else if (tab.getPosition() == 1) {

                } else {

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        loadSpinner();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.pk_room_record_tv, R.id.pk_room_rank_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.pk_room_record_tv:
                IntentUtils.startActivity(this, PKHistoryActivity.class);
                break;
            case R.id.pk_room_rank_tv:
                IntentUtils.startActivity(this, PKRankingActivity.class);
                break;
        }
    }

    @OnClick({R.id.layout_px_solo, R.id.layout_px_team})
    public void onView1Clicked(View view) {
        switch (view.getId()) {
            case R.id.layout_px_solo:
                IntentUtils.startActivity(this, PKSoloRemindActivity.class);
                break;
            case R.id.layout_px_team:
                IntentUtils.startActivity(this, PKTeamRemindActivity.class);
                break;
        }
    }

    private void loadSpinner() {
        //专业下拉框
        String[] listProfession = getResources().getStringArray(R.array.self_evaluate_profression_sponner);
        ArrayAdapter<String> professionAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listProfession);
        studyProfessionSpinner.setAdapter(professionAdapter);
        studyProfessionSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        //工种下拉框
        String[] listType = getResources().getStringArray(R.array.self_evaluate_type_sponner);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listType);
        studyWorkTypeSpinner.setAdapter(typeAdapter);
        studyWorkTypeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        //等级下拉框
        String[] levelype = getResources().getStringArray(R.array.self_evaluate_level_sponner);
        ArrayAdapter<String> levelAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, levelype);
        studyLevelpinner.setAdapter(levelAdapter);
        studyLevelpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }
}
