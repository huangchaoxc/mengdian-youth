package com.gxcommunication.sociallearn.modules.splash.bean;


import com.gxcommunication.sociallearn.base.bean.BaseBean;

import java.util.List;

public class SplashGuideBean extends BaseBean {


    /**
     * success : true
     * message : 查询成功
     * code : 200
     * timestamp : 1572609781919
     * result : [{"startImg":"qn|taoyouji2|57e79a3ad2f4de18551c104e73032979","startImage":"http://img.taoyouji666.com/57e79a3ad2f4de18551c104e73032979?imageView2/2/w/480/q/90","type":7,"orderNum":1},{"startImg":"qn|taoyouji2|5a10094214184885001de2f90c94d13d","startImage":"http://img.taoyouji666.com/5a10094214184885001de2f90c94d13d?imageView2/2/w/480/q/90","type":7,"orderNum":2},{"startImg":"qn|taoyouji2|bdc1f7d7088a4710fbb48f61138f932c","startImage":"http://img.taoyouji666.com/bdc1f7d7088a4710fbb48f61138f932c?imageView2/2/w/480/q/90","type":7,"orderNum":3},{"startImg":"qn|taoyouji2|10baa69dfa414f39d9c106c142bca43e","startImage":"http://img.taoyouji666.com/10baa69dfa414f39d9c106c142bca43e?imageView2/2/w/480/q/90","type":7,"orderNum":4}]
     * count : 0
     */

    private boolean success;
    private String message;
    private int code;
    private long timestamp;
    private int count;
    private List<ResultBean> result;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ResultBean> getResult() {
        return result;
    }

    public void setResult(List<ResultBean> result) {
        this.result = result;
    }

    public static class ResultBean {
        /**
         * startImg : qn|taoyouji2|57e79a3ad2f4de18551c104e73032979
         * startImage : http://img.taoyouji666.com/57e79a3ad2f4de18551c104e73032979?imageView2/2/w/480/q/90
         * type : 7
         * orderNum : 1
         */

        private String startImg;
        private String startImage;
        private int type;
        private int orderNum;

        public String getStartImg() {
            return startImg;
        }

        public void setStartImg(String startImg) {
            this.startImg = startImg;
        }

        public String getStartImage() {
            return startImage;
        }

        public void setStartImage(String startImage) {
            this.startImage = startImage;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(int orderNum) {
            this.orderNum = orderNum;
        }
    }
}
