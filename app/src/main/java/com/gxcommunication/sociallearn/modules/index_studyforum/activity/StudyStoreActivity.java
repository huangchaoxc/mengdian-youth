package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.FirstMenuAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.SecondMenuAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 */
public class StudyStoreActivity extends BaseActivity {

    @BindView(R.id.firstMenuView)
    RecyclerView firstMenuView;
    @BindView(R.id.secondMenuView)
    RecyclerView secondMenuView;
    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView studyCategotyRecyclerview;

	private FirstMenuAdapter firstMenuAdapter;
	private SecondMenuAdapter secondMenuAdapter;

    private StudyGridCategoryAdapter mSecondCategoryAdapter;

    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.sort_hot_iv)
    ImageView sortHotIv;
    @BindView(R.id.sort_new_iv)
    ImageView sortNewIv;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_store;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("知识库");

        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(10));
        studyCategotyRecyclerview.setLayoutManager(new GridLayoutManager(this, 3));
        studyCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);

        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });
        mSecondCategoryAdapter.setSelectItem(0);

		firstMenuView.setLayoutManager(new LinearLayoutManager(this));
		secondMenuView.setLayoutManager(new LinearLayoutManager(this));
		firstMenuAdapter = new FirstMenuAdapter(this, DateCacheUtil.getRequestList(10));
		secondMenuAdapter = new SecondMenuAdapter(this, DateCacheUtil.getRequestList(10));

		firstMenuView.setAdapter(firstMenuAdapter);
		secondMenuView.setAdapter(secondMenuAdapter);

		firstMenuAdapter.setCheckPos(0);
        firstMenuAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                firstMenuAdapter.setCheckPos(position);
            }
        });

        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.sort_hot_iv, R.id.sort_new_iv})
    public void onViewSortClicked(View view) {
        switch (view.getId()) {
            case R.id.sort_hot_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_select);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_normal);
                break;
            case R.id.sort_new_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_normal);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_select);
                break;
        }
    }
}
