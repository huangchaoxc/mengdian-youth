package com.gxcommunication.sociallearn.modules.index_studyforum.bean;

public class StudyAnswerDetailBean {
    public StudyAnswerDetailBean(String category, String title, String userName, String awardScore, String createTime,String userId,  int isAccept) {
        this.category = category;
        this.title = title;
        this.userName = userName;
        this.awardScore = awardScore;
        this.createTime = createTime;
        this.isAccept = isAccept;
        this.userId = userId;
    }

    public String id;
    public String category;
    public String title;//
    public String userName;
    public String awardScore;//奖励分数
    public String createTime;//时间

    public String userId;//
    public int    isAccept;//是否采纳  0 是已采纳， 1是未采纳
}
