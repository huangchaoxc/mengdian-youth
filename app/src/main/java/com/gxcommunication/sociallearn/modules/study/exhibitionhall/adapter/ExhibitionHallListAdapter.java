package com.gxcommunication.sociallearn.modules.study.exhibitionhall.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * 专业列表适配器
 * Created by hxc on 2017/8/7
 */

public class ExhibitionHallListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    private static String[] colors=new String[]{
      "#F6EAB2","#E9F6E0","#E0F67A","#F4B9F6","#87F660","#33F6BD"
    };
    private static String[] title=new String[]{
            "化学","变电运行与检修","输电运行与检修","电力调度","营业用电","安装"
    };

    public ExhibitionHallListAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_exhibition_hall, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
       int positon=  helper.getAdapterPosition()%6;
       View view= helper.getView(R.id.layout_box);
       helper.setText(R.id.title,title[positon]);
       view.setBackground(getItemBg(colors[positon]));
    }

    private Drawable getItemBg(String color){

        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE); // 画框
        drawable.setCornerRadius(12);
        drawable.setStroke(4,Color.parseColor("#886CBF")); // 边框粗细及颜色
        drawable.setColor(Color.parseColor(color)); // 边框内部颜色
        return drawable;
    }

	/**
	 * 获取格式化内容
	 * @param id
	 * @param formats
	 * @return
	 */
    private String getFormat(int id,Object ...formats){
		return mContext.getResources().getString(id, formats);
	}
}
