package com.gxcommunication.sociallearn.modules.study.live.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 视频详情-视频简介
 */
public class LiveIntroduceFragment extends BaseFragment {


    @BindView(R.id.live_introl_titie_tv)
    TextView liveIntrolTitieTv;//标题
    @BindView(R.id.live_introl_time_tv)
    TextView liveIntrolTimeTv;//时间
    @BindView(R.id.live_introl_content_tv)
    TextView liveIntrolContentTv;//简要
    @BindView(R.id.live_introl_detail_tv)
    TextView liveIntrolDetailTv;//详情
    private View mView;


    private static final String ARG_VIDEOID = "params_video_id";

    public static LiveIntroduceFragment getInstance(String videoId) {
        LiveIntroduceFragment fragment = new LiveIntroduceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_VIDEOID, videoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_live_introduce, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }


    @Override
    protected void initView() {

    }

    @Override
    protected void lazyLoad() {

    }
}
