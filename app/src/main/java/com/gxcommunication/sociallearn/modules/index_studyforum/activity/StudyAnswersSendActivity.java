package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.utils.DateUtils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.qqtheme.framework.picker.DateTimePicker;

/**
 * @author hxc
 * 学创论坛 子栏目  你问我答   发布
 */
public class StudyAnswersSendActivity extends BaseActivity {

    @BindView(R.id.study_share_send_title_edit)
    EditText studyShareSendTitleEdit;
    @BindView(R.id.study_categoty_zy)
    TextView studyCategotyZy;
    @BindView(R.id.study_categoty_sh)
    TextView studyCategotySh;
    @BindView(R.id.study_categoty_sx)
    TextView studyCategotySx;
    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView studyCategotyRecyclerview;
    @BindView(R.id.study_share_send_time_tv)
    TextView studyShareSendTimeTv;
    @BindView(R.id.study_share_send_content_edit)
    EditText studyShareSendContentEdit;
    @BindView(R.id.study_share_send_award_edit)
    EditText studyShareSendAwardEdit;
    private DateTimePicker mDatePicker;

	StudySecondCategoryAdapter mSecondCategoryAdapter;

	@Override
    protected int getLayoutId() {
        return R.layout.activity_study_answer_send;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("发布讯息");
		initDateTime();

		mSecondCategoryAdapter = new StudySecondCategoryAdapter(this, DateCacheUtil.getRequestList(9));
		studyCategotyRecyclerview.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
		studyCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }

    private void initDateTime() {
        Calendar cal = Calendar.getInstance();
        mDatePicker = new DateTimePicker(this, DateTimePicker.HOUR_24);
        mDatePicker.setCanceledOnTouchOutside(true);
        mDatePicker.setDateRangeEnd(2030, 12, 1);
        mDatePicker.setDateRangeStart(2015, 1, 1);
        mDatePicker.setTimeRangeStart(0, 0);
        mDatePicker.setTimeRangeEnd(23, 59);
        mDatePicker.setSelectedItem(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), 12, 0);
        mDatePicker.setOnDateTimePickListener(new DateTimePicker.OnYearMonthDayTimePickListener() {
            @Override
            public void onDateTimePicked(String year, String month, String day, String hour, String minute) {
				studyShareSendTimeTv.setText(year + "-" + month + "-" + day + " " + hour + ":" + minute);
            }
        });

		studyShareSendTimeTv.setText(DateUtils.getCurrentDetialDate());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.study_share_send_time_tv, R.id.study_share_send_question })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.study_share_send_time_tv:
				mDatePicker.show();
                break;
            case R.id.study_share_send_question:
                break;

        }
    }
}
