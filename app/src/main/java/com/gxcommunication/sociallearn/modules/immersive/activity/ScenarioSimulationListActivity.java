package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.adapter.ScenarioSimulationAdapter;
import com.gxcommunication.sociallearn.modules.immersive.adapter.ScenarioSimulationTabClaasAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.gxcommunication.sociallearn.widget.BetterSpinner;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 场景模拟列表页面
 */
public class ScenarioSimulationListActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.btn_collection)
    ImageView btnCollection;
    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView tabClass;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    @BindView(R.id.category_pop_tv)
    TextView categoryPoptv;

    @BindView(R.id.sort_hot_iv)
    ImageView sortHotIv;
    @BindView(R.id.sort_new_iv)
    ImageView sortNewIv;

    StudyGridCategoryAdapter mSecondCategoryAdapter;

    private ScenarioSimulationAdapter scenarioSimulationAdapter;

    private StudyCategoryPopupWindow mCategoryPopupWindow;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_list;
    }

    @Override
    protected void init() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(10));
        tabClass.setLayoutManager(new GridLayoutManager(this,5));
        tabClass.setAdapter(mSecondCategoryAdapter);
        mSecondCategoryAdapter.setSelectItem(0);
        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });

        scenarioSimulationAdapter=new ScenarioSimulationAdapter(this, DateCacheUtil.getRequestList(20));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(scenarioSimulationAdapter);

        scenarioSimulationAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(ScenarioSimulationListActivity.this, ScenarioSimulationChoiceActivity.class);
            }
        });
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            refreshLayout.finishRefresh();
        });

        categoryPoptv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopupwinds();
            }
        });

        mCategoryPopupWindow = new StudyCategoryPopupWindow(this);

     }



    @OnClick({R.id.back, R.id.btn_collection})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                break;
            case R.id.btn_collection:
                break;
        }
    }


    private void showPopupwinds() {
        mCategoryPopupWindow.showAsDropDown(categoryPoptv);
    }


    @OnClick({R.id.sort_hot_iv, R.id.sort_new_iv})
    public void onViewSortClicked(View view) {
        switch (view.getId()) {
            case R.id.sort_hot_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_select);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_normal);
                break;
            case R.id.sort_new_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_normal);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_select);
                break;
        }
    }

}