package com.gxcommunication.sociallearn.modules;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.modules.immersive.activity.ImmersiveMainActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyForumActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyStoreActivity;
import com.gxcommunication.sociallearn.modules.main_my.activity.TopRankingActivity;
import com.gxcommunication.sociallearn.modules.main_pk.activity.PKRoomActivity;
import com.gxcommunication.sociallearn.modules.main_teacher.activity.ApplyTeacherActivity;
import com.gxcommunication.sociallearn.modules.member_activity.activity.MemberActSignActivity;
import com.gxcommunication.sociallearn.modules.member_activity.activity.MemberActiListActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.ExamRoomActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.ExamRoomHistoryActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.ExamTopicActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.ExerciseRoomActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.SelfEvaluateActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.StudyRoomActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.StudyRoomHistoryActivity;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity.ExhibitionHallActivity;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity.ExhibitionHallDetailsActivity;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity.ExhibitionHallResultListActivity;
import com.gxcommunication.sociallearn.modules.study.live.activity.LiveApplyActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoDetailsActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoMainActivity;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoTabListActivity;
import com.gxcommunication.sociallearn.modules.user.activity.LoginActivity;
import com.gxcommunication.sociallearn.modules.user.activity.RegisterActivity;
import com.gxcommunication.sociallearn.modules.user.activity.SetPsdActivity;
import com.gxcommunication.sociallearn.modules.user.activity.SettingUserInfoActivity;
import com.gxcommunication.sociallearn.modules.user.activity.UserNewHandTaskActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/8/14.
 */

public class MainTestFragment extends BaseFragment {
    private View mView;

    @Override
    protected void initView() {

    }

    @Override
    protected void lazyLoad() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_main_test, null);
        }
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mView.getParent() != null && mView != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
    }

    @OnClick({
            R.id.button, R.id.button1, R.id.button3, R.id.button4, R.id.button5, R.id.button6, R.id.button7, R.id.button8, R.id.ExhibitionHall,
            R.id.ExhibitionHallResult, R.id.ExhibitionHallDetails, R.id.VideoList, R.id.StudyAnswerShare, R.id.StudyStore, R.id.topRanking, R.id.selfEvaluate,
            R.id.immersiveMain,
            R.id.VideoDetail, R.id.VideoMain, R.id.liveApply,
            R.id.studyRoom, R.id.studyRoomHistory, R.id.studyExercise, R.id.examRoom, R.id.examTopic, R.id.examHistory,R.id.pkRoom
    })
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button:

                IntentUtils.startActivity(getActivity(), LoginActivity.class);
                break;
            case R.id.button1:
                IntentUtils.startActivity(getActivity(), RegisterActivity.class);
                break;
            case R.id.button3:

                IntentUtils.startActivity(getActivity(), SettingUserInfoActivity.class);
                break;
            case R.id.button4:

                IntentUtils.startActivity(getActivity(), SetPsdActivity.class);
                break;
            case R.id.button5:
                IntentUtils.startActivity(getActivity(), UserNewHandTaskActivity.class);
                break;
            case R.id.button6:
                IntentUtils.startActivity(getActivity(), MemberActiListActivity.class);
                break;
            case R.id.button7:
                IntentUtils.startActivity(getActivity(), MemberActSignActivity.class);
                break;
            case R.id.ExhibitionHall://创新展览馆
                IntentUtils.startActivity(getActivity(), ExhibitionHallActivity.class);
                break;
            case R.id.ExhibitionHallResult:
                IntentUtils.startActivity(getActivity(), ExhibitionHallResultListActivity.class);
                break;
            case R.id.ExhibitionHallDetails:
                IntentUtils.startActivity(getActivity(), ExhibitionHallDetailsActivity.class);
                break;
            case R.id.button8:
                IntentUtils.startActivity(getActivity(), ApplyTeacherActivity.class);
                break;
            case R.id.VideoList:
                IntentUtils.startActivity(getActivity(), VideoTabListActivity.class);
                break;
            case R.id.StudyAnswerShare://学创论坛
                IntentUtils.startActivity(getActivity(), StudyForumActivity.class);
                break;
            case R.id.StudyStore://知识库
                IntentUtils.startActivity(getActivity(), StudyStoreActivity.class);
                break;
            case R.id.topRanking://Top 排行
                IntentUtils.startActivity(getActivity(), TopRankingActivity.class);
                break;
            case R.id.selfEvaluate://自我评定
                IntentUtils.startActivity(getActivity(), SelfEvaluateActivity.class);
                break;
            case R.id.VideoDetail:
                IntentUtils.startActivity(getActivity(), VideoDetailsActivity.class);
                break;
            case R.id.VideoMain:
                IntentUtils.startActivity(getActivity(), VideoMainActivity.class);
                break;
            case R.id.liveApply:
                IntentUtils.startActivity(getActivity(), LiveApplyActivity.class);
                break;
            case R.id.studyRoom://自习室
                IntentUtils.startActivity(getActivity(), StudyRoomActivity.class);
                break;
            case R.id.studyRoomHistory://自习室 学习记录
                IntentUtils.startActivity(getActivity(), StudyRoomHistoryActivity.class);
                break;
            case R.id.studyExercise://练习馆
                IntentUtils.startActivity(getActivity(), ExerciseRoomActivity.class);
                break;
            case R.id.examRoom://考试
                IntentUtils.startActivity(getActivity(), ExamRoomActivity.class);
                break;
            case R.id.examTopic://考试题目
                IntentUtils.startActivity(getActivity(), ExamTopicActivity.class);
                break;
            case R.id.examHistory://考试记录
                IntentUtils.startActivity(getActivity(), ExamRoomHistoryActivity.class);
                break;
            case R.id.immersiveMain://身临其境
                IntentUtils.startActivity(getActivity(), ImmersiveMainActivity.class);
                break;
            case R.id.pkRoom://PK
                IntentUtils.startActivity(getActivity(), PKRoomActivity.class);
                break;
        }
    }
}
