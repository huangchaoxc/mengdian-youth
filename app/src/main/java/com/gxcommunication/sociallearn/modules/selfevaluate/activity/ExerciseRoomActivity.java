package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 练习馆
 */
public class ExerciseRoomActivity extends BaseActivity {

    @BindView(R.id.study_room_plate_tv)
    TextView studyRoomPlateTv;//板块
    @BindView(R.id.study_room_category_tv)
    TextView studyRoomCategoryTv;//专业领域
    @BindView(R.id.study_room_level_tv)
    TextView studyRoomLevelTv;//技能等级
    @BindView(R.id.study_room_work_type_tv)
    TextView studyWorkTypeTv;//工种

    @BindView(R.id.study_room_model_ordernum_tv)
    TextView studyRoomModelOrdernumTv;//顺序学习题目数量
    @BindView(R.id.study_room_model_randomnum_tv)
    TextView studyRoomModelRandomnumTv;//随机学习题目数量
    @BindView(R.id.study_room_radio_tv)
    TextView studyRoomRadioTv;//单选
    @BindView(R.id.study_room_multiple_tv)
    TextView studyRoomMultipleTv;//多选题
    @BindView(R.id.study_room_judge_tv)
    TextView studyRoomJudgeTv;//判断题
    @BindView(R.id.study_room_compute_tv)
    TextView studyRoomComputeTv;//计算题
    @BindView(R.id.layout_model_order)
    RelativeLayout layoutModelOrder;
    @BindView(R.id.layout_model_random)
    RelativeLayout layoutModelRandom;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exercise_room;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("练习篇");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.study_room_radio_tv, R.id.study_room_multiple_tv, R.id.study_room_judge_tv, R.id.study_room_compute_tv, R.id.study_room_collect_tv, R.id.study_room_search_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.study_room_radio_tv://单选   搜题的时候可以根据按钮是否选中的状态来进行提交选中的题目类型
//                studyRoomRadioTv.setSelected(!studyRoomRadioTv.isSelected());
                IntentUtils.startActivity(this, ExerciseTopicListActivity.class);

                break;
            case R.id.study_room_multiple_tv://多选题
//                studyRoomMultipleTv.setSelected(!studyRoomMultipleTv.isSelected());
                IntentUtils.startActivity(this, ExerciseTopicListActivity.class);

                break;
            case R.id.study_room_judge_tv://判断题
//                studyRoomJudgeTv.setSelected(!studyRoomJudgeTv.isSelected());
                IntentUtils.startActivity(this, ExerciseTopicListActivity.class);

                break;
            case R.id.study_room_compute_tv://计算题
//                studyRoomComputeTv.setSelected(!studyRoomComputeTv.isSelected());
                IntentUtils.startActivity(this, ExerciseTopicListActivity.class);

                break;
            case R.id.study_room_collect_tv://收藏
                IntentUtils.startActivity(this, StudyRoomHistoryTopicActivity.class);
                break;
            case R.id.study_room_record_tv://学习记录
                IntentUtils.startActivity(this, StudyRoomHistoryActivity.class);

                break;
            case R.id.study_room_error_tv:// 错题
                IntentUtils.startActivity(this, StudyRoomHistoryTopicActivity.class);
                break;
            case R.id.study_room_search_tv://搜题
//                if (!layoutModelOrder.isSelected() && !layoutModelRandom.isSelected()){
//                    showToast("请选择学习模式");
//                    return;
//                }
                IntentUtils.startActivity(this, TopicSearchTypeActivity.class);
                break;
        }
    }


    @OnClick({R.id.layout_model_order, R.id.layout_model_random})
    public void onViewModelClicked(View view) {//题目模式
        switch (view.getId()) {
            case R.id.layout_model_order://单选，如果顺序学习为true ,那随机学习就要设置为false

                if (layoutModelOrder.isSelected()){
                    layoutModelOrder.setSelected(false);

                }else {
                    layoutModelOrder.setSelected(true);
                    layoutModelRandom.setSelected(false);
                }
                break;
            case R.id.layout_model_random:
                if (layoutModelRandom.isSelected()){
                    layoutModelRandom.setSelected(false);

                }else {
                    layoutModelRandom.setSelected(true);
                    layoutModelOrder.setSelected(false);
                }
                break;
        }
    }
}
