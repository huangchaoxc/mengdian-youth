package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.adapter.CaseAnalyseAdapter;
import com.gxcommunication.sociallearn.modules.immersive.adapter.ScenarioSimulationAdapter;
import com.gxcommunication.sociallearn.modules.immersive.adapter.ScenarioSimulationTabClaasAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 场景模拟列表页面
 */
public class CaseAnalyseListActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.btn_collection)
    ImageView btnCollection;
    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView tabClass;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    StudyGridCategoryAdapter mSecondCategoryAdapter;

    private CaseAnalyseAdapter scenarioSimulationAdapter;

    private final static String[] tabs = new String[]{
            "全部", "化学", "变电运算与维修", "营业用电"
    };

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_list;
    }

    @Override
    protected void init() {
        tabClass.setLayoutManager(new GridLayoutManager(this,5));

        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(9));
        tabClass.setAdapter(mSecondCategoryAdapter);

        scenarioSimulationAdapter = new CaseAnalyseAdapter(this, DateCacheUtil.getRequestList(20));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(scenarioSimulationAdapter);

        scenarioSimulationAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(CaseAnalyseListActivity.this, CaseAnalyseDetailActivity.class);
            }
        });
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            refreshLayout.finishRefresh();
        });
    }


    @OnClick({R.id.back, R.id.btn_collection})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.btn_collection:
                break;
        }
    }
}