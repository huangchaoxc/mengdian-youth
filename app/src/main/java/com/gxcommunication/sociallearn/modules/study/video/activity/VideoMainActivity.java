package com.gxcommunication.sociallearn.modules.study.video.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VideoLiveListFragment;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VideoMainShowFragment;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 视频直播入口主页面
 */
public class VideoMainActivity extends BaseActivity {

    @BindView(R.id.btn_show)
    LinearLayout btnShow;// 页面浏览
    @BindView(R.id.btn_live)
    LinearLayout btnLive;//直播页面
    @BindView(R.id.btn_upload)
    LinearLayout btnUpload;//上传按钮
    @BindView(R.id.btn_collection)
    LinearLayout btnCollection;//我的收藏
    @BindView(R.id.frame)
    FrameLayout frame;
    /**
     * 集合
     */
    private Map<Integer,Fragment> fragmentMap=new HashMap<>();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_main;
    }

    @Override
    protected void init() {
        Fragment fragment=getFragmentInstance(R.id.btn_show);
        getSupportFragmentManager().beginTransaction().add(R.id.frame,fragment).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_show, R.id.btn_live, R.id.btn_upload, R.id.btn_collection})
    public void onViewClicked(View view) {
        Fragment fragment=getFragmentInstance(view.getId());
        getSupportFragmentManager().beginTransaction().replace(R.id.frame,fragment).commit();
    }

    /**
     * 获取试图实例
     * @param id
     * @return
     */
    private Fragment getFragmentInstance(int id){

        if(fragmentMap.containsKey(id)){
            return  fragmentMap.get(id);
        }
        Fragment fragment=null;
        switch (id) {
            case R.id.btn_live:
                fragment= VideoLiveListFragment.getInstance();
                break;
            case R.id.btn_upload:
                 fragment= VideoMainShowFragment.getInstance();
                break;
            case R.id.btn_collection:
                fragment=  VideoMainShowFragment.getInstance();
                break;
            case R.id.btn_show:
                fragment=  VideoMainShowFragment.getInstance();
                break;
            default:
                fragment=  VideoMainShowFragment.getInstance();
        }
        fragmentMap.put(id,fragment);
        return fragment;
    }

}