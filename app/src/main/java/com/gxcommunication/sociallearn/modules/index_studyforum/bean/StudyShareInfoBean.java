package com.gxcommunication.sociallearn.modules.index_studyforum.bean;

public class StudyShareInfoBean {

    public StudyShareInfoBean(String category, String title, String userName, String createTime) {
        this.category = category;
        this.title = title;
        this.userName = userName;
        this.createTime = createTime;
    }

    public StudyShareInfoBean(String category, String title,String content, String userName, String createTime) {
        this.category = category;
        this.title = title;
        this.userName = userName;
        this.createTime = createTime;
        this.contentStr = content;
    }

    public String id;
    public String category;
    public String title;//
    public String contentStr;//
    public String userName;
    public String createTime;//时间

    public String userId;//
    public String scaneCount;
}
