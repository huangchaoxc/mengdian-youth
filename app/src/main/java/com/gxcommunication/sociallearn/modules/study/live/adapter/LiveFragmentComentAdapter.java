package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class LiveFragmentComentAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public LiveFragmentComentAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_comment_chat_receive_text, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        if (helper.getLayoutPosition() == 0){
            helper.setText(R.id.jmui_msg_content,"学习到了！！").setText(R.id.jmui_display_name_tv,"张工");
        }else  if (helper.getLayoutPosition() == 1){
            helper.setText(R.id.jmui_msg_content,"支持，点赞！").setText(R.id.jmui_display_name_tv,"程浩");
        }else  if (helper.getLayoutPosition() == 2){
            helper.setText(R.id.jmui_msg_content,"课程很精彩").setText(R.id.jmui_display_name_tv,"黄骅");
        }else  if (helper.getLayoutPosition() == 3){
            helper.setText(R.id.jmui_msg_content,"期待下次直播课程").setText(R.id.jmui_display_name_tv,"白华子");
        }else  if (helper.getLayoutPosition() == 4){
            helper.setText(R.id.jmui_msg_content,"加油！").setText(R.id.jmui_display_name_tv,"卢洲");
        }else{
            helper.setText(R.id.jmui_msg_content,"感谢您的分享，辛苦了");
        }


    }


}
