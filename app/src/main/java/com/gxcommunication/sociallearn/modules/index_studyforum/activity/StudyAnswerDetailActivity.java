package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyMyAnswerDetailAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAnswerDetailAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyAnswerDetailBean;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 学创论坛 子栏目 问答  详情
 */
public class StudyAnswerDetailActivity extends BaseActivity {

    public static final String INTENT_KEY_DETAIL_INFO = "detail_Info";

    private int answerDetailType = 0;//两种状态，0 是非作者，不显示采纳的按钮

    @BindView(R.id.study_share_category_tv)
    TextView studyShareCategoryTv; //栏目名称
    @BindView(R.id.study_share_status_tv)
    TextView studyShareStatusTv; // 状态，已完结 未完结
    @BindView(R.id.study_share_award_tv)
    TextView studyShareAwardTv; // 悬赏分数
    @BindView(R.id.study_share_title_tv)
    TextView studyShareTitleTv; // 问答标题
    @BindView(R.id.study_share_username_tv)
    TextView studyShareUsernameTv; // 用户名
    @BindView(R.id.study_share_time_tv)
    TextView studyShareTimeTv; // 回答时间
    @BindView(R.id.study_share_read_tv)
    TextView studyShareReadTv; // 阅读数
    @BindView(R.id.study_share_star_tv)
    TextView studyShareStarTv; // 点赞数
    @BindView(R.id.study_share_commit_tv)
    TextView studyShareCommitTv;// 回复数量
    @BindView(R.id.study_share_collect_tv)
    TextView studyShareCollectTv;// 收藏数量
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;
    @BindView(R.id.action_btn)
    Button actionBtn;//如果问答状态是已经采纳，那回答问题的按钮将进行隐藏

    StudyShareAnswerDetailAdapter mAdapter;
    StudyMyAnswerDetailAdapter mMyAdapter;//如果是进入我发布的信息页面，则评论详情中有显示采纳答案的按钮

    StudyAnswerDetailBean mStudyAnswerDetailBean;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_answer_detail;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("问答详情");


        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (!isEmptyStringIntent(INTENT_KEY_DETAIL_INFO)){
            mStudyAnswerDetailBean = GsonUtlils.jsonToBean(getIntentStringValue(INTENT_KEY_DETAIL_INFO),StudyAnswerDetailBean.class);

            LogHelper.e(getIntentStringValue(INTENT_KEY_DETAIL_INFO));
            loadDetailInfo();
        }


    }


    private void loadDetailInfo(){
        studyShareCategoryTv.setText(mStudyAnswerDetailBean.category);
        studyShareAwardTv.setText("悬赏分数：" + mStudyAnswerDetailBean.awardScore + "分");
        studyShareTitleTv.setText(mStudyAnswerDetailBean.title);
        studyShareTimeTv.setText(mStudyAnswerDetailBean.createTime);

        studyShareUsernameTv.setText(mStudyAnswerDetailBean.userName);

        if (mStudyAnswerDetailBean.isAccept == 0){ //模拟显示采纳，显示采纳按钮
            studyShareStatusTv.setText("已完结");
            actionBtn.setVisibility(View.GONE);
        }else {
            studyShareStatusTv.setText("未完结");
            actionBtn.setVisibility(View.VISIBLE);
        }

        if (mStudyAnswerDetailBean.userId.equals("1") ){//模拟 我自己的id是 1 是作者， 需要自己判断此条问答是否为自己发布的问答
            mMyAdapter = new StudyMyAnswerDetailAdapter(this, DateCacheUtil.getStudyAnswerResDetail(mStudyAnswerDetailBean.isAccept == 0));
            recyclerView.setAdapter(mMyAdapter);
        }else {
            mAdapter = new StudyShareAnswerDetailAdapter(this, DateCacheUtil.getStudyAnswerResDetail(mStudyAnswerDetailBean.isAccept == 0));
            recyclerView.setAdapter(mAdapter);
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {
        IntentUtils.startActivity(this, StudyAnswersCommentActivity.class);
    }
}
