package com.gxcommunication.sociallearn.modules.member_activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.member_activity.adapter.FragmentHomeActiAdapter;
import com.gxcommunication.sociallearn.modules.member_activity.adapter.MemberActiListAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by hxc on 2017/8/14.
 *
 * 首页 活动 fragment
 */

public class MainActiFragment extends BaseFragment {


	@BindView(R.id.recyclerView)
	RecyclerView mRecyclerView;
	@BindView(R.id.refreshLayout)
	SmartRefreshLayout mRefreshLayout;

	private View mView;
	FragmentHomeActiAdapter mAdapter;
	@Override
	protected void initView() {

	}

	@Override
	protected void lazyLoad() {
		mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		mRefreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
			@Override
			public void onLoadmore(RefreshLayout refreshlayout) {

			}

			@Override
			public void onRefresh(RefreshLayout refreshlayout) {

			}
		});

		mAdapter = new FragmentHomeActiAdapter(getActivity(), DateCacheUtil.getRequestList(8));

		mRecyclerView.setAdapter(mAdapter);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (mView == null) {
			mView = inflater.inflate(R.layout.fragment_main_acti, null);
		}
		ButterKnife.bind(this, mView);
		return mView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (mView.getParent() != null && mView != null){
			((ViewGroup)mView.getParent()).removeView(mView);
		}
	}
}
