package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.main_pk.adapter.PKTeamMatchMemberAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * PK  团体赛  匹配队伍
 */
public class PKTeamMatchActivity extends BaseActivity {


    @BindView(R.id.pk_room_plate_tv)
    TextView pkRoomPlateTv;
    @BindView(R.id.pk_room_category_tv)
    TextView pkRoomCategoryTv;
    @BindView(R.id.pk_room_level_tv)
    TextView pkRoomLevelTv;
    @BindView(R.id.pk_room_topicnum_tv)
    TextView pkRoomTopicnumTv;
    @BindView(R.id.pk_room_radio_tv)
    TextView pkRoomRadioTv;
    @BindView(R.id.pk_room_multple_tv)
    TextView pkRoomMultpleTv;
    @BindView(R.id.pk_room_judge_tv)
    TextView pkRoomJudgeTv;
    @BindView(R.id.pk_room_compute_tv)
    TextView pkRoomComputeTv;
    @BindView(R.id.pk_team_my_status_tv)
    TextView pkTeamMyStatusTv;
    @BindView(R.id.MyRecyclerView)
    RecyclerView MyRecyclerView;
    @BindView(R.id.pk_team_my_name_tv)
    TextView pkTeamMyNameTv;
    @BindView(R.id.pk_team_enemy_status_tv)
    TextView pkTeamEnemyStatusTv;
    @BindView(R.id.EnemyRecyclerView)
    RecyclerView EnemyRecyclerView;
    @BindView(R.id.pk_team_enemy_tips_tv)
    TextView pkTeamEnemyTipsTv;
    @BindView(R.id.pk_team_enemy_name_tv)
    TextView pkTeamEnemyNameTv;
    @BindView(R.id.pk_team_status_tv)
    TextView pkTeamStatusTv;
    @BindView(R.id.pk_remind_btn)
    Button pkRemindBtn;

    PKTeamMatchMemberAdapter memberAdapter;//我的队员
    PKTeamMatchMemberAdapter memberOtherAdapter;//敌方队员

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pk_team_match;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("匹配队伍");

        memberAdapter = new PKTeamMatchMemberAdapter(this, DateCacheUtil.getRequestList(9));
        memberOtherAdapter = new PKTeamMatchMemberAdapter(this, DateCacheUtil.getRequestList(9));

        MyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyRecyclerView.setAdapter(memberAdapter);

        EnemyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        EnemyRecyclerView.setAdapter(memberOtherAdapter);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @OnClick({R.id.pk_team_enemy_tips_tv, R.id.pk_remind_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.pk_team_enemy_tips_tv:
                break;
            case R.id.pk_remind_btn:
                IntentUtils.startActivity(this, PKTeamGameActivity.class);
                break;
        }
    }
}
