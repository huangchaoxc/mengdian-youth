package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;

import java.util.List;
import java.util.Random;


/**
 * Created by hxc on 2017/8/7
 */

public class LiveVoteResultDetailAdapter extends BaseQuickAdapter<LiveVoteCreateAndDetailBean.DataBean, BaseViewHolder> {


    public LiveVoteResultDetailAdapter(Context context, List<LiveVoteCreateAndDetailBean.DataBean> data) {
        super(R.layout.item_live_vote_result_detail_activity, data);
        mContext = context;

    }

    @Override
    protected void convert(BaseViewHolder helper, LiveVoteCreateAndDetailBean.DataBean item) {


        helper.setText(R.id.item_live_vote_title_tv, item.voteContent);
        //模拟数据后期删除
        int count = (int) Math.random() * 10;
        int countScore = (int) Math.random() * 50;
		item.selectMyCount = count;
		item.totlaCount = countScore;


        helper.setText(R.id.item_live_vote_score_tv, item.selectMyCount + "票" + item.totlaCount + "%");
    }
}
