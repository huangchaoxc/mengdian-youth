package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.ExamTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 考试题目
 */
public class ExamTopicActivity extends BaseActivity {

    @BindView(R.id.exam_room_plate_tv)
    TextView examRoomPlateTv;//板块
    @BindView(R.id.exam_room_radio_tv)
    TextView examRoomRadioTv;//单选题
    @BindView(R.id.exam_room_multple_tv)
    TextView examRoomMultpleTv;//多选题
    @BindView(R.id.exam_room_category_tv)
    TextView examRoomCategoryTv;//专业
     @BindView(R.id.exam_room_worktype_tv)
    TextView examWorkTypeTv;//工种

    @BindView(R.id.exam_room_judge_tv)
    TextView examRoomJudgeTv;//判断题
    @BindView(R.id.exam_room_compute_tv)
    TextView examRoomComputeTv;//计算题
    @BindView(R.id.exam_room_level_tv)
    TextView examRoomLevelTv;//等级
    @BindView(R.id.exam_room_introl_tv)
    TextView examRoomIntrolTv;//考试说明
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    ExamTopicListAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exam_topic;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("考试篇");
		mAdapter = new ExamTopicListAdapter(this, loadTempDate());
		recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
		recyclerView.setAdapter(mAdapter);
		new PagerSnapHelper().attachToRecyclerView(recyclerView);
		mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
			@Override
			public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
				RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
				int firs = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
				if (view.getId() == R.id.item_topic_next_iv) {
					recyclerView.scrollToPosition(firs + 1);

				}
			}
		});
    }

	private List<StudyTopicMultiItemBean> loadTempDate() {

		List<StudyTopicMultiItemBean> list = new ArrayList<>();
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Radio_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Multiple_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Judge_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));

		return list;
	}

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {

        showDiglog("是否需要交卷？", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showDialog();
            }
        });


    }

    private void showDialog() {

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_exam_result_layout, null);
        AlertDialog builder = new AlertDialog.Builder(this, R.style.NobackDialog).create();
        builder.setView(view);
        builder.setCancelable(true);
        view.findViewById(R.id.iv_colose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
                finish();
            }
        });
        builder.show();
    }
}
