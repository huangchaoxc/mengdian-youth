package com.gxcommunication.sociallearn.modules.study.live.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveFragmentAnchorAdapter;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveFragmentComentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 视频详情-视频评论
 */
public class LiveAnchorFragment extends BaseFragment {

    @BindView(R.id.jmui_avatar_iv)
    ImageView userAvatarIv;
    @BindView(R.id.live_anchor_name_tv)
    TextView liveAnchorNameTv;
    @BindView(R.id.live_anchor_introl_tv)
    TextView liveAnchorIntrolTv;
    @BindView(R.id.live_anchor_attention_tv)
    TextView liveAnchorAttentionTv;
    @BindView(R.id.live_anchor_recyclerView)
    RecyclerView liveAnchorRecyclerView;

    LiveFragmentAnchorAdapter mAdapter;
    private View mView;

    private static final String ARG_VIDEOID = "params_video_id";

    public static LiveAnchorFragment getInstance(String videoId) {
        LiveAnchorFragment fragment = new LiveAnchorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_VIDEOID, videoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_live_anchor, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }


    @Override
    protected void initView() {
        mAdapter = new LiveFragmentAnchorAdapter(getActivity(), DateCacheUtil.getRequestList(3));
        liveAnchorRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        liveAnchorRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void lazyLoad() {

    }
}
