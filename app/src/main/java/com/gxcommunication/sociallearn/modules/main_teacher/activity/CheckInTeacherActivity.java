package com.gxcommunication.sociallearn.modules.main_teacher.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyTaskActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 向老师报到
 */
public class CheckInTeacherActivity extends BaseActivity {

    @BindView(R.id.user_info_head_iv)
    ImageView userInfoHeadIv;
    @BindView(R.id.checkin_teacher_nickname_tv)
    TextView checkinTeacherNicknameTv;
    @BindView(R.id.checkin_teacher_introl_tv)
    TextView checkinTeacherIntrolTv;
    @BindView(R.id.checkin_teacher_baseinfo_tv)
    TextView checkinTeacherBaseinfoTv;
    @BindView(R.id.checkin_teacher_strong_tv)
    TextView checkinTeacherStrongTv;
    @BindView(R.id.checkin_teacher_honor_tv)
    TextView checkinTeacherHonorTv;
    @BindView(R.id.action_btn)
    TextView actionBtn;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_checkin_teacher;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        setHeadActionTitle("报到");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick(R.id.action_btn)
    public void onViewClicked() {
        IntentUtils.startActivity(this, StudyTaskActivity.class);
    }
}
