package com.gxcommunication.sociallearn.modules.user.activity;

import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc  用户登录
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.phone_et)
    EditText phoneEt;// 用户名
    @BindView(R.id.pass_et)
    EditText passEt;// 密码
	@BindView(R.id.iv_eyes)
	ImageView mIvEyes;// 控制密码显示隐藏

	private boolean mIsMingWen = false;//标识密码是否为明文转态


	@Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick({R.id.iv_close, R.id.pass_et, R.id.iv_eyes, R.id.tv_wx,R.id.loginBtn,R.id.getPass,R.id.registerBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                break;
            case R.id.pass_et:
                break;
            case R.id.iv_eyes:
				changeYanJingState();
                break;
            case R.id.tv_wx:
                break;

             case R.id.loginBtn:
                break;

             case R.id.getPass:
                 IntentUtils.startActivity(this,SetPsdActivity.class);
                break;
            case R.id.registerBtn:
                IntentUtils.startActivity(this,RegisterActivity.class);
                break;
        }
    }

	private void changeYanJingState() {
		mIvEyes.setImageResource(this.mIsMingWen ? R.drawable.ic_eyes : R.drawable.ic_eyes_s);
		passEt.setTransformationMethod(this.mIsMingWen ? PasswordTransformationMethod.getInstance() : HideReturnsTransformationMethod.getInstance());
		this.mIsMingWen = !this.mIsMingWen;
	}
}
