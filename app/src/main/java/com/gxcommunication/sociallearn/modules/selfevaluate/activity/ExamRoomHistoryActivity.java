package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.ExamRoomHistoryAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyRoomHistoryAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.hxc.toolslibrary.example.base.CommonSmartRefreshActivity;

/**
 *  
 * @author hxc
 * 自习室 考试记录 列表
 */
public class ExamRoomHistoryActivity extends CommonSmartRefreshActivity {
	private ExamRoomHistoryAdapter mAdapter;

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_stu;
//    }

    @Override
    protected void init() {
    	super.init();
		initView();
    }


	public void initView() {
        head_action_title.setText("考试记录");
		mAdapter = new ExamRoomHistoryAdapter(this, DateCacheUtil.getRequestList(6));
		mRecyclerView.setAdapter(mAdapter);

		mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
				IntentUtils.startActivity(ExamRoomHistoryActivity.this,StudyRoomHistoryTopicActivity.class);

			}
		});
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}
	
	


}
