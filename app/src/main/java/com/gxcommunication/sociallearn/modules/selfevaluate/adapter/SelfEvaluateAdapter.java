package com.gxcommunication.sociallearn.modules.selfevaluate.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 自习室
 */

public class SelfEvaluateAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	private int mSelectPos = -1;

	public int getmSelectPos() {
		return mSelectPos;
	}

	public void setmSelectPos(int mSelectPos) {
		this.mSelectPos = mSelectPos;
		notifyDataSetChanged();
	}

	public SelfEvaluateAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_self_evaluate_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
		helper.setText(R.id.item_evaluate_level_tv,"初级");//等级
		helper.setText(R.id.item_evaluate_title_tv,"变压器检修顶部作业安全带固定装置");//标题
		helper.setText(R.id.item_evaluate_condition_tv,"资格要求：积分到达xxxx");// 积分资质条件


		if (helper.getLayoutPosition() != 0){
			helper.setImageResource(R.id.item_evaluate_is_ok_iv,R.mipmap.ic_selfvaluate_false);
		}

		if (mSelectPos == helper.getAdapterPosition()){
			helper.setBackgroundRes(R.id.layoutContent,R.drawable.item_self_eveluate_select_bg);
		}else {
			helper.setBackgroundRes(R.id.layoutContent,R.drawable.item_self_eveluate_unselect_bg);
		}
	}

	@Override
	protected int getDefItemViewType(int position) {
		return super.getDefItemViewType(position);
	}
}
