package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.SelfEvaluateAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 考试
 */
public class ExamRoomActivity extends BaseActivity {


    SelfEvaluateAdapter mAdapter;
    @BindView(R.id.evaluate_recyclerview)
    RecyclerView evaluateRecyclerview;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exam_room;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("考试");

        mAdapter = new SelfEvaluateAdapter(this, DateCacheUtil.getRequestList(9));
        evaluateRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        evaluateRecyclerview.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(ExamRoomActivity.this,ExamRoomRemindActivity.class);
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @OnClick({R.id.exam_room_error_tv, R.id.exam_room_collect_tv, R.id.exam_room_record_tv, R.id.exam_room_rank_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.exam_room_error_tv:
                IntentUtils.startActivity(ExamRoomActivity.this,StudyRoomHistoryTopicActivity.class);
                break;
            case R.id.exam_room_collect_tv:
                IntentUtils.startActivity(ExamRoomActivity.this,StudyRoomHistoryTopicActivity.class);
                break;
            case R.id.exam_room_record_tv:
                IntentUtils.startActivity(ExamRoomActivity.this,ExamRoomHistoryActivity.class);
                break;
            case R.id.exam_room_rank_tv:
                IntentUtils.startActivity(ExamRoomActivity.this,ExamRankingActivity.class);
                break;
        }
    }
}
