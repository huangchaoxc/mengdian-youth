package com.gxcommunication.sociallearn.modules.main_pk.adapter;

import android.content.Context;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.main_pk.activity.PKRoomSchemaActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * PK 竞技场
 */

public class PKRoomAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public PKRoomAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_pk_room_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
		helper.setText(R.id.item_evaluate_title_tv,"初级");//专业
		helper.setText(R.id.item_evaluate_condition_tv,"资格要求：通过自我鉴定初级");// 要求

		helper.getView(R.id.item_evaluate_is_ok_iv).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				IntentUtils.startActivity(mContext, PKRoomSchemaActivity.class);
			}
		});
	}

	@Override
	protected int getDefItemViewType(int position) {
		return super.getDefItemViewType(position);
	}
}
