package com.gxcommunication.sociallearn.modules.main_message.adapter;

import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.utils.ImageManager;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class HomeMessageListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public HomeMessageListAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_home_message_fragment, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.item_home_message_title_tv,"系统消息").
		setText(R.id.item_home_message_content_tv,"系统刚刚发布了一条消息。。。。");

//		ImageManager.getManager(mContext).loadUrlFitImage("",helper.getView(R.id.item_home_message_pic_iv));
    }


}
