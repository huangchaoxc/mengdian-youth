package com.gxcommunication.sociallearn.modules.selfevaluate.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

/**
 *  自习室 多布局实体类
 */
public class StudyTopicMultiItemBean implements MultiItemEntity {
    public static final int Topic_Radio_Type = 0;
    public static final int Topic_Multiple_Type = 1;
    public static final int Topic_Judge_Type = 2;
    public static final int Topic_Compute_Type = 3;

    private int TopicType = Topic_Radio_Type;//题目类型 ，单选，多选，判断，计算题

    public StudyTopicMultiItemBean(int topicType) {
        TopicType = topicType;
    }

    public RequestResBean TopicRadioBean ;
    public RequestResBean TopicMultipleBean  ;
    public RequestResBean TopicJudgeBean  ;
    public RequestResBean TopicComputeBean  ;

    @Override
    public int getItemType() {
        return TopicType;
    }
}
