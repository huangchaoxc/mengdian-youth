package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAdapter;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 学创论坛 子栏目  经验分享   下 我的收藏和我的问题
 */
public class MyStudyShareCollectActivity extends BaseActivity {

    public static String INTENT_KEY_TYPE = "key_type"; //根据type 判断 进入的时候是选择加载 我的收藏还是我发布的问题， 0是收藏 1是 问题


    @BindView(R.id.my_study_collect)
    TextView myStudyCollect;
    @BindView(R.id.my_study_issue)
    TextView myStudyIssue;
    @BindView(R.id.study_recyclerview)
    RecyclerView studyRecyclerview;

    StudyShareAdapter mShareAdapter;

	@Override
    protected int getLayoutId() {
        return R.layout.activity_my_study_collect_issue;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("经验分享");

        int selectType = getIntent().getIntExtra(INTENT_KEY_TYPE,0);

        if (selectType == 0){
            myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_head));
            myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
        }else {
            myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_head));
        }

        mShareAdapter = new StudyShareAdapter(this,DateCacheUtil.getMyStudyShareSendList(this));
        studyRecyclerview .setLayoutManager(new LinearLayoutManager(this));
        studyRecyclerview.setAdapter(mShareAdapter);
        mShareAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                IntentUtils.startActivityForString(MyStudyShareCollectActivity.this, StudyShareDetailActivity.class,StudyShareDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mShareAdapter.getItem(position)));
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.my_study_collect, R.id.my_study_issue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.my_study_collect:
				myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_head));
				myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                mShareAdapter.setNewData(DateCacheUtil.getMyStudyShareList(this));
                 break;
            case R.id.my_study_issue:
				myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
				myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_head));
                mShareAdapter.setNewData(DateCacheUtil.getMyStudyShareSendList(this));
                 break;
        }
    }
}
