package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveMeetingPlayerAdapter;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveMenuAdapter;
import com.gxcommunication.sociallearn.widget.popup.CommonPopupWindow;
import com.scwang.smartrefresh.layout.util.DensityUtil;
import com.tencent.rtmp.ui.TXCloudVideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveMeetingPlayerActivity extends BaseActivity {

    @BindView(R.id.live_menu_play_start_btn)
    Button liveMenuPlayStartBtn;
    @BindView(R.id.live_menu_camera_switcher_btn)
    Button liveMenuCameraSwitcherBtn;
    @BindView(R.id.live_menu_voice_btn)
    Button liveMenuVoiceBtn;
    @BindView(R.id.live_menu_more_btn)
    Button liveMenuMoreBtn;

    @BindView(R.id.head_action_backimage)
    ImageView backimage;
    @BindView(R.id.head_action_title)
    TextView headActionTitle;
    @BindView(R.id.live_meeting_host_iv)
    ImageView liveMeetingHostIv;
    @BindView(R.id.live_meeting_host_video)
    TXCloudVideoView liveMeetingHostVideo;
    @BindView(R.id.live_meeting_host_textureview)
    TextureView liveMeetingHostTextureview;
    @BindView(R.id.live_meeting_host_layout)
    FrameLayout liveMeetingHostLayout;
    @BindView(R.id.live_meeting_host_name_tv)
    TextView liveMeetingHostNameTv;
    @BindView(R.id.player_toolsbar_layout)
    LinearLayout playerToolsbarLayout;


    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;


    CommonPopupWindow popupWindow = null;
    LiveMeetingPlayerAdapter meetingPlayerAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_meeting_player;
    }

    @Override
    protected void init() {
        meetingPlayerAdapter = new LiveMeetingPlayerAdapter(this,DateCacheUtil.getRequestList(9));
        recyclerview.setLayoutManager(new GridLayoutManager(this,2));
        recyclerview.setAdapter(meetingPlayerAdapter);
    }


    @OnClick({R.id.head_action_backimage,R.id.live_menu_play_start_btn, R.id.live_menu_camera_switcher_btn, R.id.live_menu_voice_btn, R.id.live_menu_more_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.head_action_backimage:
                showDiglog("是否要退出会议", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                break;
            case R.id.live_menu_play_start_btn:
                liveMenuPlayStartBtn.setSelected(!liveMenuPlayStartBtn.isSelected());

                if (liveMenuPlayStartBtn.isSelected()) {
                    liveMenuPlayStartBtn.setBackgroundResource(R.drawable.play_start);
                } else {
                    liveMenuPlayStartBtn.setBackgroundResource(R.drawable.play_pause);
                }

                break;
            case R.id.live_menu_camera_switcher_btn:

                liveMenuCameraSwitcherBtn.setSelected(!liveMenuCameraSwitcherBtn.isSelected());

                if (liveMenuCameraSwitcherBtn.isSelected()) {//前置摄像头
                    liveMenuCameraSwitcherBtn.setBackgroundResource(R.drawable.app_meeting_camera_front);
                } else {
                    liveMenuCameraSwitcherBtn.setBackgroundResource(R.drawable.app_meeting_camera_back);
                }
                break;
            case R.id.live_menu_voice_btn:
                liveMenuVoiceBtn.setSelected(!liveMenuVoiceBtn.isSelected());

                if (liveMenuVoiceBtn.isSelected()) {//开启语音
                    liveMenuVoiceBtn.setBackgroundResource(R.drawable.mic_normal);
                } else {
                    liveMenuVoiceBtn.setBackgroundResource(R.drawable.mic_disable);
                }
                break;
            case R.id.live_menu_more_btn:
                showPopuView();

                break;
        }
    }

    private void showPopuView() {

        View upView = LayoutInflater.from(this).inflate(R.layout.layout_live_menu_popu, null);
        //测量View的宽高
        popupWindow = new CommonPopupWindow.Builder(this)
                .setView(upView)
                .setWidthAndHeight(ViewGroup.LayoutParams.MATCH_PARENT / 2, ViewGroup.LayoutParams.MATCH_PARENT / 2)
                .setOutsideTouchable(true)
                .setBackGroundLevel(0.5f) //取值范围0.0f-1.0f 值越小越暗
                .setAnimationStyle(R.style.AnimUp)
//            .setViewOnclickListener(this)
                .create();

        RecyclerView recyclerView = upView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));

        LiveMenuAdapter adapter = new LiveMenuAdapter(this, DateCacheUtil.getRequestList(6));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = null;
                switch (position) {
                    case 0:// 测试主持人进入投票
                        intent = new Intent(LiveMeetingPlayerActivity.this, LiveVoteListActivity.class);
                        intent.putExtra(LiveVoteListActivity.INTENT_KEY_IS_HOST, true);
                        startActivity(intent);
                        break;

                    case 1:// 非测试主持人进入投票
                        intent = new Intent(LiveMeetingPlayerActivity.this, LiveVoteListActivity.class);
                        intent.putExtra(LiveVoteListActivity.INTENT_KEY_IS_HOST, false);
                        startActivity(intent);
                        break;
                }
                popupWindow.dismiss();
            }
        });

        int[] positions = new int[2];
        liveMenuMoreBtn.getLocationOnScreen(positions);
        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.TOP, positions[0], positions[1] - DensityUtil.dp2px(60));
//        popupWindow.showAtLocation(findViewById(android.R.id.content), Gravity.NO_GRAVITY, positions[0], positions[1] - popupWindow.getHeight() - DensityUtil.dp2px(50));
    }
}