package com.gxcommunication.sociallearn.modules.main_my.activity;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAdapter;

import butterknife.BindView;

/**
 *  
 * @author hxc
 * 我的发布
 */
public class MyIssueInfoActivity extends BaseActivity {

	@BindView(R.id.tabLayout)
	TabLayout tabLayout;
	@BindView(R.id.recyclerview)
	RecyclerView recyclerview;

	private StudyAnswerAdapter mAnswerAdapter = new StudyAnswerAdapter(this, DateCacheUtil.getStudyAnswerDetail());//你问我答
	private StudyShareAdapter mNShareAdapter  = new StudyShareAdapter(this,DateCacheUtil.getMyStudyShareList(this));//经验分享

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_issue;
    }

    @Override
    protected void init() {
		initView();
    }

	
	public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("我的发布");
		initTabLayout();

		recyclerview.setLayoutManager(new LinearLayoutManager(this));
		recyclerview.setAdapter(mAnswerAdapter);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}


	private void initTabLayout() {
		tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
		tabLayout.setTabMode(TabLayout.MODE_FIXED);
		tabLayout.addTab(tabLayout.newTab().setText("你问我答"));
		tabLayout.addTab(tabLayout.newTab().setText("经验分享"));

		tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
			@Override
			public void onTabSelected(TabLayout.Tab tab) {
				if (tab.getPosition() == 0){
					recyclerview.setAdapter(mAnswerAdapter);
				}else {
					recyclerview.setAdapter(mNShareAdapter);
				}
			}

			@Override
			public void onTabUnselected(TabLayout.Tab tab) {

			}

			@Override
			public void onTabReselected(TabLayout.Tab tab) {

			}
		});
	}

}
