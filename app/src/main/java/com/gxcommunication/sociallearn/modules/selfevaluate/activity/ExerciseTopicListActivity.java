package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.ExerciseTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hxc
 * <p>
 * 练习室
 */
public class ExerciseTopicListActivity extends BaseActivity {

    @BindView(R.id.study_topic_plate_tv)
    TextView studyTopicPlateTv;
    @BindView(R.id.study_topic_model_tv)
    TextView studyTopicModelTv;
    @BindView(R.id.study_topic_category_tv)
    TextView studyTopicCategoryTv;
    @BindView(R.id.study_topic_position_tv)
    TextView studyTopicPositionTv;
    @BindView(R.id.study_topic_level_tv)
    TextView studyTopicLevelTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    ExerciseTopicListAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_topic_list;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("练习篇");

        mAdapter = new ExerciseTopicListAdapter(this, loadTempDate());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(mAdapter);
        new PagerSnapHelper().attachToRecyclerView(recyclerView);

        mAdapter.setShowResultHint(true);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                int firs = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (view.getId() == R.id.item_topic_next_iv) {
                    recyclerView.scrollToPosition(firs + 1);

                }
            }
        });
    }

    private  List<StudyTopicMultiItemBean> loadTempDate() {

        List<StudyTopicMultiItemBean> list = new ArrayList<>();
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Radio_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Multiple_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Judge_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));

        return list;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }
}
