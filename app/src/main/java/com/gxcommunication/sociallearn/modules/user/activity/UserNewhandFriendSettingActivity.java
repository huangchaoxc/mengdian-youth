package com.gxcommunication.sociallearn.modules.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.user.adapter.UserInfoLikeAdapter;
import com.gxcommunication.sociallearn.utils.GlideEngine;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.ImageManager;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 用户模块 新手培训 设置交友偏好
 */
public class UserNewhandFriendSettingActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    @BindView(R.id.action_btn)
    TextView actionBtn;

    @BindView(R.id.user_info_head_iv)
    ImageView userInfoHeadIv;
    @BindView(R.id.user_info_nickname_tv)
    TextView userInfoNicknameTv;

    @BindView(R.id.friend_setting_marry_rg)//结婚选项
    RadioGroup friendSettingMarryRg;
    @BindView(R.id.friend_setting_marry_yes_rb)
    RadioButton friendSettingMarryYesRb;
    @BindView(R.id.friend_setting_marry_no_rb)
    RadioButton friendSettingMarryNoRb;
    @BindView(R.id.friend_setting_marry_unlimit_rb)
    RadioButton friendSettingMarryUnlimitRb;

    @BindView(R.id.friend_setting_sex_rg)//性别选项
    RadioGroup friendSettingSexRg;
    @BindView(R.id.friend_setting_sex_box_rb)
    RadioButton friendSettingSexBoxRb;
    @BindView(R.id.friend_setting_sex_girl_rb)
    RadioButton friendSettingSexGirlRb;
    @BindView(R.id.friend_setting_sex_unlimit_rb)
    RadioButton friendSettingSexUnlimitRb;

    @BindView(R.id.friend_setting_education_rg)//教育选项
    RadioGroup friendSettingEducationRg;
    @BindView(R.id.friend_setting_education_level_1_rg)
    RadioButton friendSettingEducationLevel1Rg;
    @BindView(R.id.friend_setting_education_level_2_rg)
    RadioButton friendSettingEducationLevel2Rg;
    @BindView(R.id.friend_setting_education_level_3_rg)
    RadioButton friendSettingEducationLevel3Rg;
    @BindView(R.id.friend_setting_education_level_unlimit_rg)
    RadioButton friendSettingEducationLevelUnlimitRg;


    UserInfoLikeAdapter mAdapter;

    private List<LocalMedia> filePaths = new ArrayList<>();//选择相册的图片


    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_newhand_friend_setting;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        setHeadActionTitle("设置交友偏好");

        mAdapter = new UserInfoLikeAdapter(this, DateCacheUtil.getRequestList(12));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(mAdapter);


        userInfoHeadIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PictureSelector.create(UserNewhandFriendSettingActivity.this)
                        .openGallery(PictureMimeType.ofImage())
                        .loadImageEngine(GlideEngine.createGlideEngine())
                        .maxSelectNum(1) //最多选择多少张图
                        .compress(true)//是否压缩
                        .enableCrop(true)//是否剪裁
                        .withAspectRatio(1,1)//裁剪比例
                        .selectionMedia(filePaths)//选中文件返回集合
                        .previewImage(false)
                        .previewVideo(false)
                        .forResult(PictureConfig.CHOOSE_REQUEST);
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {

    }

    //用来存放拍照之后的图片存储路径文件
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == PictureConfig.CHOOSE_REQUEST) {
            filePaths = PictureSelector.obtainMultipleResult(data);

            if (filePaths.size() > 0){
                LogHelper.e(GsonUtlils.objectToJson(filePaths.get(0).getPath()));
                LogHelper.e(GsonUtlils.objectToJson(filePaths.get(0).getCutPath()));
                ImageManager.getManager(UserNewhandFriendSettingActivity.this).loadCircleImage(filePaths.get(0).getCutPath(), R.drawable.ic_circle_user_default, userInfoHeadIv);

            }

        }

    }
}
