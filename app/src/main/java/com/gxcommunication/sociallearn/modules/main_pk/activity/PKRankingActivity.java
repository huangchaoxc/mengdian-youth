package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.main_pk.adapter.PKRankingAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.ExamRankingAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 *  PK排行榜 排行榜
 */
public class PKRankingActivity extends BaseActivity {

    @BindView(R.id.my_top_rank_level)
    TextView myLevel;
    @BindView(R.id.my_top_rank_integral)
    TextView myIntegral;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerview;

    PKRankingAdapter mAdapter;

	@Override
    protected int getLayoutId() {
        return R.layout.activity_pk_ranking;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("PK排行榜");

		mAdapter = new PKRankingAdapter(this, DateCacheUtil.getRequestList(9));
        mRecyclerview .setLayoutManager(new LinearLayoutManager(this));
        mRecyclerview.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.my_top_rank_level, R.id.my_top_rank_integral})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.my_top_rank_level:
                myLevel.setBackgroundColor(getResources().getColor(R.color.color_head));
                myIntegral.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                break;
            case R.id.my_top_rank_integral:
                myLevel.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                myIntegral.setBackgroundColor(getResources().getColor(R.color.color_head));
                break;
        }
    }
}
