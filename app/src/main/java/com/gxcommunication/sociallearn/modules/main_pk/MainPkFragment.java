package com.gxcommunication.sociallearn.modules.main_pk;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;


/**
 * Created by hxc on 2017/8/14.
 */

public class MainPkFragment extends BaseFragment {
	private View mView;

	@Override
	protected void initView() {

	}

	@Override
	protected void lazyLoad() {

	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if (mView == null) {
			mView = inflater.inflate(R.layout.fragment_main_pk, null);
		}
		return mView;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (mView.getParent() != null && mView != null){
			((ViewGroup)mView.getParent()).removeView(mView);
		}
	}
}
