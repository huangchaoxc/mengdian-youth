package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.TopicSearchListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 */
public class TopicSearchTypeActivity extends BaseActivity {

    @BindView(R.id.layout_topic_search_radio)
    LinearLayout layoutTopicSearchRadio;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private StudyCategoryPopupWindow mCategoryPopupWindow;

    TopicSearchListAdapter mAdapter;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_topic_search_type;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBothImage();
        head_action_both_title.setText("搜题");
        head_action_both_rightiv.setImageResource(R.mipmap.category_select_all_ic);

        head_action_both_rightiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCategoryPopupWindow.showAsDropDown(head_action_both_rightiv);
            }
        });
        mCategoryPopupWindow = new StudyCategoryPopupWindow(this);

        mAdapter = new TopicSearchListAdapter(this, R.layout.item_study_topic_radio_activity, DateCacheUtil.getRequestList(9));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.layout_topic_search_radio, R.id.action_btn})
    public void onViewClicked(View view) {
        Intent intent = new Intent(this, TopicSearchListActivity.class);
        switch (view.getId()) {
            case R.id.layout_topic_search_radio:
                intent.putExtra(TopicSearchListActivity.INTENT_KEY_TYPE, StudyTopicMultiItemBean.Topic_Radio_Type);
                startActivity(intent);
                break;
            case R.id.action_btn:
                mRecyclerView.setAdapter(mAdapter);
                break;

        }

    }
}
