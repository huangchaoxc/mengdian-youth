package com.gxcommunication.sociallearn.modules.splash.bean;


import com.gxcommunication.sociallearn.base.bean.BaseBean;

import java.io.Serializable;

public class SplashAdvertBean extends BaseBean {


    /**
     * success : true
     * message : 查询成功
     * code : 200
     * timestamp : 1572611304936
     * result : {"id":16,"adImage":"http://img.taoyouji666.com/86a48482fdc6b13ce80a9e8cc5b2b5f7","adUrl":"http://wwwasdasdsa222","adProductId":"","type":1,"adSort":2,"noOff":1}
     * count : 0
     */

    private boolean success;
    private String message;
    private int code;
    private long timestamp;
    private ResultBean result;
    private int count;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static class ResultBean implements Serializable {
        /**
         * id : 16
         * adImage : http://img.taoyouji666.com/86a48482fdc6b13ce80a9e8cc5b2b5f7
         * adUrl : http://wwwasdasdsa222
         * adProductId :
         * type : 1
         * adSort : 2
         * noOff : 1
         */

        private int id;
        private String adImage;
        private String adUrl;
        private String adProductId;
        private int type;
        private int adSort;
        private int noOff;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAdImage() {
            return adImage;
        }

        public void setAdImage(String adImage) {
            this.adImage = adImage;
        }

        public String getAdUrl() {
            return adUrl;
        }

        public void setAdUrl(String adUrl) {
            this.adUrl = adUrl;
        }

        public String getAdProductId() {
            return adProductId;
        }

        public void setAdProductId(String adProductId) {
            this.adProductId = adProductId;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getAdSort() {
            return adSort;
        }

        public void setAdSort(int adSort) {
            this.adSort = adSort;
        }

        public int getNoOff() {
            return noOff;
        }

        public void setNoOff(int noOff) {
            this.noOff = noOff;
        }
    }
}
