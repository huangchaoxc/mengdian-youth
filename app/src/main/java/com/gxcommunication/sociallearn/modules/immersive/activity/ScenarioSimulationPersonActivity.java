package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 场景模拟列 确认人员
 */
public class ScenarioSimulationPersonActivity extends BaseActivity {

    @BindView(R.id.user_info_head_iv)
    ImageView userInfoHeadIv;
    @BindView(R.id.user_info_nickname_tv)
    TextView userInfoNicknameTv;
    @BindView(R.id.user_info_position_tv)
    TextView userInfoPositionTv;
    @BindView(R.id.user_info_check_tv)
    TextView userInfoCheckTv;
    @BindView(R.id.user_info_sure_tv)
    TextView userInfoSureTv;
    @BindView(R.id.user_info_check_iv)
    ImageView userInfoCheckIv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_person;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("确认人员");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.user_info_check_tv, R.id.user_info_sure_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.user_info_check_tv://确定人员通过开始
                if (userInfoCheckTv.isSelected()) {
                    userInfoCheckTv.setSelected(false);
//                    userInfoCheckIv.setVisibility(View.GONE);
                } else {
                    userInfoCheckTv.setSelected(true);
//                    userInfoCheckIv.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.user_info_sure_tv://准备工作
                if (userInfoCheckTv.isSelected()) {
                    IntentUtils.startActivity(this,ScenarioSimulationPrepareActivity.class);
                } else {
                   showToast("请确定巡视人员");
                }

                break;

        }
    }
}
