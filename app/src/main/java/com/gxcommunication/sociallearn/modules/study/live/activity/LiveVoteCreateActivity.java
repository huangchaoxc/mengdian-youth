package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveVoteCreateAdapter;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/9/20.
 */

public class LiveVoteCreateActivity extends BaseActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    LiveVoteCreateAdapter mAdapter;
    @BindView(R.id.live_vote_create_title_edit)
    EditText liveVoteCreateTitleEdit;
    @BindView(R.id.live_vote_create_remark_edit)
    EditText liveVoteCreateRemarkEdit;

    @BindView(R.id.user_type_rb)
    RadioGroup userTypeRb;

    @BindView(R.id.user_type_radio_rb)
    RadioButton userTypeRadioRb;
    @BindView(R.id.user_type_mul_rb)
    RadioButton userTypeMulRb;

    LiveVoteCreateAndDetailBean mLiveVoteCreateBean = new LiveVoteCreateAndDetailBean();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_vote_create;
    }

    @Override
    protected void init() {
        super.initHeadActionBar();
        setHeadActionTitle("创建投票");

        mAdapter = new LiveVoteCreateAdapter(this, DateCacheUtil.getRequestList(2));
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(mAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.action_add_btn, R.id.action_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.action_add_btn:
                mAdapter.addData(new RequestResBean());
                break;
            case R.id.action_btn:
                createVote();
                IntentUtils.startActivityForString(LiveVoteCreateActivity.this, LiveVoteHostDetailActivity.class,
                        LiveVoteHostDetailActivity.INTENT_KEY_CONTENT, GsonUtlils.objectToJson(mLiveVoteCreateBean));
                finish();
                break;
        }
    }

    private void createVote() {

        if (liveVoteCreateTitleEdit.getText().toString().isEmpty()) {
            showToast("请输入标题");
            return;
        }

        if (liveVoteCreateRemarkEdit.getText().toString().isEmpty()) {
            showToast("请输入描述内容");
            return;
        }

        mLiveVoteCreateBean.title = liveVoteCreateTitleEdit.getText().toString();
        mLiveVoteCreateBean.content = liveVoteCreateRemarkEdit.getText().toString();

        if (userTypeRb.getCheckedRadioButtonId() == R.id.user_type_radio_rb){
            mLiveVoteCreateBean.voteModeCode = 0;
        }else {
            mLiveVoteCreateBean.voteModeCode = 1;
        }

        for (RequestResBean item : mAdapter.getData()){
            mLiveVoteCreateBean.data.add(new LiveVoteCreateAndDetailBean.DataBean(item.getMsg()));
        }

    }
}
