package com.gxcommunication.sociallearn.modules.user.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 用户信息页
 */
public class UserInfoActivity extends BaseActivity {

    @BindView(R.id.user_info_head_iv)
    ImageView userInfoHeadIv;
    @BindView(R.id.user_info_nickname_tv)
    TextView userInfoNicknameTv;
    @BindView(R.id.user_info_level_tv)
    TextView userInfoLevelTv;
    @BindView(R.id.user_info_experience_tv)
    TextView userInfoExperienceTv;
    @BindView(R.id.user_info_integral_tv)
    TextView userInfoIntegralTv;
    @BindView(R.id.tv_user_phone)
    TextView tvUserPhone;
    @BindView(R.id.tv_user_profession)
    TextView tvUserProfession;
    @BindView(R.id.tv_user_wordtype)
    TextView tvUserWordtype;
    @BindView(R.id.tv_user_level)
    TextView tvUserLevel;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_info;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick({R.id.user_info_head_iv, R.id.action_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.user_info_head_iv:
                break;
            case R.id.action_btn:
                break;
        }
    }
}
