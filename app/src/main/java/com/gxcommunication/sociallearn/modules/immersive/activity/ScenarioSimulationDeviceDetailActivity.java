package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.adapter.SimulationDangerAnalyseAdapter;
import com.gxcommunication.sociallearn.modules.immersive.adapter.SimulationDeviceDetailAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.gxcommunication.sociallearn.widget.BetterSpinner;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 场景模拟列 设备详情
 */
public class ScenarioSimulationDeviceDetailActivity extends BaseActivity {

    @BindView(R.id.scenario_simulation_title_spinner)
    BetterSpinner titleSpinner;

    @BindView(R.id.scenario_simulation_img)
    ImageView scenarioSimulationImg;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.scenario_simulation_action_tv)
    TextView scenarioSimulationActionTv;

    SimulationDeviceDetailAdapter mAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_device_detail;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("检查详情");

        mAdapter = new SimulationDeviceDetailAdapter(this, DateCacheUtil.getRequestList(8));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter);
        loadSpinner();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick(R.id.scenario_simulation_action_tv)
    public void onViewClicked() { // 可以进行下一项巡视或者结束巡视

        //结束巡视 跳转
        IntentUtils.startActivity(this,ScenarioSimulationCompleteActivity.class);
        finish();
    }

    private void loadSpinner() {//可以下拉选择要进行检查的设备

        String[] list = new  String[]{"电容器 01","电容器 02","逆变器 01","逆变器 02","变压器 01","变压器 02"};

        ArrayAdapter<String> sortAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, list);
        titleSpinner.setAdapter(sortAdapter);

        titleSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LogHelper.e("-----------" + position);
            }
        });
    }


    /**
     * 监听返回--是否退出程序
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean flag = true;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 是否退出应用
            showToast("是否要");

            showDiglog("是否要退出模拟实训？", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            flag = super.onKeyDown(keyCode, event);
        }
        return flag;
    }
}
