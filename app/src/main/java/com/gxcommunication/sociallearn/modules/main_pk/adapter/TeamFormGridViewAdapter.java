package com.gxcommunication.sociallearn.modules.main_pk.adapter;

import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.utils.ToastUtils;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import java.util.List;


/**
 * Created by hxc on 2017/5/8.
 */

public class TeamFormGridViewAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	private RecyclerView.LayoutParams itemLayoutParams;
	private int horizentalNum = 4;

	public TeamFormGridViewAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_pk_team_form_layout, data);
		mContext = context;
		setItemWidth(getWith(mContext));
	}

	public static int getWith(Context context) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);

		int width = wm.getDefaultDisplay().getWidth();
		return width;
	}

	/**
	 * 设置每一个Item的宽高
	 */
	public void setItemWidth(int screenWidth) {
		int horizentalSpace = mContext.getResources().getDimensionPixelSize(
				R.dimen.sticky_item_horizontalSpacing);
		int itemWidth = (screenWidth - (horizentalSpace * (horizentalNum - 1)) - DensityUtil.dp2px(42f))
				/ horizentalNum;
		this.itemLayoutParams = new RecyclerView.LayoutParams(itemWidth, itemWidth);
	}

	@Override
	protected void convert(final BaseViewHolder helper, RequestResBean item) {
		helper.itemView.setLayoutParams(itemLayoutParams);
		ImageView imageView = helper.getView(R.id.pk_team_userhead_iv);
		if (helper.getLayoutPosition() + 1 == getItemCount()){

			helper.setVisible(R.id.pk_team_add_layout,true);
			helper.setGone(R.id.pk_team_userhead_iv,false);
			helper.setGone(R.id.pk_team_name_tv,false);
		}else {
			helper.setVisible(R.id.pk_team_add_layout,false);
			helper.setGone(R.id.pk_team_userhead_iv,true);
			helper.setGone(R.id.pk_team_name_tv,true);
		}

		helper.setOnClickListener(R.id.pk_team_add_layout, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ToastUtils.showShort(mContext,"邀请队员");
			}
		});


	}
}
