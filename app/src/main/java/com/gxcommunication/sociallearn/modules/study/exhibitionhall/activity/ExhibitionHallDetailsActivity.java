package com.gxcommunication.sociallearn.modules.study.exhibitionhall.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.exhibitionhall.adapter.ExhibitionHallDetailImgViewAdapter;
import com.gxcommunication.sociallearn.utils.BannerGlideImageResLoader;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 创新展览馆-成果详情
 */
public class ExhibitionHallDetailsActivity extends BaseActivity {

    @BindView(R.id.home_head_banner)
    Banner homeHeadBanner;// 顶部轮播图
    @BindView(R.id.img_list)
    RecyclerView imgList;// 展示横向滚动图片列表
    @BindView(R.id.webInfo)
    WebView webInfo;// 内容信息展示页
    @BindView(R.id.show_num)
    TextView showNum;// 查看数量
    @BindView(R.id.show_top)
    TextView showTop;// 点赞数量
    @BindView(R.id.show_collection)
    TextView showCollection;// 收藏数量
    @BindView(R.id.btn_share)
    ImageView btnShare;// 分享按钮
    @BindView(R.id.show_supper)
    TextView showSupper; // 查看数量

    @Override
    protected int getLayoutId() {
        return R.layout.activity_exhibition_hall_details;
    }

    @Override
    protected void init() {
        initHeadActionBar();
        setHeadActionTitle("成果详情");
        /*加载图片*/
        loadBanner();
        initImgList();
        this.setInfo(R.id.t_info);
    }

    /**
     * 初始化图片集，可横向滚动
     */
    private void initImgList() {
        imgList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        imgList.setAdapter(new ExhibitionHallDetailImgViewAdapter(this, DateCacheUtil.getRequestList(20)));
    }

    @OnClick({R.id.t_info, R.id.t_exhibition, R.id.t_result, R.id.t_author})
    public void onTabClick(View view) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            TextView child = (TextView) viewGroup.getChildAt(i);
            child.setBackgroundColor(getResources().getColor(R.color.exhibition_tad_default_bg));
            child.setTextColor(getResources().getColor(R.color.exhibition_tad_default));
        }
        TextView indexView = (TextView) view;
        indexView.setBackgroundColor(getResources().getColor(R.color.exhibition_tab_select));
        indexView.setTextColor(getResources().getColor(R.color.white));
        this.setInfo(view.getId());
    }

    private void setInfo(int id) {
        String html = "";
        switch (id) {
            case R.id.t_info:
                html = "<h5>用户信息</h5>";
                break;
            case R.id.t_exhibition:
                html = "<p>荣誉奖励</p>";
                break;
            case R.id.t_result:
                html = "<p style=\"color: red;\">应用效果</p>";
                break;
            case R.id.t_author:
                html = "<p>作者效果</p>";
                break;
        }
        webInfo.loadData(html, "text/html", "utf-8");

    }


    private void loadBanner() {

        List<Integer> bannerList = new ArrayList<>();
        bannerList.add(R.mipmap.main_banner);
        bannerList.add(R.drawable.ic_circle_user_default);
        bannerList.add(R.mipmap.main_banner);
        bannerList.add(R.mipmap.main_banner);

        homeHeadBanner.isAutoPlay(true);//是否自动滚动
        homeHeadBanner.setDelayTime(5000);//滚动间隔时间

        homeHeadBanner.setImageLoader(new BannerGlideImageResLoader());
        homeHeadBanner.setImages(bannerList);
        homeHeadBanner.start();

        homeHeadBanner.setOnBannerListener(new OnBannerListener() {
            @Override
            public void OnBannerClick(int position) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}