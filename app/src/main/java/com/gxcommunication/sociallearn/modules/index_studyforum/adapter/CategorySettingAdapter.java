package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *  学创论坛 你问我答 二级分类列表
 */

public class CategorySettingAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {
	private  boolean isShowEdit  = false;

	public CategorySettingAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_category_setting_layout, data);
		mContext = context;
	}

	public void setShowEdit(boolean showEdit) {
		isShowEdit = showEdit;
		notifyDataSetChanged();
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {

		if (isShowEdit){
			helper.setGone(R.id.ivAdd,true);
		}else {
			helper.setGone(R.id.ivAdd,false);
		}

		switch (helper.getAdapterPosition()) {
			case 0:
				helper.setText(R.id.tvTitle, "全部");

				break;
			case 1:
				helper.setText(R.id.tvTitle, "化学");

				break;

			case 2:
				helper.setText(R.id.tvTitle, "线路运行与检修");

				break;

			case 3:
				helper.setText(R.id.tvTitle, "营业用电");

				break;
			default:
				helper.setText(R.id.tvTitle, "其他");
				break;
		}

	}
}
