package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 场景模拟列 巡视完成 巡视报告
 */
public class ScenarioSimulationCompleteActivity extends BaseActivity {


    @BindView(R.id.complete_principal_tv)
    TextView completePrincipalTv;//负责人
    @BindView(R.id.complete_second_principal_tv)
    TextView completeSecondPrincipalTv;//巡视人
    @BindView(R.id.complete_time_tv)
    TextView completeTimeTv;// 巡视时间
    @BindView(R.id.complete_result_tv)
    TextView completeResultTv;//巡视结果
    @BindView(R.id.complete_result_summary_tv)
    TextView summaryTv;//巡视总结
    @BindView(R.id.complete_finish_tv)
    TextView finishTv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_complete;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("巡视报告");


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.complete_finish_tv)
    public void onViewClicked() {
        IntentUtils.startActivity(this,ScenarioSimulationListActivity.class);
        finish();

    }
}
