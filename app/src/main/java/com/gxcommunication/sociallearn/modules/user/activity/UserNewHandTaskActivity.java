package com.gxcommunication.sociallearn.modules.user.activity;

import android.view.View;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.OnClick;

/**
 * @author hxc
 * 用户模块 新手任务
 */
public class UserNewHandTaskActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_newhand_task;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        setHeadActionTitle("新手任务");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick({R.id.user_task_userinfo_btn, R.id.user_task_frienddeclaration_tn, R.id.user_task_friendsetting_btn, R.id.user_task_newhandlearn_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.user_task_userinfo_btn://完善个人资料
				IntentUtils.startActivity(this,SettingUserInfoActivity.class);
                break;
            case R.id.user_task_frienddeclaration_tn://发布交友宣言
                IntentUtils.startActivity(this,UserNewhandFriendDeclarationActivity.class);
                break;
            case R.id.user_task_friendsetting_btn://设置好友偏好
                IntentUtils.startActivity(this,UserNewhandFriendSettingActivity.class);
                break;
            case R.id.user_task_newhandlearn_btn://设置新手考核
                IntentUtils.startActivity(this,UserNewhandLearnActivity.class);
                break;
        }
    }
}
