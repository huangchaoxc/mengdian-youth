package com.gxcommunication.sociallearn.modules.selfevaluate.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class ExamRoomHistoryAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public ExamRoomHistoryAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_exam_room_history, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.item_title_tv,"技能鉴定").
		setText(R.id.item_category_tv,"变电运行与检修  初级");

//		ImageManager.getManager(mContext).loadUrlFitImage("",helper.getView(R.id.item_home_message_pic_iv));
    }


}
