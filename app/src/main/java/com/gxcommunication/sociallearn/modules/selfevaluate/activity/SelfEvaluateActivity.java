package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.SelfEvaluateAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.gxcommunication.sociallearn.widget.BetterSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 自我评定
 */
public class SelfEvaluateActivity extends BaseActivity {

//    @BindView(R.id.evaluate_jineng_btn)
//    TextView evaluateJinengBtn;
//    @BindView(R.id.evaluate_angui_btn)
//    TextView evaluateAnguiBtn;
//    @BindView(R.id.evaluate_pukao_btn)
//    TextView evaluatePukaoBtn;


    @BindView(R.id.evaluate_recyclerview)
    RecyclerView evaluateRecyclerview;
    @BindView(R.id.evaluate_xue_btn)
    TextView evaluateXueBtn;
    @BindView(R.id.evaluate_lian_btn)
    TextView evaluateLianBtn;
    @BindView(R.id.evaluate_kao_btn)
    TextView evaluateKaoBtn;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.study_profession_tv)
    BetterSpinner studyProfessionSpinner;// 专业
    @BindView(R.id.study_work_type_tv)
    BetterSpinner studyWorkTypeSpinner;//工种

    StudySecondCategoryAdapter mSecondCategoryAdapter;
    SelfEvaluateAdapter mAdapter;



    @Override
    protected int getLayoutId() {
        return R.layout.activity_self_evaluate;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("自我提升");

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.addTab(tabLayout.newTab().setText("技能鉴定"));
        tabLayout.addTab(tabLayout.newTab().setText("安规"));
        tabLayout.addTab(tabLayout.newTab().setText("公共安全"));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {//技能鉴定 默认第一项   安规 公共安全 无工种  所以不可以选择
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    studyWorkTypeSpinner.setSelection(1);
                    studyWorkTypeSpinner.setEnabled(true);
                } else if (tab.getPosition() == 1) {
                    studyWorkTypeSpinner.setSelection(0);
                    studyWorkTypeSpinner.setEnabled(false);
                } else {
                    studyWorkTypeSpinner.setSelection(0);
                    studyWorkTypeSpinner.setEnabled(false);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        mSecondCategoryAdapter = new StudySecondCategoryAdapter(this, DateCacheUtil.getRequestList(9));
//        evaluateCategotyRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        evaluateCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);
//        mSecondCategoryAdapter.setSelectItem(0);

        mAdapter = new SelfEvaluateAdapter(this, DateCacheUtil.getRequestList(9));
        evaluateRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        evaluateRecyclerview.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mAdapter.setmSelectPos(position);
            }
        });

        loadSpinner();
    }

    private void loadSpinner() {

        String[] listProfession = getResources().getStringArray(R.array.self_evaluate_profression_sponner);
        ArrayAdapter<String> professionAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listProfession);
        studyProfessionSpinner.setAdapter(professionAdapter);
        studyProfessionSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        String[] listType = getResources().getStringArray(R.array.self_evaluate_type_sponner);
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, listType);
        studyWorkTypeSpinner.setAdapter(typeAdapter);
        studyWorkTypeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.evaluate_jineng_btn, R.id.evaluate_angui_btn, R.id.evaluate_pukao_btn, R.id.evaluate_xue_btn, R.id.evaluate_lian_btn, R.id.evaluate_kao_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.evaluate_jineng_btn://技能鉴定
                selectFirstCategory(R.id.evaluate_jineng_btn);
                break;
            case R.id.evaluate_angui_btn://安规
                selectFirstCategory(R.id.evaluate_angui_btn);
                break;
            case R.id.evaluate_pukao_btn://普考
                selectFirstCategory(R.id.evaluate_pukao_btn);
                break;
            case R.id.evaluate_xue_btn://学
                if (mAdapter.getmSelectPos() < 0){
                    showToast("请选择等级");
                    return;
                }
                IntentUtils.startActivity(this, StudyRoomActivity.class);
                break;
            case R.id.evaluate_lian_btn://练
                if (mAdapter.getmSelectPos() < 0){
                    showToast("请选择等级");
                    return;
                }
                IntentUtils.startActivity(this, ExerciseRoomActivity.class);
                break;
            case R.id.evaluate_kao_btn://考
                if (mAdapter.getmSelectPos() < 0){
                    showToast("请选择等级");
                    return;
                }
                IntentUtils.startActivity(this, ExamRoomRemindActivity.class);
                break;
        }
    }

    private void selectFirstCategory(int resid) {

//        if (resid == R.id.evaluate_jineng_btn) {//技能鉴定
//            evaluateJinengBtn.setBackgroundColor(getResources().getColor(R.color.color_head));
//            evaluateAnguiBtn.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//            evaluatePukaoBtn.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//        } else if (resid == R.id.evaluate_angui_btn) {//安规
//            evaluateJinengBtn.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//            evaluateAnguiBtn.setBackgroundColor(getResources().getColor(R.color.color_head));
//            evaluatePukaoBtn.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//        } else if (resid == R.id.evaluate_pukao_btn) {//普考
//            evaluateJinengBtn.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//            evaluateAnguiBtn.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//            evaluatePukaoBtn.setBackgroundColor(getResources().getColor(R.color.color_head));
//        }
    }

    @OnClick({R.id.study_profession_tv, R.id.study_work_type_tv})
    public void onViewTypeClicked(View view) {
        switch (view.getId()) {
            case R.id.study_profession_tv:// 专业
                break;
            case R.id.study_work_type_tv:// 工种
                break;
        }
    }
}
