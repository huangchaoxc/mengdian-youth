package com.gxcommunication.sociallearn.modules.study.video.adapter;

import android.content.Context;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;

import java.util.List;


/**
 * 视频标签卡
 * Created by hxc on 2017/8/7
 */

public class VedioListerAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public VedioListerAdapter(Context context, List<String> data) {
        super(R.layout.item_vedio_list, data);
        mContext = context;
    }
    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.video_title_tv, "  如何正确佩戴安全帽" );

    }
	/**
	 * 获取格式化内容
	 * @param id
	 * @param formats
	 * @return
	 */
    private String getFormat(int id,Object ...formats){
		return mContext.getResources().getString(id, formats);
	}
}
