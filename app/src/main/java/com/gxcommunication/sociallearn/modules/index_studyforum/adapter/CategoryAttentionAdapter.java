package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *  学创论坛 你问我答 二级分类列表
 */

public class CategoryAttentionAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {
	private  int selectItem  = -1;

	public CategoryAttentionAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_category_attention_category, data);
		mContext = context;
	}

	public void setSelectItem(int selectItem) {
		this.selectItem = selectItem;
		notifyDataSetChanged();
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {


		switch (helper.getAdapterPosition()) {
			case 0:
				helper.setText(R.id.tvTitle, "全部");

				break;
			case 1:
				helper.setText(R.id.tvTitle, "化学");

				break;

			case 2:
				helper.setText(R.id.tvTitle, "线路运行与检修");

				break;

			case 3:
				helper.setText(R.id.tvTitle, "营业用电");

				break;
			default:
				helper.setText(R.id.tvTitle, "其他");
				break;
		}

	}
}
