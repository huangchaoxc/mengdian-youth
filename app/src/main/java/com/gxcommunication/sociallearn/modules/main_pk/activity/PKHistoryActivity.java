package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.main_pk.adapter.PKHistoryAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.StudyRoomHistoryTopicActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.ExamRoomHistoryAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.hxc.toolslibrary.example.base.CommonSmartRefreshActivity;

/**
 *  
 * @author hxc
 * PK 记录 列表
 */
public class PKHistoryActivity extends CommonSmartRefreshActivity {
	private PKHistoryAdapter mAdapter;

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_stu;
//    }

    @Override
    protected void init() {
    	super.init();
		initView();
    }


	public void initView() {
        head_action_title.setText("PK记录");
		mAdapter = new PKHistoryAdapter(this, DateCacheUtil.getRequestList(6));
		mRecyclerView.setAdapter(mAdapter);

		mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
				IntentUtils.startActivity(PKHistoryActivity.this,StudyRoomHistoryTopicActivity.class);

			}
		});
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}
	
	


}
