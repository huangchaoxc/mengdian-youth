package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class LiveVoteProceedDetailAdapter extends BaseQuickAdapter<LiveVoteCreateAndDetailBean.DataBean, BaseViewHolder> {

	public int voteType = 0;
	public int selectPos = -1;

	public LiveVoteProceedDetailAdapter(Context context, List<LiveVoteCreateAndDetailBean.DataBean> data, int type) {
		super(R.layout.item_live_vote_proceed_detail_activity, data);
		mContext = context;

		voteType = type;
	}

	public int getSelectPos() {//单选模式下选择
		return selectPos;
	}

	public void setSelectPos(int selectPos) {
		this.selectPos = selectPos;
		notifyDataSetChanged();
	}

	@Override
	protected void convert(BaseViewHolder helper, LiveVoteCreateAndDetailBean.DataBean item) {


		if (voteType == 0){//单选
			helper.setGone(R.id.item_live_vote_radio_layout,true);
			helper.setGone(R.id.item_live_vote_multiple_layout,false);

			helper.setText(R.id.item_live_vote_title_tv,item.voteContent);
			if (selectPos == helper.getAdapterPosition()){
				helper.setChecked(R.id.item_live_vote_title_tv,true);
			}else {
				helper.setChecked(R.id.item_live_vote_title_tv,false);
			}

		}else {//多选
			helper.setGone(R.id.item_live_vote_radio_layout,false);
			helper.setGone(R.id.item_live_vote_multiple_layout,true);

			helper.setText(R.id.item_live_vote_mul_title_cb,item.voteContent);
		}

	}
}
