package com.gxcommunication.sociallearn.modules.index_studyforum.bean;

public class StudyAnswerDetailResListBean {

    public StudyAnswerDetailResListBean(String headImage, String userName, String awardScore, String createTime, String content, String userId, int isAccept) {
        this.headImage = headImage;
        this.userName = userName;
        this.awardScore = awardScore;
        this.createTime = createTime;
        this.content = content;
        this.userId = userId;
        this.isAccept = isAccept;
    }

    public String headImage;
    public String userName;
    public String awardScore;//奖励分数
    public String createTime;//时间
    public String content;//
    public String userId;//
    public int    isAccept;//回答是否被采纳  0 是已采纳， 1是未采纳
    public boolean isItemAccept = false; // 是否本条回答为采纳答案
}
