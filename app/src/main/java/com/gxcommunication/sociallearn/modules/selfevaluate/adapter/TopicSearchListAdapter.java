package com.gxcommunication.sociallearn.modules.selfevaluate.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class TopicSearchListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public TopicSearchListAdapter(Context context,int resid, List<RequestResBean> data) {
		super(resid, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
			int pos = helper.getLayoutPosition() + 1;
		helper.setText(R.id.item_learn_pos_tv,"第" + pos+ "题");

	}
}
