package com.gxcommunication.sociallearn.modules.user.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 */
public class RegisterActivity extends BaseActivity {

    @BindView(R.id.user_inner_checkbox)
    RadioButton userInnerCheckbox;
    @BindView(R.id.user_external_checkbox)
    RadioButton userExternalCheckbox;
    @BindView(R.id.user_name_edit)
    EditText userNameEdit;
    @BindView(R.id.user_marry_checkbox_has)
    RadioButton userMarryCheckboxHas;
    @BindView(R.id.user_marry_checkbox_no)
    RadioButton userMarryCheckboxNo;
    @BindView(R.id.user_num_edit)
    EditText userNumEdit;
    @BindView(R.id.user_sex_checkbox_man)
    RadioButton userSexCheckboxHas;
    @BindView(R.id.user_sex_checkbox_girl)
    RadioButton userSexCheckboxNo;
    @BindView(R.id.user_birthday_edit)
    EditText userBirthdayEdit;
    @BindView(R.id.user_education_edit)
    EditText userEducationEdit;
    @BindView(R.id.user_phone_edit)
    EditText userPhoneEdit;
    @BindView(R.id.user_height_edit)
    EditText userHeightEdit;

    @BindView(R.id.user_word_company_edit)
    EditText userWordCompanyEdit;
    @BindView(R.id.user_word_tw_edit)
    EditText userWordTwEdit;
    @BindView(R.id.user_word_tzb_edit)
    EditText userWordTzbEdit;
    @BindView(R.id.user_word_profession_edit)
    EditText userWordProfessionEdit;
    @BindView(R.id.user_word_wordtype_edit)
    EditText userWordWordtypeEdit;
    @BindView(R.id.user_word_position_edit)
    EditText userWordPositionEdit;
    @BindView(R.id.user_word_area_edit)
    EditText userWordAreaEdit;

    @BindView(R.id.user_type_rb)
    RadioGroup userTypeRadioGroup;

    @BindView(R.id.user_word_company_spinner)
    Spinner userWordCompanySpinner;
    @BindView(R.id.user_word_tw_spinner)
    Spinner userWordTwSpinner;
    @BindView(R.id.user_word_tzb_spinner)
    Spinner userWordTzbSpinner;
    @BindView(R.id.user_word_profession_spinner)
    Spinner userWordProfessionSpinner;
    @BindView(R.id.user_word_wordtype_spinner)
    Spinner userWordWordtypeSpinner;
    @BindView(R.id.user_word_position_spinner)
    Spinner userWordPositionSpinner;
    @BindView(R.id.user_word_area_spinner)
    Spinner userWordAreaSpinner;

    @BindView(R.id.user_num_id_layout)
    LinearLayout userNumIdLayout;
    @BindView(R.id.user_education_spinner)
    Spinner userEducationSpinner;
    @BindView(R.id.user_word_tw_layout)
    LinearLayout userWordTwLayout;
    @BindView(R.id.user_word_tzb_layout)
    LinearLayout userWordTzbLayout;
    @BindView(R.id.user_word_profession_layout)
    LinearLayout userWordProfessionLayout;
    @BindView(R.id.user_word_wordtype_layout)
    LinearLayout userWordWordtypeLayout;
    @BindView(R.id.user_word_position_layout)
    LinearLayout userWordPositionLayout;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_register;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {


        userTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.user_inner_checkbox) {//内部用户
                    showInnerUserInfo();
                } else {
                    showExternalUserInfo();
                }
            }
        });
        showInnerUserInfo();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick({R.id.return_btn, R.id.btn_send_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.return_btn:
                finish();
                break;
            case R.id.btn_send_message:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    private void showInnerUserInfo() {

        userNumIdLayout.setVisibility(View.VISIBLE);//工号

        userWordCompanySpinner.setVisibility(View.VISIBLE);
        userWordCompanyEdit.setVisibility(View.GONE);

        userWordTwSpinner.setVisibility(View.VISIBLE);
        userWordTwLayout.setVisibility(View.VISIBLE);

        userWordTzbSpinner.setVisibility(View.VISIBLE);
        userWordTzbLayout.setVisibility(View.VISIBLE);

        userWordProfessionSpinner.setVisibility(View.VISIBLE);
        userWordProfessionLayout.setVisibility(View.VISIBLE);

        userWordWordtypeSpinner.setVisibility(View.VISIBLE);
        userWordWordtypeLayout.setVisibility(View.VISIBLE);

        userWordPositionSpinner.setVisibility(View.VISIBLE);
        userWordPositionLayout.setVisibility(View.VISIBLE);

        userWordAreaSpinner.setVisibility(View.VISIBLE);
        userWordAreaEdit.setVisibility(View.GONE);

        loadUserTypeSpinner();
        loadEdcationSpinner();
    }

    private void showExternalUserInfo() {
        userNumIdLayout.setVisibility(View.GONE);//工号

        userWordCompanySpinner.setVisibility(View.GONE);
        userWordCompanyEdit.setVisibility(View.VISIBLE);

//        userWordTwSpinner.setVisibility(View.GONE);
        userWordTwLayout.setVisibility(View.GONE);

//        userWordTzbSpinner.setVisibility(View.GONE);
        userWordTzbLayout.setVisibility(View.GONE);

//        userWordProfessionSpinner.setVisibility(View.GONE);
        userWordProfessionLayout.setVisibility(View.GONE);

//        userWordWordtypeSpinner.setVisibility(View.GONE);
        userWordWordtypeLayout.setVisibility(View.GONE);

//        userWordPositionSpinner.setVisibility(View.GONE);
        userWordPositionLayout.setVisibility(View.GONE);

        userWordAreaSpinner.setVisibility(View.GONE);
        userWordAreaEdit.setVisibility(View.VISIBLE);

    }

    private void loadUserTypeSpinner() {

        // 建立Adapter并且绑定数据源
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, DateCacheUtil.getStringList(9));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //绑定 Adapter到控件
        userWordCompanySpinner.setAdapter(adapter);

        userWordCompanySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        userWordTwSpinner.setAdapter(adapter);
        userWordTzbSpinner.setAdapter(adapter);
        userWordProfessionSpinner.setAdapter(adapter);
        userWordWordtypeSpinner.setAdapter(adapter);
        userWordPositionSpinner.setAdapter(adapter);
        userWordAreaSpinner.setAdapter(adapter);

    }

    private void loadEdcationSpinner(){

        // 建立Adapter并且绑定数据源
        String[] array =  {"本科","硕士研究生","博士研究生"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //绑定 Adapter到控件
        userEducationSpinner.setAdapter(adapter);
    }
}
