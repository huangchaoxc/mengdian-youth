package com.gxcommunication.sociallearn.modules.main_my;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.modules.main_my.activity.MyBrowseInfoActivity;
import com.gxcommunication.sociallearn.modules.main_my.activity.MyCollectActivity;
import com.gxcommunication.sociallearn.modules.main_my.activity.MyIntegralActivity;
import com.gxcommunication.sociallearn.modules.main_my.activity.MyIssueInfoActivity;
import com.gxcommunication.sociallearn.modules.main_my.activity.MyMemberActiActivity;
import com.gxcommunication.sociallearn.modules.main_my.activity.TopRankingActivity;
import com.gxcommunication.sociallearn.modules.main_pk.activity.PKRankingActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.ExamRoomHistoryActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.activity.ExamRoomRemindActivity;
import com.gxcommunication.sociallearn.modules.user.activity.UserNewHandTaskActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/8/14.
 */

public class MainMineFragment extends BaseFragment {
    @BindView(R.id.main_mine_setting_userhead)
    ImageView mainMineSettingUserhead;
    @BindView(R.id.main_mine_setting_nickname_tv)
    TextView mainMineSettingNicknameTv;
    @BindView(R.id.main_mine_setting_level_tv)
    TextView mainMineSettingLevelTv;
    private View mView;

    @Override
    protected void initView() {

    }

    @Override
    protected void lazyLoad() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_main_mine, null);
        }
        ButterKnife.bind(this, mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mView.getParent() != null && mView != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
    }


    @OnClick({R.id.main_mine_setting_iv, R.id.main_mine_setting_qrcode_iv, R.id.main_mine_task_01, R.id.main_mine_task_02, R.id.main_mine_task_03,
            R.id.main_mine_tools_01, R.id.main_mine_tools_02, R.id.main_mine_tools_03, R.id.main_mine_tools_21, R.id.main_mine_tools_22, R.id.main_mine_tools_23,
            R.id.main_mine_tools_41, R.id.main_mine_tools_42, R.id.main_mine_friend_01, R.id.main_mine_friend_02, R.id.main_mine_friend_03})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.main_mine_setting_iv:
                break;
            case R.id.main_mine_setting_qrcode_iv:
                break;
            case R.id.main_mine_task_01://我的积分
                IntentUtils.startActivity(getActivity(), MyIntegralActivity.class);
                break;
            case R.id.main_mine_task_02://我的任务
                IntentUtils.startActivity(getActivity(), UserNewHandTaskActivity.class);
                break;
            case R.id.main_mine_task_03://成长足迹
                break;
            case R.id.main_mine_tools_01://我的排名
                IntentUtils.startActivity(getActivity(), TopRankingActivity.class);
                break;
            case R.id.main_mine_tools_02://我的浏览
                IntentUtils.startActivity(getActivity(), MyBrowseInfoActivity.class);
                break;
            case R.id.main_mine_tools_03://我的小组
                break;
            case R.id.main_mine_tools_21://我的收藏
                IntentUtils.startActivity(getActivity(), MyCollectActivity.class);
                break;
            case R.id.main_mine_tools_22://我的课程
                break;
            case R.id.main_mine_tools_23://我的发布
                IntentUtils.startActivity(getActivity(), MyIssueInfoActivity.class);
                break;
            case R.id.main_mine_tools_41://PK战绩
                IntentUtils.startActivity(getActivity(), PKRankingActivity.class);
                break;
            case R.id.main_mine_tools_42://考试记录
                IntentUtils.startActivity(getActivity(), ExamRoomHistoryActivity.class);
                break;
            case R.id.main_mine_friend_01://我的好友
                break;
            case R.id.main_mine_friend_02://我的活动
                IntentUtils.startActivity(getActivity(), MyMemberActiActivity.class);
                break;
            case R.id.main_mine_friend_03:
                break;
        }
    }
}
