package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAdapter;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 学创论坛 栏目
 */
public class StudyForumActivity extends BaseActivity {


    @BindView(R.id.study_answers_recyclerview)
    RecyclerView studyAnswersRecyclerview;
    @BindView(R.id.study_share_recyclerview)
    RecyclerView studyShareRecyclerview;
    @BindView(R.id.study_answers_tv)
    TextView studyAnswersTv;
    @BindView(R.id.study_share_tv)
    TextView studyShareTv;

    private StudyAnswerAdapter mAnswerAdapter;
    private StudyShareAdapter mShareAdapter;

    private int Select_MODE_TYPE = 0; // 1是经验分享 0是你问我答

    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_forum;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBothImage();
        head_action_both_title.setText("学创论坛");
        head_action_both_rightiv.setImageResource(R.mipmap.ic_collect_white);

        mAnswerAdapter = new StudyAnswerAdapter(this, DateCacheUtil.getStudyAnswerDetail());
        studyAnswersRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        studyAnswersRecyclerview.setAdapter(mAnswerAdapter);

        mAnswerAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivityForString(StudyForumActivity.this,StudyAnswerDetailActivity.class,StudyAnswerDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mAnswerAdapter.getItem(position)));
            }
        });

        mShareAdapter = new StudyShareAdapter(this, DateCacheUtil.getMyStudyShareList(this));
        studyShareRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        studyShareRecyclerview.setAdapter(mShareAdapter);

        mShareAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivityForString(StudyForumActivity.this, StudyShareDetailActivity.class,StudyShareDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mShareAdapter.getItem(position)));
            }
        });
    }

    private void changSelectBarStyle(){

        if (Select_MODE_TYPE == 0){
            studyAnswersTv.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
            studyShareTv.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
            studyAnswersRecyclerview.setAdapter(mAnswerAdapter);
        }else {
            studyAnswersTv.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
            studyShareTv.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
            studyAnswersRecyclerview.setAdapter(mShareAdapter);
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.head_action_both_rightimage, R.id.study_answer_more_tv, R.id.study_share_more_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.head_action_both_rightimage:
                IntentUtils.startActivity(StudyForumActivity.this, MyStudyCollectAndIssuActivity.class);
                break;
            case R.id.study_answer_more_tv:
                if (Select_MODE_TYPE == 0){
                    IntentUtils.startActivity(StudyForumActivity.this, StudyAnswerActivity.class);
                }else {
                    IntentUtils.startActivity(StudyForumActivity.this, StudyShareActivity.class);
                }

//                IntentUtils.startActivity(StudyForumActivity.this, StudyAnswerActivity.class);
                break;
            case R.id.study_share_more_tv:
                IntentUtils.startActivity(StudyForumActivity.this, StudyShareActivity.class);
                break;
        }
    }

    @OnClick({R.id.study_answers_tv, R.id.study_share_tv})
    public void onViewSelectClicked(View view) {
        switch (view.getId()) {
            case R.id.study_answers_tv://选择你问我答
                Select_MODE_TYPE = 0;
                changSelectBarStyle();
                break;
            case R.id.study_share_tv://选择经验分享
                Select_MODE_TYPE = 1;
                changSelectBarStyle();
                break;
        }
    }
}
