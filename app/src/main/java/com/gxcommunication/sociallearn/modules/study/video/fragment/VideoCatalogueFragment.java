package com.gxcommunication.sociallearn.modules.study.video.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoPlayActivity;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioCatalogueAdapter;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioFirstCatalogueAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
/**
 * 视频详情-视频目录
 */
public class VideoCatalogueFragment extends BaseFragment {



    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    VedioFirstCatalogueAdapter mAdapter;

    private View mView;

    private static final String ARG_VIDEOID = "params_video_id";
    public static VideoCatalogueFragment getInstance(String videoId) {
        VideoCatalogueFragment fragment = new VideoCatalogueFragment();
        Bundle args = new Bundle();
        args.putString(ARG_VIDEOID, videoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_video_catalogue, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }

    @Override
    protected void initView() {
        mAdapter = new VedioFirstCatalogueAdapter(getActivity(), DateCacheUtil.getRequestList(6));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
    }

    @Override
    protected void lazyLoad() {

    }
}
