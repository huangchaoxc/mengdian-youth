package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyShareInfoBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 学创论坛 你问我答 经验分享
 */

public class StudyShareAdapter extends BaseQuickAdapter<StudyShareInfoBean, BaseViewHolder> {

	public StudyShareAdapter(Context context, List<StudyShareInfoBean> data) {
		super(R.layout.item_study_share_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, StudyShareInfoBean item) {
		helper.setText(R.id.item_study_share_category_tv,item.category  );//类型
		helper.setText(R.id.item_study_share_title_tv,item.title  );//标题
		helper.setText(R.id.item_study_share_username_tv,item.userName  );//用户
		helper.setText(R.id.item_study_share_time_tv,item.createTime );//时间
	}
}
