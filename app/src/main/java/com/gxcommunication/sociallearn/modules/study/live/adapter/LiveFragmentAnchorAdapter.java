package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class LiveFragmentAnchorAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public LiveFragmentAnchorAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_live_anchor_course, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.item_live_course_title_tv,"梁宁 增长思维30讲");

		if (helper.getLayoutPosition() == 1){
            helper.setText(R.id.item_live_course_title_tv,"梁宁 产品思维10讲");
        }

    }


}
