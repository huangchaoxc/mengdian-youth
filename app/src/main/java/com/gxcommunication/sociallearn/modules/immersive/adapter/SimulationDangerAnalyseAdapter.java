
package com.gxcommunication.sociallearn.modules.immersive.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName ScenarioSimulationAdapter.java
 * @Description 场景模拟实训
 * @createTime 2021年05月08日 21:49
 */
public class SimulationDangerAnalyseAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public SimulationDangerAnalyseAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_simulation_dangeranalyse_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

    }
}
