package com.gxcommunication.sociallearn.modules.member_activity.activity;

import android.Manifest;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.config.ConfigUtils;
import com.gxcommunication.sociallearn.modules.main.MainActivity;
import com.gxcommunication.sociallearn.modules.splash.FlashActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 线下活动详情
 */
public class MemberActDetailsActivity extends BaseActivity {

    /**
     * 活动标题
     */
    @BindView(R.id.act_title)
    TextView actTitle;
    /**
     * 参加人数
     */
    @BindView(R.id.actNumText)
    TextView actNumText;//参加人数
    /**
     * 发布时间
     */
    @BindView(R.id.actCreateTimeText)
    TextView actCreateTimeText;
    /**
     * 活动时间
     */
    @BindView(R.id.act_time)
    TextView actTime;
    /**
     * 活动地点
     */
    @BindView(R.id.act_location)
    TextView actLocation;
    /**
     * 活动内容
     */
    @BindView(R.id.act_content)
    TextView actContent;
    /**
     * 报名条件
     */
    @BindView(R.id.act_join_condition)
    TextView actJoinCondition;
    /**
     * 年龄要求
     */
    @BindView(R.id.act_age_condition)
    TextView actAgeCondition;
    /**
     * 报名要求描述
     */
    @BindView(R.id.act_join_remark)
    TextView actJoinRemark;

    LocationClient locationClient;
    LatLng memberActiLatlng  = new LatLng(108.386523,22.815198);//服务端获取到的活动的坐标 默认南宁国际会展中心
    @Override
    protected int getLayoutId() {
        return R.layout.activity_member_act_details;
    }

    @Override
    protected void init() {

        super.initHeadActionBar();
        setHeadActionTitle(" 线下活动详情");


    }
    @OnClick({R.id.btn_join,R.id.btn_skip})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_join://参加活动
                break;
            case R.id.btn_skip://返回上级（跳过）
            default:
        }
    }

}

