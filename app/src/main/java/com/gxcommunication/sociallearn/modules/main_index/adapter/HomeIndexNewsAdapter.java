package com.gxcommunication.sociallearn.modules.main_index.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.main_index.bean.HomeIndexBannerListBean;
import com.gxcommunication.sociallearn.modules.main_index.bean.HomeIndexNewsListBean;
import com.gxcommunication.sociallearn.utils.ImageManager;

import java.util.List;


/**
 * Created by hxc on 2017/5/8.
 *
 * 首页 学习 新闻资讯适配器
 */

public class HomeIndexNewsAdapter extends BaseQuickAdapter<HomeIndexNewsListBean.ResultBean, BaseViewHolder> {
	private Context mContext;

	public HomeIndexNewsAdapter(Context context, List<HomeIndexNewsListBean.ResultBean> data) {
		super(R.layout.item_home_index_news_fragment, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, HomeIndexNewsListBean.ResultBean item) {


        helper.setText(R.id.item_home_index_news_title_tv,"降低光纤通信系统故障效率" )
        .setText(R.id.item_home_index_news_content_tv,"供电局信息通信处传输小组" )
        .setText(R.id.item_home_index_news_type_tv,"视频课程" )
        .setText(R.id.item_home_index_news_program_tv,"国家地理" );

//		ImageManager.getManager(mContext).loadUrlFitImage(item.productUrl,helper.getView(R.id.item_home_index_news_pic_iv));

	}
}
