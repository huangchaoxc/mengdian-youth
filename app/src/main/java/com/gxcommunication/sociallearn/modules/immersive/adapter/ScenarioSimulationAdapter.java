
package com.gxcommunication.sociallearn.modules.immersive.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.utils.DateUtils;

import java.util.List;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName ScenarioSimulationAdapter.java
 * @Description 场景模拟列表适配器
 * @createTime 2021年05月08日 21:49
 */
public class ScenarioSimulationAdapter  extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public ScenarioSimulationAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_scenario_simulation, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        helper.setText(R.id.complete_time,mContext.getString(R.string.scenario_simulation_complete_teime_format, DateUtils.getDateToDetialTime(System.currentTimeMillis())));
        ImageView ivContent = helper.getView(R.id.iv_content);
        ivContent.setImageResource(R.drawable.test_rect_img);
    }
}
