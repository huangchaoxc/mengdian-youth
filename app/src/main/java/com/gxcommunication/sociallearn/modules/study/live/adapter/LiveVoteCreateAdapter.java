package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class LiveVoteCreateAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public LiveVoteCreateAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_live_vote_create_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.getView(R.id.item_live_vote_delete_iv).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				remove(helper.getAdapterPosition());
			}
		});

		EditText editText = helper.getView(R.id.item_live_vote_title_edit);

		editText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				item.setMsg(s.toString());
			}
		});
	}
}
