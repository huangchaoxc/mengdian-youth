package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyAnswerActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.activity.StudyStoreActivity;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyForumAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.hxc.toolslibrary.example.base.CommonSmartRefreshActivity;

import java.util.ArrayList;
import java.util.List;

/**
 *  
 * @author hxc
 * 自习室历史学习记录 题目列表
 */
public class StudyRoomHistoryTopicActivity extends CommonSmartRefreshActivity {
	StudyTopicListAdapter mAdapter;

//    @Override
//    protected int getLayoutId() {
//        return R.layout.activity_stu;
//    }

    @Override
    protected void init() {
    	super.init();
		initView();
    }


	public void initView() {
        head_action_title.setText("考试篇");

		mAdapter = new StudyTopicListAdapter(this, loadTempDate());
		mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
		mRecyclerView.setAdapter(mAdapter);
		new PagerSnapHelper().attachToRecyclerView(mRecyclerView);
		mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
			@Override
			public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
				RecyclerView.LayoutManager layoutManager = mRecyclerView.getLayoutManager();
				int firs = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
				if (view.getId() == R.id.item_topic_next_iv) {
					mRecyclerView.scrollToPosition(firs + 1);

				}
			}
		});
	}

	private List<StudyTopicMultiItemBean> loadTempDate() {

		List<StudyTopicMultiItemBean> list = new ArrayList<>();
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Radio_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Multiple_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Judge_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
		list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));

		return list;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {

		default:
			break;
		}
	}
	
	


}
