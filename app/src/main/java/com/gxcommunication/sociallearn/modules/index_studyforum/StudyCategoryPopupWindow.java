package com.gxcommunication.sociallearn.modules.index_studyforum;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.CategoryAttentionAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.CategorySettingAdapter;
import com.gxcommunication.sociallearn.utils.ToastUtils;


/**
 * author：luck
 * project：PictureSelector
 * package：widget

 */

public class StudyCategoryPopupWindow extends PopupWindow implements View.OnClickListener {

    private Context mContext;
    private TextView tvEdit;
    private RecyclerView mAttentionRV,mSettingZyRV,mSettingSxRV,mSettingShRV;
    private Animation animationIn, animationOut;
    private boolean isDismiss = false;
    private boolean isModeEdit = false;//是否处于编辑模式，是的话显示+ 号按钮
    private CategorySettingAdapter mSettingAdapter;
    private CategoryAttentionAdapter mAttentionAdapter;
    private NestedScrollView fl_content;


    public StudyCategoryPopupWindow(Context context) {
        super(context);
        mContext = context;
        View inflate = LayoutInflater.from(context).inflate(R.layout.layout_category_popuview, null);
        this.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        this.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        this.setBackgroundDrawable(new ColorDrawable());
        this.setFocusable(true);
        this.setOutsideTouchable(true);

        this.update();
        this.setBackgroundDrawable(new ColorDrawable());
        this.setContentView(inflate);
        animationIn = AnimationUtils.loadAnimation(context, R.anim.activity_push_down_in);
        animationOut = AnimationUtils.loadAnimation(context, R.anim.activity_push_down_out);
        fl_content = inflate.findViewById(R.id.fl_content);

        mAttentionRV = inflate.findViewById(R.id.recyclerView);

        mSettingZyRV = inflate.findViewById(R.id.zy_recyclerView);
        mSettingSxRV = inflate.findViewById(R.id.sxzz_recyclerView);
        mSettingShRV = inflate.findViewById(R.id.shbk_recyclerView);

        tvEdit = inflate.findViewById(R.id.category_pop_edit_tv);
        inflate.findViewById(R.id.iv_close).setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        fl_content.setOnClickListener(this);

        initRecycler();
    }
 

    private void initRecycler() {
        mAttentionAdapter = new CategoryAttentionAdapter(mContext, DateCacheUtil.getRequestList(10));
        mAttentionRV.setLayoutManager(new GridLayoutManager(mContext,4));
        mAttentionRV.setAdapter(mAttentionAdapter);

        mSettingAdapter = new CategorySettingAdapter(mContext, DateCacheUtil.getRequestList(10));
        mSettingAdapter.setShowEdit(isModeEdit);

        mSettingAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                dismiss();
            }
        });

        mSettingZyRV.setLayoutManager(new GridLayoutManager(mContext,4));
        mSettingZyRV.setAdapter(mSettingAdapter);
        mSettingSxRV.setLayoutManager(new GridLayoutManager(mContext,4));
        mSettingSxRV.setAdapter(mSettingAdapter);
        mSettingShRV.setLayoutManager(new GridLayoutManager(mContext,4));
        mSettingShRV.setAdapter(mSettingAdapter);

        mSettingAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (onItemClickListener != null) {

                }

                if (isModeEdit){
                    ToastUtils.showLong(mContext,"添加栏目");
                }else{
                    StudyCategoryPopupWindow.super.dismiss();
                }
            }
        });
    }

    @Override
    public void showAsDropDown(View parent) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                int[] location = new int[2];
                parent.getLocationOnScreen(location);
                int x = location[0];
                int y = location[1] + parent.getHeight();
                this.showAtLocation(parent, Gravity.BOTTOM, x, y);
            } else {
                this.showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            }

            isDismiss = false;
//            ll_content.startAnimation(animationIn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        if (isDismiss) {
            return;
        }
        isDismiss = true;
//        ll_content.startAnimation(animationOut);
        dismiss();
        animationOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isDismiss = false;
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN) {
                    dismiss4Pop();
                } else {
                    StudyCategoryPopupWindow.super.dismiss();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    /**
     * 在android4.1.1和4.1.2版本关闭PopWindow
     */
    private void dismiss4Pop() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                StudyCategoryPopupWindow.super.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.category_pop_edit_tv) {
            if (onItemClickListener != null) {
               

            }
            isModeEdit = !isModeEdit;
            mSettingAdapter.setShowEdit(isModeEdit);
        } else {
            StudyCategoryPopupWindow.super.dismiss();
        }
     }


    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }


    public interface OnItemClickListener {
        void onAddCarItemClick(int count, String name);

        void onBuyItemClick(int count, RequestResBean bean);
    }
}
