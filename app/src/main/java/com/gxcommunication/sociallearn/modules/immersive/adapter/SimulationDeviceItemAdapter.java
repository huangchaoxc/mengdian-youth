
package com.gxcommunication.sociallearn.modules.immersive.adapter;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.bean.SimulationDeviceItemBean;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoPlayActivity;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioCatalogueAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.List;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName ScenarioSimulationAdapter.java
 * @Description 场景模拟实训 设备检查详情
 * @createTime 2021年05月08日 21:49
 */
public class SimulationDeviceItemAdapter extends BaseQuickAdapter<SimulationDeviceItemBean, BaseViewHolder> {

    public SimulationDeviceItemAdapter(Context context, List<SimulationDeviceItemBean> data) {
        super(R.layout.item_simulation_device_item_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, SimulationDeviceItemBean item) {
        helper.setText(R.id.item_device_content_tv,item.title);
    }
}
