package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyMyAnswerDetailAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAnswerDetailAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyAnswerDetailBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyShareInfoBean;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.LogHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hxc
 * <p>
 * 学创论坛 子栏目  你问我答  详情
 */
public class StudyShareDetailActivity extends BaseActivity {

    public static final String INTENT_KEY_DETAIL_INFO = "detail_Info";


    @BindView(R.id.study_share_category_tv)
    TextView studyShareCategoryTv; //栏目名称

    @BindView(R.id.study_share_title_tv)
    TextView studyShareTitleTv; // 问答标题

    @BindView(R.id.study_share_content_tv)
    TextView studyShareContentTv; // 问答标题

    @BindView(R.id.study_share_username_tv)
    TextView studyShareUsernameTv; // 用户名
    @BindView(R.id.study_share_time_tv)
    TextView studyShareTimeTv; // 回答时间
    @BindView(R.id.study_share_read_tv)
    TextView studyShareReadTv; // 阅读数
    @BindView(R.id.study_share_star_tv)
    TextView studyShareStarTv; // 点赞数
    @BindView(R.id.study_share_commit_tv)
    TextView studyShareCommitTv;// 回复数量
    @BindView(R.id.study_share_collect_tv)
    TextView studyShareCollectTv;// 收藏数量

    StudyShareInfoBean mStudyShareInfoBean;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_share_detail;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("经验分享详情");

        if (!isEmptyStringIntent(INTENT_KEY_DETAIL_INFO)) {
            mStudyShareInfoBean = GsonUtlils.jsonToBean(getIntentStringValue(INTENT_KEY_DETAIL_INFO), StudyShareInfoBean.class);

            loadDetailInfo();
        }
    }

    private void loadDetailInfo() {
        studyShareCategoryTv.setText(mStudyShareInfoBean.category);
        studyShareTitleTv.setText(mStudyShareInfoBean.title);
        studyShareContentTv.setText(mStudyShareInfoBean.contentStr);
        studyShareTimeTv.setText(mStudyShareInfoBean.createTime);
        studyShareUsernameTv.setText(mStudyShareInfoBean.userName);

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

}
