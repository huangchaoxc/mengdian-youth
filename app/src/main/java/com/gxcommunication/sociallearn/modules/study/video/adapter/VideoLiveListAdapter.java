package com.gxcommunication.sociallearn.modules.study.video.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName VideoMainListAdapter.java
 * @Description 主页面视频列表适配器
 * @createTime 2021年05月05日 18:45
 */
public class VideoLiveListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public VideoLiveListAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_vedio_live, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        ViewHolder viewHolder=   new ViewHolder(helper.getConvertView());
        viewHolder.txTitle.setText("如何正确佩戴安全帽"+helper.getAdapterPosition());
    }

    static
    class ViewHolder {
        @BindView(R.id.img_status)
        ImageView imgStatus;
        @BindView(R.id.tx_title)
        TextView txTitle;
        @BindView(R.id.tx_start_time)
        TextView txStartTime;
        @BindView(R.id.tx_end_time)
        TextView txEndTime;
        @BindView(R.id.tx_person_num)
        TextView txPersonNum;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
