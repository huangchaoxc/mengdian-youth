package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.member_activity.bean.MemberActiListBean;
import com.gxcommunication.sociallearn.utils.ImageManager;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *  学创论坛 你问我答 二级分类列表
 */

public class StudySecondCategoryAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {
	private  int selectItem  = -1;

	public StudySecondCategoryAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_study_share_second_category, data);
		mContext = context;
	}

	public void setSelectItem(int selectItem) {
		this.selectItem = selectItem;
		notifyDataSetChanged();
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {


		switch (helper.getAdapterPosition()) {
			case 0:
				helper.setText(R.id.tv_item_all_fenlei, "全部");

				break;
			case 1:
				helper.setText(R.id.tv_item_all_fenlei, "化学");

				break;

			case 2:
				helper.setText(R.id.tv_item_all_fenlei, "线路运行与检修");

				break;

			case 3:
				helper.setText(R.id.tv_item_all_fenlei, "营业用电");

				break;
			default:
				helper.setText(R.id.tv_item_all_fenlei, "其他");
				break;
		}

		if (helper.getAdapterPosition() == selectItem){
			helper.setBackgroundColor(R.id.tv_item_all_fenlei,mContext.getResources().getColor(R.color.color_head));
			helper.setTextColor(R.id.tv_item_all_fenlei,mContext.getResources().getColor(R.color.white));
		}else {
			helper.setBackgroundColor(R.id.tv_item_all_fenlei,mContext.getResources().getColor(R.color.color_transparent_head));
			helper.setTextColor(R.id.tv_item_all_fenlei,mContext.getResources().getColor(R.color.grey_150));
		}
	}
}
