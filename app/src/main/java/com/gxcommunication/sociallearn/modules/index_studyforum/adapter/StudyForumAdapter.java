package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.main_index.bean.HomeIndexBannerListBean;
import com.gxcommunication.sociallearn.utils.ImageManager;

import java.util.List;


/**
 * Created by hxc on 2017/5/8.
 * <p>
 * 首页 学习论坛主功能区
 */

public class StudyForumAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {
    private Context mContext;

    public StudyForumAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_study_forum_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

        switch (helper.getAdapterPosition()) {
            case 0:
                helper.setText(R.id.tv_item_all_fenlei, "学任务");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_01, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 1:
                helper.setText(R.id.tv_item_all_fenlei, "知识库");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_02, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 2:
                helper.setText(R.id.tv_item_all_fenlei, "学习小组");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_03, helper.getView(R.id.iv_item_all_fenlei));
                break;

            case 3:
                helper.setText(R.id.tv_item_all_fenlei, "你问我答");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_04, helper.getView(R.id.iv_item_all_fenlei));

                break;
            case 4:
                helper.setText(R.id.tv_item_all_fenlei, "经验分享");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_05, helper.getView(R.id.iv_item_all_fenlei));
                break;
            case 5:
                helper.setText(R.id.tv_item_all_fenlei, "排行榜");
                ImageManager.getManager(mContext).loadResImage(R.mipmap.main_index_banner_06, helper.getView(R.id.iv_item_all_fenlei));
                break;

        }


    }
}
