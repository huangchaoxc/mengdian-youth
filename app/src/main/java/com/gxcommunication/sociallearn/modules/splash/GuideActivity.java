package com.gxcommunication.sociallearn.modules.splash;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.manage.AppManager;
import com.gxcommunication.sociallearn.utils.CommonSharePreferenceUtils;
import com.gxcommunication.sociallearn.utils.OneKeyExit;
import com.gxcommunication.sociallearn.utils.ToastUtils;
import com.gxcommunication.sociallearn.widget.CircleIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hxc on 2017/9/20.
 */

public class GuideActivity extends BaseActivity {

    public static final String INTENT_KEY_IMAGE_PATH = "image_path";

    ViewPager viewpager;
    CircleIndicator indicator;
    private List<String> mList = new ArrayList<>();
    private int mImageViewArray[] = {};
    private OneKeyExit exit = new OneKeyExit();
    @Override
    protected int getLayoutId() {
        return R.layout.activity_guide;
    }

    @Override
    protected void init() {
        setData();

    }

    private void setData() {
//        if (getIntent().getStringArrayListExtra(INTENT_KEY_IMAGE_PATH) == null){
//            IntentUtils.startActivity(GuideActivity.this, MainFragmentTabActivity.class);
//            finish();
//            return;
//        }
//        mList = getIntent().getStringArrayListExtra(INTENT_KEY_IMAGE_PATH);

        viewpager = findViewById(R.id.viewpager);
        indicator = findViewById(R.id.indicator);
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter();
        viewpager.setAdapter(myPagerAdapter);
        indicator.setViewPager(viewpager);

        CommonSharePreferenceUtils.setFirstOpenAppToFalse();
    }


    class MyPagerAdapter extends PagerAdapter {

        List<ImageView> listView = new ArrayList<>();

        @Override
        public int getCount() {
            return mImageViewArray.length;
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            ImageView imageView = (ImageView) LayoutInflater.from(getApplication()).inflate(R.layout.layout_imageview_match, null);
//            String url = mList.get(position);
//            ImageManager.getManager(GuideActivity.this).loadUrlImageNoDefault(url, imageView);

            int imgId = mImageViewArray[position];
            imageView.setImageResource(imgId);
            listView.add(imageView);
            container.addView(imageView);
            if (position == mImageViewArray.length -1){
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        IntentUtils.startActivity(GuideActivity.this, MainFragmentTabActivity.class);
                        finish();
                    }
                });
            }
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(listView.get(position));
        }

    }

    /**
     * 监听返回--是否退出程序
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean flag = true;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 是否退出应用
            pressAgainExit();
        } else {
            flag = super.onKeyDown(keyCode, event);
        }
        return flag;
    }

    private void pressAgainExit() {

        if (exit.isExit()) {
            AppManager.getAppManager().AppExit(this.getApplicationContext());
        } else {
            ToastUtils.showShort(this.getApplicationContext(), "再按一次返回键退出");
            exit.doExitInOneSecond();
        }
    }

}
