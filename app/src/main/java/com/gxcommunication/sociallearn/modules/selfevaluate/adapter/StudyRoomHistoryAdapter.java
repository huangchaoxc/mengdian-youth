package com.gxcommunication.sociallearn.modules.selfevaluate.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class StudyRoomHistoryAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public StudyRoomHistoryAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_study_room_history, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.item_title_tv,"学习时间 2020-1-1 10：10").
		setText(R.id.item_content_tv,"本次学习题目：10题");

//		ImageManager.getManager(mContext).loadUrlFitImage("",helper.getView(R.id.item_home_message_pic_iv));
    }


}
