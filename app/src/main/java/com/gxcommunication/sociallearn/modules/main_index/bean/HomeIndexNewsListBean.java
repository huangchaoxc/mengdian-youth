package com.gxcommunication.sociallearn.modules.main_index.bean;/**
 * Created by hxc on 2016/8/15.
 */

import com.gxcommunication.sociallearn.base.bean.BaseBean;

/**
 * 请求返回通用实体类
 * Created by hxc on 2016/8/15.
 */
public class HomeIndexNewsListBean extends BaseBean {
    /**
     * code : 400
     * msg : 删除成功
     * result : {}
     */

    private int code;
    private String msg ="";
    private String message = "";

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private ResultBean result;


    public static class ResultBean {

        public String productName;
        public String productUrl;


    }

}
