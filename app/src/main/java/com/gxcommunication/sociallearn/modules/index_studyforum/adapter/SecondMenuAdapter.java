package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on
 * 首页——分类 --右侧分类
 */

public class SecondMenuAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public SecondMenuAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_study_store_secondmenu, data);
        mContext = context;

    }


    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        helper.setText(R.id.item_study_store_title_tv, "内蒙古电力管理办法" + helper.getLayoutPosition());
        helper.setText(R.id.item_study_store_time_tv, " 时间 2020-1-1 10：00   " + helper.getLayoutPosition());

        switch (helper.getLayoutPosition()) {
            case 1:
                helper.setImageResource(R.id.item_study_store_pic_iv, R.mipmap.ic_word_white);
                break;
			case 2:
				helper.setImageResource(R.id.item_study_store_pic_iv, R.mipmap.ic_music_white);
				break;
        }

    }


}
