package com.gxcommunication.sociallearn.modules.study.video.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.base.CommonViewPagerAdapter;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VedioListFragment;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VideoCatalogueFragment;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VideoCommentFragment;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VideoIntroduceFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * hgc
 * （学习课堂）视频详情页
 */
public class VideoDetailsActivity extends BaseActivity {

    private final static String[] TABS=new String[]{"简介","目录","评论"};


    @BindView(R.id.no_text)
    TextView noText;
    @BindView(R.id.sendtime_text)
    TextView sendtimeText;
    @BindView(R.id.show_num)
    TextView showNum;
    @BindView(R.id.show_top)
    TextView showTop;
    @BindView(R.id.show_collection)
    TextView showCollection;
    @BindView(R.id.show_comment)
    TextView showComment;
    @BindView(R.id.btn_share)
    ImageView btnShare;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_details;
    }

    @Override
    protected void init() {
        initTab();
    }

    private void initTab() {
        super.initHeadActionBar();
        setHeadActionTitle("视频详情");
        List<Fragment> fragments=new ArrayList<>();
        fragments.add(VideoIntroduceFragment.getInstance(""));
        fragments.add(VideoCatalogueFragment.getInstance(""));
        fragments.add(VideoCommentFragment.getInstance(""));


        for(int i=0;i<TABS.length;i++){
            tabLayout.addTab(tabLayout.newTab());
        }
        viewPager.setAdapter(new CommonViewPagerAdapter(getSupportFragmentManager(),TABS,fragments));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);

    }


    @OnClick({R.id.show_num, R.id.show_top, R.id.show_collection, R.id.show_comment, R.id.btn_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.show_num:
                break;
            case R.id.show_top:
                break;
            case R.id.show_collection:
                break;
            case R.id.show_comment:
                break;
            case R.id.btn_share:
                break;
        }
    }
}