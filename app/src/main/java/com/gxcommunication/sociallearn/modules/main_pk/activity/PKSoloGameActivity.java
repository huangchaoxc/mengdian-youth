package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * PK 个人比赛 题目列表
 */
public class PKSoloGameActivity extends BaseActivity {


    @BindView(R.id.pk_room_plate_tv)
    TextView pkRoomPlateTv;
    @BindView(R.id.pk_room_category_tv)
    TextView pkRoomCategoryTv;
    @BindView(R.id.pk_room_level_tv)
    TextView pkRoomLevelTv;
    @BindView(R.id.pk_room_radio_tv)
    TextView pkRoomRadioTv;
    @BindView(R.id.pk_room_multple_tv)
    TextView pkRoomMultpleTv;
    @BindView(R.id.pk_room_judge_tv)
    TextView pkRoomJudgeTv;
    @BindView(R.id.pk_room_compute_tv)
    TextView pkRoomComputeTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    StudyTopicListAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pk_solo_game;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("个人挑战赛");
        mAdapter = new StudyTopicListAdapter(this, loadTempDate());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(mAdapter);
        new PagerSnapHelper().attachToRecyclerView(recyclerView);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                int firs = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (view.getId() == R.id.item_topic_next_iv) {
                    recyclerView.scrollToPosition(firs + 1);

                }
            }
        });
    }

    private List<StudyTopicMultiItemBean> loadTempDate() {

        List<StudyTopicMultiItemBean> list = new ArrayList<>();
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Radio_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Multiple_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Judge_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));

        return list;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {

        showDialog();
    }

    private void showDialog() {

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_pk_solo_result_layout, null);
        AlertDialog builder = new AlertDialog.Builder(this, R.style.NobackDialog).create();
        builder.setView(view);
        builder.setCancelable(true);
        view.findViewById(R.id.exam_remind_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
                finish();
            }
        });
        builder.show();
    }
}
