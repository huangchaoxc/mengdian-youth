package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteItemBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class LiveVoteListAdapter extends BaseQuickAdapter<LiveVoteItemBean, BaseViewHolder> {

	public LiveVoteListAdapter(Context context, List<LiveVoteItemBean> data) {
		super(R.layout.item_live_vote_list_activity, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, LiveVoteItemBean item) {
		helper.setText(R.id.item_live_vote_title_tv,item.title);
		if (item.statusCode == 0){
			helper.setText(R.id.item_live_vote_content_tv,"未完成");
		}else {
			helper.setText(R.id.item_live_vote_content_tv,"已完成");
		}

	}
}
