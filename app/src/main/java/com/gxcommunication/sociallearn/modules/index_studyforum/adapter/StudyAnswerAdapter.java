package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.modules.index_studyforum.bean.StudyAnswerDetailBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 学创论坛 你问我答 经验分享
 */

public class StudyAnswerAdapter extends BaseQuickAdapter<StudyAnswerDetailBean, BaseViewHolder> {

	public StudyAnswerAdapter(Context context, List<StudyAnswerDetailBean> data) {
		super(R.layout.item_study_answer, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, StudyAnswerDetailBean item) {
		helper.setText(R.id.item_study_share_category_tv,item.category)
				.setText(R.id.item_study_share_award_tv,"悬赏积分：" + item.awardScore + "分")
				.setText(R.id.item_study_share_title_tv,item.title)
				.setText(R.id.item_study_share_username_tv,item.userName)
				.setText(R.id.item_study_share_time_tv,item.createTime);
		if (item.isAccept == 0){ //模拟显示采纳，显示采纳按钮
			helper.setText(R.id.item_study_share_status_tv,"已完结");
		}else {
			helper.setText(R.id.item_study_share_status_tv,"未完结");
		}
	}
}
