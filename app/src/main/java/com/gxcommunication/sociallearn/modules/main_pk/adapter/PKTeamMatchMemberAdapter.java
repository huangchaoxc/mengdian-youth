package com.gxcommunication.sociallearn.modules.main_pk.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class PKTeamMatchMemberAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public PKTeamMatchMemberAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_pk_team_member_layout, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.pk_team_name_tv,"张三");//名字

//		ImageManager.getManager(mContext).loadUrlFitImage("",helper.getView(R.id.item_home_message_pic_iv));
    }


}
