package com.gxcommunication.sociallearn.modules.study.video.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.base.CommonViewPagerAdapter;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VedioListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoTabListActivity extends BaseActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    private final static String[] TABS=new String[]{"专业技能","思想政治","生活情感"};

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_tab_list;
    }

    @Override
    protected void init() {
        initHeadActionBar();
        setHeadActionTitle("学习课堂");
        initTab();

    }

    private void initTab() {

        List<Fragment> fragments=new ArrayList<>();
        for(int i=0;i<TABS.length;i++){
            fragments.add(VedioListFragment.getInstance(TABS[i]));
            tabLayout.addTab(tabLayout.newTab());
        }
        viewPager.setAdapter(new CommonViewPagerAdapter(getSupportFragmentManager(),TABS,fragments));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setupWithViewPager(viewPager);
    }


}