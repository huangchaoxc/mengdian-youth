package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class LiveVoteHostDetailAdapter extends BaseQuickAdapter<LiveVoteCreateAndDetailBean.DataBean, BaseViewHolder> {


	public LiveVoteHostDetailAdapter(Context context, List<LiveVoteCreateAndDetailBean.DataBean> data ) {
		super(R.layout.item_live_vote_host_detail_activity, data);
		mContext = context;

	}

	@Override
	protected void convert(BaseViewHolder helper, LiveVoteCreateAndDetailBean.DataBean item) {


		helper.setText(R.id.item_live_vote_title_tv,item.voteContent);
	}
}
