package com.gxcommunication.sociallearn.modules.main_my.activity;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.SecondMenuAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAdapter;
import com.gxcommunication.sociallearn.modules.main_my.adapter.MyCollectTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioListerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * @author hxc
 * 我的收藏
 */
public class MyCollectActivity extends BaseActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

	private VedioListerAdapter vedioListerAdapter = new VedioListerAdapter(MyCollectActivity.this, DateCacheUtil.getStringList(20));// 学习课堂 --视频栏目
	private MyCollectTopicListAdapter mStudyAdapter = new MyCollectTopicListAdapter(this, loadTempDate());//自我评定，题目列表
	private SecondMenuAdapter mStoreAdapter = new SecondMenuAdapter(this, DateCacheUtil.getRequestList(9));//知识库
	private StudyAnswerAdapter mAnswerAdapter = new StudyAnswerAdapter(this, DateCacheUtil.getStudyAnswerDetail());//你问我答
	private StudyShareAdapter mNShareAdapter  = new StudyShareAdapter(this,DateCacheUtil.getMyStudyShareList(this));//经验分享

    private List<StudyTopicMultiItemBean> loadTempDate() {

        List<StudyTopicMultiItemBean> list = new ArrayList<>();
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Radio_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Multiple_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Judge_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));

        return list;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_collect;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("我的收藏");
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(vedioListerAdapter);
        initTabLayout();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }

    private void initTabLayout() {
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.addTab(tabLayout.newTab().setText("学习课堂"));
        tabLayout.addTab(tabLayout.newTab().setText("身临其境"));
        tabLayout.addTab(tabLayout.newTab().setText("自我评定"));
        tabLayout.addTab(tabLayout.newTab().setText("知识库"));
        tabLayout.addTab(tabLayout.newTab().setText("你问我答"));
        tabLayout.addTab(tabLayout.newTab().setText("经验分享"));


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0://学习课堂
                        vedioListerAdapter = new VedioListerAdapter(MyCollectActivity.this, DateCacheUtil.getStringList(20));
                        recyclerview.setAdapter(vedioListerAdapter);
                        break;
                    case 1://身临其境

                        break;
                    case 2://自我评定
                        recyclerview.setAdapter(mStudyAdapter);
                        break;
                    case 3://知识库
                        recyclerview.setAdapter(mStoreAdapter);
                        break;
                    case 4://学创论坛 你问我答
						recyclerview.setAdapter(mAnswerAdapter);
                        break;

					case 5://学创论坛 经验分享
						recyclerview.setAdapter(mNShareAdapter);
						break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


}
