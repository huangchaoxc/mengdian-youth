package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class LiveCourseAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public LiveCourseAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_live_course_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        if (helper.getLayoutPosition() == 0){
            helper.setText(R.id.item_live_title_tv,"中国共产党建党100周年");
            helper.setImageResource(R.id.item_live_pic_iv,R.mipmap.live_course_item_01);
        }else {
            helper.setText(R.id.item_live_title_tv,"跟着机器人一起巡视变电站");
            helper.setImageResource(R.id.item_live_pic_iv,R.mipmap.live_coures_item_02);
        }


    }


}
