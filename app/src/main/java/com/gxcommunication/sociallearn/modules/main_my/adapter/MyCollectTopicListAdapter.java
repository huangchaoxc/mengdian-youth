package com.gxcommunication.sociallearn.modules.main_my.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 * 自我评定 问题列表
 */

public class MyCollectTopicListAdapter extends BaseMultiItemQuickAdapter<StudyTopicMultiItemBean, BaseViewHolder> {

    public MyCollectTopicListAdapter(Context context, List<StudyTopicMultiItemBean> data) {
        super(data);
        mContext = context;

        addItemType(StudyTopicMultiItemBean.Topic_Radio_Type, R.layout.item_study_topic_radio_activity);
        addItemType(StudyTopicMultiItemBean.Topic_Multiple_Type, R.layout.item_study_topic_multiple_activity);
        addItemType(StudyTopicMultiItemBean.Topic_Judge_Type, R.layout.item_study_topic_judge_activity);
        addItemType(StudyTopicMultiItemBean.Topic_Compute_Type, R.layout.item_study_topic_radio_activity);
    }

    @Override
    protected void convert(BaseViewHolder helper, StudyTopicMultiItemBean item) {
        helper.setText(R.id.item_learn_pos_tv, "第" + helper.getLayoutPosition() + 1 + "题");//

        switch (helper.getItemViewType()) {
            case StudyTopicMultiItemBean.Topic_Radio_Type://单选题目

                break;
            case StudyTopicMultiItemBean.Topic_Multiple_Type://多选题目

                break;
            case StudyTopicMultiItemBean.Topic_Judge_Type://判断题

                break;
            case StudyTopicMultiItemBean.Topic_Compute_Type://计算题
                helper.setText(R.id.item_learn_type_tv, "计算题");

                break;
        }

        helper.setGone(R.id.item_topic_next_iv,false); // 在我的收藏题目中无需显示收藏，下一题等操作

        helper.addOnClickListener(R.id.item_topic_next_iv);

    }
}
