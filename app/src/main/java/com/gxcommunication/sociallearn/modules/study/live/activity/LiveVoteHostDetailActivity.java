package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveVoteHostDetailAdapter;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/9/20.
 * 主持人投票详情
 */

public class LiveVoteHostDetailActivity extends BaseActivity {

    public static final String INTENT_KEY_CONTENT = "vote_content";//

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;


    @BindView(R.id.live_vote_detail_title_tv)
    TextView liveVoteDetailTitleTv;
    @BindView(R.id.live_vote_detail_type_tv)
    TextView liveVoteDetailTypeTv;
    @BindView(R.id.live_vote_detail_status_tv)
    TextView liveVoteDetailStatusTv;
    @BindView(R.id.live_vote_detail_remark_tv)
    TextView liveVoteDetailRemarkTv;

    LiveVoteCreateAndDetailBean mLiveVoteCreateBean = new LiveVoteCreateAndDetailBean();
    LiveVoteHostDetailAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_vote_host_detail;
    }

    @Override
    protected void init() {
        super.initHeadActionBar();
        setHeadActionTitle("投票详情");

        if (!isEmptyStringIntent(INTENT_KEY_CONTENT)){
            mLiveVoteCreateBean = GsonUtlils.jsonToBean(getIntentStringValue(INTENT_KEY_CONTENT), LiveVoteCreateAndDetailBean.class);
            loadVoteInfo();
        }
    }

    private void loadVoteInfo(){
        liveVoteDetailTitleTv.setText(mLiveVoteCreateBean.title);
        liveVoteDetailRemarkTv.setText(mLiveVoteCreateBean.content);

        if (mLiveVoteCreateBean.voteModeCode == 0){
            liveVoteDetailTypeTv.setText("单选题");
        }else {
            liveVoteDetailTypeTv.setText("多选题");
        }


        mAdapter = new LiveVoteHostDetailAdapter(this, mLiveVoteCreateBean.data);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(mAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.action_start_btn, R.id.action_edit_btn, R.id.action_delete_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.action_start_btn:
                 IntentUtils.startActivityForString(LiveVoteHostDetailActivity.this, LiveVoteProceedDetailActivity.class,
                        LiveVoteHostDetailActivity.INTENT_KEY_CONTENT, GsonUtlils.objectToJson(mLiveVoteCreateBean));
				finish();

				break;
            case R.id.action_edit_btn:
				IntentUtils.startActivity(LiveVoteHostDetailActivity.this,LiveVoteCreateActivity.class);
				finish();
                break;
            case R.id.action_delete_btn:

            	showDiglog("是否需要删除此项投票", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
                break;
        }
    }
}
