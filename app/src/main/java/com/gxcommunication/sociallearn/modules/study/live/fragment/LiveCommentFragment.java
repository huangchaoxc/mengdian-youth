package com.gxcommunication.sociallearn.modules.study.live.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveFragmentComentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 视频详情-视频评论
 */
public class LiveCommentFragment extends BaseFragment {

    @BindView(R.id.comment_recyclerView)
    RecyclerView commentRecyclerView;
    @BindView(R.id.comment_content_edit)
    EditText commentContentEdit;
    @BindView(R.id.comment_send_btn)
    Button commentSendBtn;
    private View mView;
    LiveFragmentComentAdapter mAdapter;

    private static final String ARG_VIDEOID = "params_video_id";

    public static LiveCommentFragment getInstance(String videoId) {
        LiveCommentFragment fragment = new LiveCommentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_VIDEOID, videoId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_video_comment, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }


    @Override
    protected void initView() {

        mAdapter = new LiveFragmentComentAdapter(getActivity(), DateCacheUtil.getRequestList(10));
        commentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        commentRecyclerView.setAdapter(mAdapter);

    }

    @Override
    protected void lazyLoad() {

    }
}
