package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * PK  团体赛 赛前准备
 */
public class PKTeamRemindActivity extends BaseActivity {


    @BindView(R.id.pk_room_plate_tv)
    TextView pkRoomPlateTv;
    @BindView(R.id.pk_room_category_tv)
    TextView pkRoomCategoryTv;
    @BindView(R.id.pk_room_level_tv)
    TextView pkRoomLevelTv;
    @BindView(R.id.pk_room_radio_tv)
    TextView pkRoomRadioTv;
    @BindView(R.id.pk_room_multple_tv)
    TextView pkRoomMultpleTv;
    @BindView(R.id.pk_room_judge_tv)
    TextView pkRoomJudgeTv;
    @BindView(R.id.pk_room_compute_tv)
    TextView pkRoomComputeTv;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_pk_team_remind;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("团队挑战赛");

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.pk_remind_btn)
    public void onViewClicked() {
        IntentUtils.startActivity(this, PKSoloGameActivity.class);
    }

    @OnClick(R.id.pk_team_name_btn)
    public void onViewnameClicked() {

        IntentUtils.startActivity(this, PKTeamFormActivity.class);
    }
}
