package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.StudyCategoryPopupWindow;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyGridCategoryAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudySecondCategoryAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.gxcommunication.sociallearn.widget.BetterSpinner;
import com.scwang.smartrefresh.layout.util.DensityUtil;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author hxc
 * 学创论坛 子栏目  你问我答
 */
public class StudyAnswerActivity extends BaseActivity {

    @BindView(R.id.study_categoty_zy)
    TextView studyCategotyZy;
    @BindView(R.id.study_categoty_sh)
    TextView studyCategotySh;
    @BindView(R.id.study_categoty_sx)
    TextView studyCategotySx;

    @BindView(R.id.head_action_both_rightimage)
    ImageView head_action_both_rightimage;
    @BindView(R.id.head_action_both_add_iv)
    ImageView head_action_both_add_iv;

    @BindView(R.id.study_share_answer_user_iv)
    ImageView study_share_answer_user_iv;

    @BindView(R.id.category_pop_tv)
    TextView categoryPoptv;

    @BindView(R.id.study_categoty_recyclerview)
    RecyclerView studyCategotyRecyclerview;
    @BindView(R.id.study_recyclerview)
    RecyclerView studyRecyclerview;

    StudyGridCategoryAdapter mSecondCategoryAdapter;
    StudyAnswerAdapter mAdapter;

    @BindView(R.id.ed_search)
    EditText edSearch;

    @BindView(R.id.sort_hot_iv)
    ImageView sortHotIv;
    @BindView(R.id.sort_new_iv)
    ImageView sortNewIv;

    private StudyCategoryPopupWindow mCategoryPopupWindow;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_study_share_answer;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBothTitle();
        setHeadActionBothTitle("你问我答","发布");
        head_action_both_add_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startActivity(StudyAnswerActivity.this,StudyAnswersSendActivity.class);
            }
        });

        head_action_both_rightimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startActivity(StudyAnswerActivity.this,MyStudyAnswerCollectActivity.class);
            }
        });

        study_share_answer_user_iv.setOnClickListener(new View.OnClickListener() {//点击搜索旁边的用户头像，跳转到我发布的信息列表
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(StudyAnswerActivity.this, MyStudyAnswerCollectActivity.class);
                intent.putExtra(MyStudyAnswerCollectActivity.INTENT_KEY_TYPE, 1);
                startActivity(intent);
            }
        });

        studyCategotyZy.setOnClickListener(this);
        studyCategotySh.setOnClickListener(this);
        studyCategotySx.setOnClickListener(this);

        mSecondCategoryAdapter = new StudyGridCategoryAdapter(this, DateCacheUtil.getRequestList(10));
        studyCategotyRecyclerview.setLayoutManager(new GridLayoutManager(this,5));
        studyCategotyRecyclerview.setAdapter(mSecondCategoryAdapter);

        mAdapter = new StudyAnswerAdapter(this,DateCacheUtil.getStudyAnswerDetail());
        studyRecyclerview .setLayoutManager(new LinearLayoutManager(this));
        studyRecyclerview.setAdapter(mAdapter);

        mSecondCategoryAdapter.setSelectItem(0);
        mSecondCategoryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mSecondCategoryAdapter.setSelectItem(position);
            }
        });

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivityForString(StudyAnswerActivity.this,StudyAnswerDetailActivity.class,StudyAnswerDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mAdapter.getItem(position)));
            }
        });

        categoryPoptv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopupwinds();
            }
        });

        mCategoryPopupWindow = new StudyCategoryPopupWindow(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        selectFirstCategory(v.getId());
    }


    private void selectFirstCategory(int resid){

        if (resid == R.id.study_categoty_zy){//专业
            studyCategotyZy.setBackgroundColor(getResources().getColor(R.color.color_head));
            studyCategotySh.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            studyCategotySx.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            studyCategotyRecyclerview.setVisibility(View.VISIBLE);//只有专业有二级分类
        }else  if (resid == R.id.study_categoty_sh){//生活
            studyCategotyZy.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            studyCategotySh.setBackgroundColor(getResources().getColor(R.color.color_head));
            studyCategotySx.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
//            studyCategotyRecyclerview.setVisibility(View.GONE);
        } else if (resid == R.id.study_categoty_sx){//思想
            studyCategotyZy.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            studyCategotySh.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
            studyCategotySx.setBackgroundColor(getResources().getColor(R.color.color_head));
//            studyCategotyRecyclerview.setVisibility(View.GONE);
        }
    }


    private void showPopupwinds() {
        mCategoryPopupWindow.showAsDropDown(categoryPoptv);

     }


    @OnClick({R.id.sort_hot_iv, R.id.sort_new_iv})
    public void onViewSortClicked(View view) {
        switch (view.getId()) {
            case R.id.sort_hot_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_select);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_normal);
                break;
            case R.id.sort_new_iv:
                sortHotIv.setImageResource(R.mipmap.ic_sort_hot_normal);
                sortNewIv.setImageResource(R.mipmap.ic_sort_new_select);
                break;
        }
    }

}
