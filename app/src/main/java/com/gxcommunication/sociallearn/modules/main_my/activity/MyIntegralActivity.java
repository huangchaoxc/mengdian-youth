package com.gxcommunication.sociallearn.modules.main_my.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.main_my.adapter.MyIntegralAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hxc
 * 我的积分
 */
public class MyIntegralActivity extends BaseActivity {

    @BindView(R.id.integral_score_tv)
    TextView integralScoreTv;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

	MyIntegralAdapter mAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_my_integral;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("我的积分");
		mAdapter = new MyIntegralAdapter(this, DateCacheUtil.getRequestList(10));

		recyclerView.setLayoutManager(new LinearLayoutManager(this));
		recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
