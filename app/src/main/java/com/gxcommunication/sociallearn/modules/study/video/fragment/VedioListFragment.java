package com.gxcommunication.sociallearn.modules.study.video.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseFragment;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoDetailsActivity;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioListerAdapter;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioTabClaasAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VedioListFragment extends BaseFragment {


    public static final String ARG_TABID = "tabid";

    @BindView(R.id.tab_class)
    RecyclerView tabClasss;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private View mView;


    VedioTabClaasAdapter tabClaasAdapter;

    VedioListerAdapter vedioListerAdapter;


    private final static String[] tabs=new String[]{
            "全部","化学","变电运算与维修","营业用电"
    };

    public static VedioListFragment getInstance(String tabid) {
        VedioListFragment fragment = new VedioListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TABID, tabid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_vedio_list;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_main_vedio_list, null);
        }
        ButterKnife.bind(this, mView);
        initView();
        return mView;
    }

    @Override
    protected void initView() {


        tabClasss.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));

        tabClaasAdapter=new VedioTabClaasAdapter(getContext(), Arrays.asList(tabs));
        tabClasss.setAdapter(tabClaasAdapter);

        vedioListerAdapter=new VedioListerAdapter(getContext(), DateCacheUtil.getStringList(20));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(vedioListerAdapter);

        vedioListerAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(getActivity(), VideoDetailsActivity.class);
            }
        });

    }

    @Override
    protected void lazyLoad() {

    }
}
