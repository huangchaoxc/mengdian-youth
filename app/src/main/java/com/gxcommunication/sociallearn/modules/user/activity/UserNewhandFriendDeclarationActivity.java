package com.gxcommunication.sociallearn.modules.user.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * * 用户模块 新手任务  发布交友宣言
 */
public class UserNewhandFriendDeclarationActivity extends BaseActivity {

    @BindView(R.id.user_info_head_iv)
    ImageView userInfoHeadIv; // 选择头像后加载显示的 imageview
    @BindView(R.id.user_newhand_introl_edit)
    EditText userNewhandIntrolEdit; // 交友宣言的说明
    @BindView(R.id.user_newhand_tag_01)
    Button userNewhandTag01; // 添加标签，因为说明是加3个标签，这里就考虑不用列表实现，直接用三个按钮
    @BindView(R.id.user_newhand_tag_02)
    Button userNewhandTag02;
    @BindView(R.id.user_newhand_tag_03)
    Button userNewhandTag03;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_user_newhand_friend_declaration;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        setHeadActionTitle(" 发布交友宣言");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @OnClick({R.id.user_info_head_iv, R.id.action_btn,R.id.action_preview_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.user_info_head_iv:// 选择头像图片，相册或者拍照

                break;
            case R.id.action_btn://提交交友宣言信息
                break;
            case R.id.action_preview_btn://提交交友宣言信息
                IntentUtils.startActivity(this, UserNewhandFriendDeclarationPreviewActivity.class);
                break;
        }
    }
}
