package com.gxcommunication.sociallearn.modules.main_my.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.main_my.adapter.TopRankingAdapter;
import com.gxcommunication.sociallearn.modules.study.video.fragment.VedioListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * top 排行榜
 */
public class TopRankingActivity extends BaseActivity {

    @BindView(R.id.my_top_rank_level)
    TextView myLevel;
    @BindView(R.id.my_top_rank_integral)
    TextView myIntegral;
    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerview;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    TopRankingAdapter mAdapter;
    private final static String[] tabTitleArr = new String[]{"日榜", "周榜", "月榜","总榜"};

    @Override
    protected int getLayoutId() {
        return R.layout.activity_top_ranking;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("Top 排行榜");

        mAdapter = new TopRankingAdapter(this, DateCacheUtil.getRequestList(9));
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerview.setAdapter(mAdapter);

        for (int i = 0; i < tabTitleArr.length; i++) {
            tabLayout.addTab(tabLayout.newTab().setText(tabTitleArr[i]));
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {//切换选项

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.my_top_rank_level, R.id.my_top_rank_integral})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.my_top_rank_level:
                myLevel.setBackgroundResource(R.drawable.round_white_bg);
                myIntegral.setBackgroundResource(R.drawable.bg_exam_topic_btn_round);
                break;
            case R.id.my_top_rank_integral:
                myLevel.setBackgroundResource(R.drawable.bg_exam_topic_btn_round);
                myIntegral.setBackgroundResource(R.drawable.round_white_bg);
                break;
        }
    }
}
