package com.gxcommunication.sociallearn.modules.user.activity;

import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.user.adapter.UserInfoLikeAdapter;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 用户信息更新
 */
public class SettingUserInfoActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {


    @BindView(R.id.iv_userAvatar)
    ImageView ivUserAvatar;
    @BindView(R.id.tv_nickame)
    TextView nickNameTv;
    //姓名
    @BindView(R.id.user_name_tv)
    TextView userNameTv;
    @BindView(R.id.user_name_switch)
    Switch userNameSwitch;
    @BindView(R.id.user_marry_tv)
	//单身
    TextView userMarryTv;
    @BindView(R.id.user_marry_switch)
    Switch userMarrySwitch;
	//工号
    @BindView(R.id.user_num_tv)
    TextView userNumTv;
    @BindView(R.id.user_num_switch)
    Switch userNumSwitch;
	//性别
    @BindView(R.id.user_sex_tv)
    TextView userSexTv;
    @BindView(R.id.user_sex_switch)
    Switch userSexSwitch;
	//出生
    @BindView(R.id.user_birthday_tv)
    TextView userBirthdayTv;
    @BindView(R.id.user_birthday_switch)
    Switch userBirthdaySwitch;
	//教育
    @BindView(R.id.user_education_tv)
    TextView userEducationTv;
    @BindView(R.id.user_education_switch)
    Switch userEducationSwitch;
	//手机
    @BindView(R.id.user_phone_tv)
    TextView userPhoneTv;
    @BindView(R.id.user_phone_switch)
    Switch userPhoneSwitch;
	//身高
    @BindView(R.id.user_height_tv)
    TextView userHeightTv;
    @BindView(R.id.user_height_switch)
    Switch userHeightSwitch;


	//公司
    @BindView(R.id.user_word_company_tv)
    TextView userWordCompanyTv;
    @BindView(R.id.user_word_company_switch)
    Switch userWordCompanySwitch;
	//团委
    @BindView(R.id.user_word_tw_tv)
    TextView userWordTwTv;
    @BindView(R.id.user_word_tw_switch)
    Switch userWordTwSwitch;
	//支团委
    @BindView(R.id.user_word_tzb_tv)
    TextView userWordTzbTv;
    @BindView(R.id.user_word_tzb_switch)
    Switch userWordTzbSwitch;
	//专业
    @BindView(R.id.user_word_profession_tv)
    TextView userWordProfessionTv;
    @BindView(R.id.user_word_profession_switch)
    Switch userWordProfessionSwitch;
	//工种
    @BindView(R.id.user_word_wordtype_tv)
    TextView userWordWordtypeTv;
    @BindView(R.id.user_word_wordtype_switch)
    Switch userWordWordtypeSwitch;
	//岗位
    @BindView(R.id.user_word_position_tv)
    TextView userWordPositionTv;
    @BindView(R.id.user_word_position_switch)
    Switch userWordPositionSwitch;
	//所在地
    @BindView(R.id.user_word_area_tv)
    TextView userWordAreaTv;
    @BindView(R.id.user_word_area_switch)
    Switch userWordAreaSwitch;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btn_send_message)
    Button btnSendMessage;


	UserInfoLikeAdapter mAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting_user_info;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {

		mAdapter = new UserInfoLikeAdapter(this, DateCacheUtil.getRequestList(12));
		recyclerView.setLayoutManager(new GridLayoutManager(this,4));
		recyclerView.setAdapter(mAdapter);
        initListener();
    }

    private void initListener(){

        userNameSwitch.setOnCheckedChangeListener(this);
        userMarrySwitch.setOnCheckedChangeListener(this);
        userNumSwitch.setOnCheckedChangeListener(this);
        userSexSwitch.setOnCheckedChangeListener(this);
        userBirthdaySwitch.setOnCheckedChangeListener(this);
        userEducationSwitch.setOnCheckedChangeListener(this);
        userPhoneSwitch.setOnCheckedChangeListener(this);
        userHeightSwitch.setOnCheckedChangeListener(this);


        userWordCompanySwitch.setOnCheckedChangeListener(this);
        userWordTwSwitch.setOnCheckedChangeListener(this);
        userWordTzbSwitch.setOnCheckedChangeListener(this);
        userWordProfessionSwitch.setOnCheckedChangeListener(this);
        userWordWordtypeSwitch.setOnCheckedChangeListener(this);
        userWordPositionSwitch.setOnCheckedChangeListener(this);
        userWordAreaSwitch.setOnCheckedChangeListener(this);



    }




    @OnClick({R.id.iv_userAvatar, R.id.btn_send_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_userAvatar://查看头像

                break;
            case R.id.btn_send_message://提交修改

                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked){
            buttonView.setText("公开");
        }else {
            buttonView.setText("不公开");
        }
    }
}
