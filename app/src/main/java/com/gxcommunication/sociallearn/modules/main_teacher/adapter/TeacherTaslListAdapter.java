package com.gxcommunication.sociallearn.modules.main_teacher.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class TeacherTaslListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public TeacherTaslListAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_teacher_task_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.item_teacher_task_title_tv,"系统消息").
		setText(R.id.item_teacher_task_content_tv,"系统刚刚发布了一条消息。。。。");

        if (helper.getAdapterPosition() % 2 == 0){
            helper.setText(R.id.teacher_task_action_btn,"已完成");
        }
    }


}
