package com.gxcommunication.sociallearn.modules.study.video.adapter;

import android.content.Context;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * 视频标签卡
 * Created by hxc on 2017/8/7
 */

public class VedioTabClaasAdapter extends BaseQuickAdapter<String, BaseViewHolder> {

    public VedioTabClaasAdapter(Context context, List<String> data) {
        super(R.layout.item_vedio_tabclass, data);
        mContext = context;
    }
    @Override
    protected void convert(BaseViewHolder helper, String item) {
        TextView tab_text=helper.getView(R.id.tab_text);
        tab_text.setText(item);

        //根据选中状态更换背景

        if(helper.getAdapterPosition()==0){
            tab_text.setBackground(mContext.getResources().getDrawable(R.drawable.blue_btn_selector));
        }
    }

}
