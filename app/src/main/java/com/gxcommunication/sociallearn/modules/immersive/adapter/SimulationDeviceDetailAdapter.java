
package com.gxcommunication.sociallearn.modules.immersive.adapter;

import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.immersive.bean.SimulationDeviceItemBean;
import com.gxcommunication.sociallearn.modules.study.video.activity.VideoPlayActivity;
import com.gxcommunication.sociallearn.modules.study.video.adapter.VedioCatalogueAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hgc
 * @version 1.0.0
 * @ClassName ScenarioSimulationAdapter.java
 * @Description 场景模拟实训 设备检查详情
 * @createTime 2021年05月08日 21:49
 */
public class SimulationDeviceDetailAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public SimulationDeviceDetailAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_simulation_device_detail_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

        SimulationDeviceItemAdapter secondAdapter = null;
        List<SimulationDeviceItemBean> list = new ArrayList<>();
        switch (helper.getAdapterPosition()){
            case 0:
                helper.setText(R.id.item_device_title_tv, "设备标识" );

                list.add(new SimulationDeviceItemBean("设备名称、调度编号清晰，无损坏；"));
                list.add(new SimulationDeviceItemBean("相序标注清晰，无脱落、变色"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);
                break;
            case 1:
                helper.setText(R.id.item_device_title_tv, "瓷绝缘部分 " );
                list.add(new SimulationDeviceItemBean("无破损裂纹、放电痕迹，表面清洁。"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);

                break;
            case 2:
                helper.setText(R.id.item_device_title_tv, "母线及引线 " );
                list.add(new SimulationDeviceItemBean("无过紧过松，设备连接处无松动、过热。"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);

                break;
            case 3:
                helper.setText(R.id.item_device_title_tv, "电容器 " );
                list.add(new SimulationDeviceItemBean("涂漆无变色、变形，外壳无鼓肚、膨胀变形，接缝无开裂、渗漏油现象，内部无放电声；"));
                list.add(new SimulationDeviceItemBean("各接头无熔锡、发热现象，无冒烟、异味、变色"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);

                break;
            case 4:
                helper.setText(R.id.item_device_title_tv, "熔断器、放电装置" );
                list.add(new SimulationDeviceItemBean("熔断器、接地装置、放电回路及指示灯完好、安装牢固，"));
                list.add(new SimulationDeviceItemBean("接地引线无严重锈蚀、断股；"));
                list.add(new SimulationDeviceItemBean("外熔丝电容器组单台保护用熔断器外壳及弹簧连接牢固，螺丝无锈蚀；"));
                list.add(new SimulationDeviceItemBean("放电装置无渗油、漏油现象，接线牢固；"));
                list.add(new SimulationDeviceItemBean("零序CT接线牢固，外绝缘清洁，无渗漏油、无裂纹、损伤放电现象。"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);
                break;
            case 5:
                helper.setText(R.id.item_device_title_tv, "电抗器 " );
                list.add(new SimulationDeviceItemBean("电抗器巡视项目及标准。"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);

                break;
            case 6:
                helper.setText(R.id.item_device_title_tv, "电缆" );
                list.add(new SimulationDeviceItemBean("电缆巡视项目"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);

                break;
            case 7:
                helper.setText(R.id.item_device_title_tv, "其它 " );
                list.add(new SimulationDeviceItemBean("基础无倾斜、下沉，架构无锈蚀，接地牢固，标识清晰；"));
                list.add(new SimulationDeviceItemBean("围网无破损、锈蚀，网门锁好。"));
                secondAdapter = new SimulationDeviceItemAdapter(mContext, list);

                break;

        }

        RecyclerView recyclerView = helper.getView(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(secondAdapter);

        secondAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

                IntentUtils.startActivity(mContext, VideoPlayActivity.class);
            }
        });
    }
}
