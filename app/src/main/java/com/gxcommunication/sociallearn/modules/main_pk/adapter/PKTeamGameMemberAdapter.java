package com.gxcommunication.sociallearn.modules.main_pk.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class PKTeamGameMemberAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public PKTeamGameMemberAdapter(Context context, int resid,List<RequestResBean> data) {
        super(resid, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.pk_team_name_tv,20 + helper.getAdapterPosition() + "");//分数

//		ImageManager.getManager(mContext).loadUrlFitImage("",helper.getView(R.id.item_home_message_pic_iv));
    }


}
