package com.gxcommunication.sociallearn.modules.main_my.activity;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.member_activity.activity.MemberActDetailsActivity;
import com.gxcommunication.sociallearn.modules.member_activity.adapter.MemberActiListAdapter;
import com.gxcommunication.sociallearn.utils.IntentUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadmoreListener;

import butterknife.BindView;

/**
 * @author hxc
 *
 *  线下活动
 */
public class MyMemberActiActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;

	MemberActiListAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_smartrefresh_material;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        setHeadActionTitle(" 我的活动");
		mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		mRefreshLayout.setOnRefreshLoadmoreListener(new OnRefreshLoadmoreListener() {
			@Override
			public void onLoadmore(RefreshLayout refreshlayout) {

			}

			@Override
			public void onRefresh(RefreshLayout refreshlayout) {

			}
		});

		mAdapter = new MemberActiListAdapter(this, DateCacheUtil.getRequestList(6));

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivity(MyMemberActiActivity.this,MemberActDetailsActivity.class);
            }
        });

		mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }



}
