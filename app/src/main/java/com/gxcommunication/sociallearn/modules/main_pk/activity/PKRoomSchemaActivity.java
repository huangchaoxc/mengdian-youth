package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.os.Bundle;
import android.view.View;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * PK  竞技场 模式选择
 */
public class PKRoomSchemaActivity extends BaseActivity {


    @Override
    protected int getLayoutId() {
        return R.layout.activity_pk_room_schema;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("PK 竞技场");


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    @OnClick({R.id.layout_px_solo, R.id.layout_px_team})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layout_px_solo:
                IntentUtils.startActivity(this,PKSoloRemindActivity.class);
                break;
            case R.id.layout_px_team:
                IntentUtils.startActivity(this,PKTeamRemindActivity.class);
                break;
        }
    }
}
