package com.gxcommunication.sociallearn.modules.index_studyforum.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyAnswerAdapter;
import com.gxcommunication.sociallearn.modules.index_studyforum.adapter.StudyShareAdapter;
import com.gxcommunication.sociallearn.utils.GsonUtlils;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @deprecated  废弃
 * @author hxc
 * 学创论坛 子栏目  你问我答 和 经验分享   下 我的收藏和我的问题
 */
public class MyStudyCollectAndIssuActivity extends BaseActivity {

    @BindView(R.id.my_study_collect)
    TextView myStudyCollect;
    @BindView(R.id.my_study_issue)
    TextView myStudyIssue;
    @BindView(R.id.study_recyclerview)
    RecyclerView studyRecyclerview;

	StudyAnswerAdapter mAdapter;
    StudyShareAdapter mShareAdapter;

	@Override
    protected int getLayoutId() {
        return R.layout.activity_my_study_collect_issue;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		head_action_title.setText("学创论坛收藏");
        myStudyIssue.setText("经验分享");
		mAdapter = new StudyAnswerAdapter(this, DateCacheUtil.getStudyAnswerDetail());
		studyRecyclerview .setLayoutManager(new LinearLayoutManager(this));
		studyRecyclerview.setAdapter(mAdapter);

        mShareAdapter = new StudyShareAdapter(this, DateCacheUtil.getMyStudyShareList(this));

        mShareAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivityForString(MyStudyCollectAndIssuActivity.this, StudyShareDetailActivity.class,StudyShareDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mShareAdapter.getItem(position)));
            }
        });

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                IntentUtils.startActivityForString(MyStudyCollectAndIssuActivity.this,StudyAnswerDetailActivity.class,StudyAnswerDetailActivity.INTENT_KEY_DETAIL_INFO,
                        GsonUtlils.objectToJson(mAdapter.getItem(position)));
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.my_study_collect, R.id.my_study_issue})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.my_study_collect:
				myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_head));
				myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
                studyRecyclerview.setAdapter(mAdapter);
                break;
            case R.id.my_study_issue:
				myStudyCollect.setBackgroundColor(getResources().getColor(R.color.color_transparent_head));
				myStudyIssue.setBackgroundColor(getResources().getColor(R.color.color_head));
                studyRecyclerview.setAdapter(mShareAdapter);
                break;
        }
    }
}
