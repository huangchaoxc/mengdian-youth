package com.gxcommunication.sociallearn.modules.member_activity.adapter;

import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;

import butterknife.BindView;


/**
 * Created by hxc on 2017/8/7
 */

public class MemberActiListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {




    public MemberActiListAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_member_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		TextView actTitle = helper.getView(R.id.act_title);
		TextView actNum= helper.getView(R.id.act_num);
		TextView actCrateTime= helper.getView(R.id.act_crate_time);
		TextView actTime= helper.getView(R.id.act_time);
		TextView actLocation= helper.getView(R.id.act_location);
		TextView actContent= helper.getView(R.id.act_content);
		TextView actStatus= helper.getView(R.id.act_status);
		Button btnShowDetatis= helper.getView(R.id.btn_show_detatis);

		actTitle.setText(getFormat(R.string.member_act_num_format, "23人"));

        //TODO 渲染文本
        //actCreateTimeText.setText();

        //点击跳转详情
		btnShowDetatis.setOnClickListener(view -> {

        });
    }

	/**
	 * 获取格式化内容
	 * @param id
	 * @param formats
	 * @return
	 */
    private String getFormat(int id,Object ...formats){
		return mContext.getResources().getString(id, formats);
	}
}
