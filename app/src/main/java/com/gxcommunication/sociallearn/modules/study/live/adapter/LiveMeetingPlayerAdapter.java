package com.gxcommunication.sociallearn.modules.study.live.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;
import com.tencent.rtmp.ui.TXCloudVideoView;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class LiveMeetingPlayerAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public LiveMeetingPlayerAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_live_meeting_player_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {
        ImageView userHead = helper.getView(R.id.item_live_metting_iv);
        TXCloudVideoView videoView = helper.getView(R.id.item_live_metting_video);
        helper.setText(R.id.item_live_metting_name_tv,"用户"  + helper.getAdapterPosition());

    }


}
