package com.gxcommunication.sociallearn.modules.study.live.activity;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.modules.study.live.adapter.LiveVoteResultDetailAdapter;
import com.gxcommunication.sociallearn.modules.study.live.bean.LiveVoteCreateAndDetailBean;
import com.gxcommunication.sociallearn.utils.GsonUtlils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by hxc on 2017/9/20.
 */

public class LiveVoteResultActivity extends BaseActivity {
    public static final String INTENT_KEY_CONTENT = "vote_content";//


    @BindView(R.id.live_vote_detail_title_tv)
    TextView liveVoteDetailTitleTv;
    @BindView(R.id.live_vote_detail_type_tv)
    TextView liveVoteDetailTypeTv;
    @BindView(R.id.live_vote_detail_status_tv)
    TextView liveVoteDetailStatusTv;
    @BindView(R.id.live_vote_detail_remark_tv)
    TextView liveVoteDetailRemarkTv;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.action_layout)
    LinearLayout actionLayout;

    LiveVoteCreateAndDetailBean mLiveVoteCreateBean = new LiveVoteCreateAndDetailBean();
    LiveVoteResultDetailAdapter mAdapter;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_live_vote_result_detail;
    }

    @Override
    protected void init() {

        super.initHeadActionBar();
        setHeadActionTitle("投票结果");
        if (!isEmptyStringIntent(INTENT_KEY_CONTENT)) {
            mLiveVoteCreateBean = GsonUtlils.jsonToBean(getIntentStringValue(INTENT_KEY_CONTENT), LiveVoteCreateAndDetailBean.class);
            loadVoteInfo();
        }
    }

    private void loadVoteInfo() {
        liveVoteDetailTitleTv.setText(mLiveVoteCreateBean.title);
        liveVoteDetailRemarkTv.setText(mLiveVoteCreateBean.content);

        if (mLiveVoteCreateBean.voteModeCode == 0) {
            liveVoteDetailTypeTv.setText("单选题");
        } else {
            liveVoteDetailTypeTv.setText("多选题");
        }

        if (mLiveVoteCreateBean.statusCode == 0) {
            liveVoteDetailStatusTv.setText("未完成");
        } else {
            liveVoteDetailStatusTv.setText("已结束");
        }

        mAdapter = new LiveVoteResultDetailAdapter(this, mLiveVoteCreateBean.data);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(mAdapter);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_delete_btn)
    public void onViewClicked() {
    }
}
