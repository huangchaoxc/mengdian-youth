package com.gxcommunication.sociallearn.modules.selfevaluate.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.TopicSearchListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 */
public class TopicSearchListActivity extends BaseActivity {

    public final static String INTENT_KEY_TYPE = "topic_type";// 判断进入选择的是什么题型

    @BindView(R.id.editSearch)
    EditText editSearch;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout refreshLayout;

    TopicSearchListAdapter mAdapter;
    int topicType = StudyTopicMultiItemBean.Topic_Radio_Type;//默认是单选题

    @Override
    protected int getLayoutId() {
        return R.layout.activity_topict_search_lis;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("搜题");

        topicType = getIntent().getIntExtra(INTENT_KEY_TYPE, StudyTopicMultiItemBean.Topic_Radio_Type);
        if (topicType == StudyTopicMultiItemBean.Topic_Radio_Type) {
            mAdapter = new TopicSearchListAdapter(this, R.layout.item_study_topic_radio_activity, DateCacheUtil.getRequestList(9));
        } else if (topicType == StudyTopicMultiItemBean.Topic_Multiple_Type) {
            mAdapter = new TopicSearchListAdapter(this, R.layout.item_study_topic_multiple_activity, DateCacheUtil.getRequestList(9));
        } else if (topicType == StudyTopicMultiItemBean.Topic_Judge_Type) {
            mAdapter = new TopicSearchListAdapter(this, R.layout.item_study_topic_judge_activity, DateCacheUtil.getRequestList(9));
        } else if (topicType == StudyTopicMultiItemBean.Topic_Compute_Type) {
            mAdapter = new TopicSearchListAdapter(this, R.layout.item_study_topic_radio_activity, DateCacheUtil.getRequestList(9));
        }

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick(R.id.action_btn)
    public void onViewClicked() {//搜索


    }
}
