package com.gxcommunication.sociallearn.modules.study.live.bean;

import java.util.ArrayList;
import java.util.List;

public class LiveVoteCreateAndDetailBean {


    public String voteId;
    public String userId;
    public String title;
    public String content;
    public int voteModeCode;//投票模式 0 单选 1 多选
    public String createTime;
    public int statusCode;//0 未完成 1 已完成

    public List<DataBean> data = new ArrayList<>();


    public static class DataBean{

        public DataBean(String voteContent) {
            this.voteContent = voteContent;
        }

        public int voteModeCode;//投票模式 0 单选 1 多选
        public String voteContent;

        public int selectMyCount;//投了此选项的票数

        public int totlaCount;// 总票数
    }

}
