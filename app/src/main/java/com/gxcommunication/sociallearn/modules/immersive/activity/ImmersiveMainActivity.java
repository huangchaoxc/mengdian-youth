package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 身临其境(页面主入口)
 */
public class ImmersiveMainActivity extends BaseActivity {

    @BindView(R.id.btn_mnsx)
    Button btnMnsx;// 模拟实训按钮
    @BindView(R.id.btn_alfxsx)
    Button btnAlfxsx;//案例分析按钮

    @Override
    protected int getLayoutId() {
        return R.layout.activity_immersive_main;
    }
    @Override
    protected void init() {
        super.initHeadActionBar();

        setHeadActionTitle("身临其境");

    }
    
    @OnClick({R.id.btn_mnsx, R.id.btn_alfxsx})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_mnsx:
                IntentUtils.startActivity(this,ScenarioSimulationListActivity.class);
                break;
            case R.id.btn_alfxsx:
                IntentUtils.startActivity(this,CaseAnalyseListActivity.class);
                break;
        }
    }
}