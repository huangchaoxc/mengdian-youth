package com.gxcommunication.sociallearn.modules.main_teacher.activity;

import android.os.Bundle;
import android.view.View;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * 拜师 名师堂
 */
public class ApplyTeacherActivity extends BaseActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_apply_teacher;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
		super.initHeadActionBar();
		setHeadActionTitle(" 拜师");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }



    @OnClick({R.id.apply_teacher_to_teacher, R.id.apply_teacher_layout_zy, R.id.apply_teacher_layout_zz, R.id.apply_teacher_layout_sh})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.apply_teacher_to_teacher://向老师报道
                IntentUtils.startActivity(this,CheckInTeacherActivity.class);
                break;
            case R.id.apply_teacher_layout_zy://专业技能

                break;
            case R.id.apply_teacher_layout_zz://思想政治

                break;
            case R.id.apply_teacher_layout_sh://生活情感

                break;
        }
    }
}
