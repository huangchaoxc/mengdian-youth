package com.gxcommunication.sociallearn.modules.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.gxcommunication.sociallearn.R
import com.gxcommunication.sociallearn.base.BaseActivity
import com.gxcommunication.sociallearn.modules.MainTestFragment
import com.gxcommunication.sociallearn.modules.main_index.MainIndexFragment
import com.gxcommunication.sociallearn.modules.main_message.MainMessageFragment
import com.gxcommunication.sociallearn.modules.main_my.MainMineFragment
import com.gxcommunication.sociallearn.modules.member_activity.MainActiFragment
import com.gxcommunication.sociallearn.utils.LogHelper
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private val fragments = arrayListOf<Fragment>()

    private var lastPosition: Int = 0
    private var mPosition: Int = 0
    private var currentFragment: Fragment? = null//要显示的Fragment
    private var hideFragment: Fragment? = null//要隐藏的Fragment

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("last_position", lastPosition)//activity重建时保存页面的位置
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        lastPosition = savedInstanceState.getInt("last_position")//获取重建时的fragment的位置
        setSelectedFragment(lastPosition)
        bottom_nav_view.selectedItemId = bottom_nav_view.menu.getItem(lastPosition).itemId

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            //根据传入的Bundle对象判断是正常启动还是重建 true表示正常启动，false表示重建
            setSelectedFragment(0)
        }
    }

    override fun getLayoutId(): Int =
        R.layout.activity_main


    override fun init() {

        fragments.apply {
            add(MainIndexFragment())
            add(MainTestFragment())
            add(MainActiFragment())
            add(MainMessageFragment())
            add(MainMineFragment())
        }

        //BottomNavigationView 点击事件监听
        bottom_nav_view.setOnNavigationItemSelectedListener { menuItem ->

//            if (CommonSharePreferenceUtils.getUserToken().isNullOrBlank()){
//                ARouter.getInstance().build(UserARouters.PATH_LOGIN).navigation()
//                false
//            }

            // 跳转指定页面：Fragment
            when (menuItem.itemId) {
                R.id.navigation_home -> setSelectedFragment(0)
                R.id.navigation_shop -> setSelectedFragment(1)
                R.id.navigation_product -> setSelectedFragment(2)
                R.id.navigation_car -> setSelectedFragment(3)
                R.id.navigation_mine -> {
//                    if (CommonSharePreferenceUtils.getUserToken().isNullOrBlank()){
//
//                        false
//                    }else{
//                        setSelectedFragment(4)
//                    }

                    setSelectedFragment(4)
                }
            }

            true
        }


    }



    override fun onClick(v: View?) {
        super.onClick(v)
    }

    /**
     * 根据位置选择Fragment
     * @param position 要选中的fragment的位置
     */
    private fun setSelectedFragment(position: Int) {
        LogHelper.e("------------setSelectedFragment--------------")
        bottom_nav_view.menu.getItem(position).isChecked = true
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        currentFragment =
            fragmentManager.findFragmentByTag("fragment$position")//要显示的fragment(解决了activity重建时新建实例的问题)
        hideFragment =
            fragmentManager.findFragmentByTag("fragment$lastPosition")//要隐藏的fragment(解决了activity重建时新建实例的问题)
        if (position != lastPosition) {//如果位置不同
            //如果要隐藏的fragment存在，则隐藏
            hideFragment?.let { transaction.hide(it) }
            if (currentFragment == null) {//如果要显示的fragment不存在，则新加并提交事务
                currentFragment = fragments[position]
                currentFragment?.let { transaction.add(R.id.fl_container, it, "fragment$position") }
            } else {//如果要显示的存在则直接显示
                currentFragment?.let { transaction.show(it) }
            }
        }else

            if (position == lastPosition) {//如果位置相同
                if (currentFragment == null) {//如果fragment不存在(第一次启动应用的时候) //遇到个问题第一次页面会加载两次
                    currentFragment = fragments[position]
                    currentFragment?.let { transaction.add(R.id.fl_container, it, "fragment$position") }
                }//如果位置相同，且fragment存在，则不作任何操作
            }

        transaction.commit()//提交事务
        lastPosition = position//更新要隐藏的fragment的位置
        mPosition = position
    }
}