package com.gxcommunication.sociallearn.modules.member_activity.activity;

import android.Manifest;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 *  线下活动 签到
 */
public class MemberActSignActivity extends BaseActivity {


    @BindView(R.id.act_time)
    TextView actTime;//活动时间
    @BindView(R.id.location_tip)
    TextView locationTip; //签到提示

    LocationClient locationClient;
//    LatLng memberActiLatlng  = new LatLng(108.386523,22.815198);//服务端获取到的活动的坐标 默认南宁国际会展中心
    LatLng memberActiLatlng  = new LatLng(22.814342,108.400568);//服务端获取到的活动的坐标 默认龙光

    private boolean isArriveDestination = false;// 是否到达目的地
    private int distanceDestination = 300;// 距离目的地的距离

    @Override
    protected int getLayoutId() {
        return R.layout.activity_member_act_sign;
    }

    @Override
    protected void init() {
        super.initHeadActionBar();
        setHeadActionTitle(" 线下活动签到");

        getLocationPermission();
    }



    @OnClick(R.id.btn_sign)
    public void onViewClicked(View view) {
        //签到点击响应时间
        if (isArriveDestination){// 到达签到地点，点击发送签到操作

        }else {//未到达签到地点，点击提示未到达
            showToast("您未到达签到地点，请前往目的地进行签到");
        }
    }


    /**
     * 初始化定位参数配置
     */

    private void initLocationOption() {
        //定位服务的客户端。宿主程序在客户端声明此类，并调用，目前只支持在主线程中启动
        locationClient = new LocationClient(this);
        //声明LocationClient类实例并配置定位参数
        LocationClientOption locationOption = new LocationClientOption();
        MyLocationListener myLocationListener = new MyLocationListener();
        //注册监听函数
        locationClient.registerLocationListener(myLocationListener);
        //可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        locationOption.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
        //可选，默认gcj02，设置返回的定位结果坐标系，如果配合百度地图使用，建议设置为bd09ll;
        locationOption.setCoorType("bd09ll");
        //可选，默认0，即仅定位一次，设置发起连续定位请求的间隔需要大于等于1000ms才是有效的
        locationOption.setScanSpan(5000);
        //可选，设置是否需要地址信息，默认不需要
        locationOption.setIsNeedAddress(true);
        //可选，设置是否需要地址描述
        locationOption.setIsNeedLocationDescribe(true);
        //可选，设置是否需要设备方向结果
        locationOption.setNeedDeviceDirect(false);
        //可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        locationOption.setLocationNotify(true);
        //可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        locationOption.setIgnoreKillProcess(true);
        //可选，默认false，设置是否需要位置语义化结果，可以在BDLocation.getLocationDescribe里得到，结果类似于“在北京天安门附近”
        locationOption.setIsNeedLocationDescribe(true);
        //可选，默认false，设置是否需要POI结果，可以在BDLocation.getPoiList里得到
        locationOption.setIsNeedLocationPoiList(true);
        //可选，默认false，设置是否收集CRASH信息，默认收集
        locationOption.SetIgnoreCacheException(false);
        //可选，默认false，设置是否开启Gps定位
        locationOption.setOpenGps(true);
        //可选，默认false，设置定位时是否需要海拔信息，默认不需要，除基础定位版本都可用
        locationOption.setIsNeedAltitude(false);
        //设置打开自动回调位置模式，该开关打开后，期间只要定位SDK检测到位置变化就会主动回调给开发者，该模式下开发者无需再关心定位间隔是多少，定位SDK本身发现位置变化就会及时回调给开发者
        locationOption.setOpenAutoNotifyMode();
        //设置打开自动回调位置模式，该开关打开后，期间只要定位SDK检测到位置变化就会主动回调给开发者
        locationOption.setOpenAutoNotifyMode(3000,1, LocationClientOption.LOC_SENSITIVITY_HIGHT);
        //需将配置好的LocationClientOption对象，通过setLocOption方法传递给LocationClient对象使用
        locationClient.setLocOption(locationOption);
        //开始定位
        locationClient.start();


    }
    /**
     * 实现定位回调
     */
    public class MyLocationListener extends BDAbstractLocationListener {
        @Override
        public void onReceiveLocation(BDLocation location){
            //此处的BDLocation为定位结果信息类，通过它的各种get方法可获取定位相关的全部结果
            //以下只列举部分获取经纬度相关（常用）的结果信息
            //更多结果信息获取说明，请参照类参考中BDLocation类中的说明

            //获取纬度信息
            double latitude = location.getLatitude();
            //获取经度信息
            double longitude = location.getLongitude();
            //获取定位精度，默认值为0.0f
            float radius = location.getRadius();
            //获取经纬度坐标类型，以LocationClientOption中设置过的坐标类型为准
            String coorType = location.getCoorType();
            //获取定位类型、定位错误返回码，具体信息可参照类参考中BDLocation类中的说明
            int errorCode = location.getLocType();
            if (errorCode == 62 || errorCode == 167){
                showToast("定位失败");
                return;
            }

            LatLng myLatlng = new LatLng(latitude,longitude);
            double distance = DistanceUtil.getDistance(memberActiLatlng, myLatlng);//测量距离，计算目的地和当前位置的距离
            LogHelper.e(location.getAddrStr());
            LogHelper.e("" + latitude);
            LogHelper.e("" + longitude);

            if (distance < distanceDestination){
                LogHelper.e("--------- 您已到达签到地点");
                isArriveDestination = true;
                locationTip.setText("您已到达签到地点,可以进行活动签到！");
            }else {
                LogHelper.e("--------- 您未到达签到地点");
                isArriveDestination = false;
                locationTip.setText("您未到达签到地点,距离目的地" + distance + " 米");
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (locationClient != null && locationClient.isStarted()){
            locationClient.stop();
        }

    }

    private void getLocationPermission() {

        Acp.getInstance(this).request(new AcpOptions.Builder().setPermissions(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        ).build(), new AcpListener() {
            @Override
            public void onGranted() {

                initLocationOption();

            }

            @Override
            public void onDenied(List<String> permissions) {

            }
        });
    }
}