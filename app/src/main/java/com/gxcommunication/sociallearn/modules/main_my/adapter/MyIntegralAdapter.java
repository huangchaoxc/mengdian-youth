package com.gxcommunication.sociallearn.modules.main_my.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 *
 */

public class MyIntegralAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

	public MyIntegralAdapter(Context context, List<RequestResBean> data) {
		super(R.layout.item_my_integral, data);
		mContext = context;
	}

	@Override
	protected void convert(BaseViewHolder helper, RequestResBean item) {
//		helper.setText(R.id.tvTitle,"");
	}
}
