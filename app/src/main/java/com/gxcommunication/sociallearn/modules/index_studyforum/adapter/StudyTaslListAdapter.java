package com.gxcommunication.sociallearn.modules.index_studyforum.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.bean.RequestResBean;

import java.util.List;


/**
 * Created by hxc on 2017/8/7
 */

public class StudyTaslListAdapter extends BaseQuickAdapter<RequestResBean, BaseViewHolder> {

    public StudyTaslListAdapter(Context context, List<RequestResBean> data) {
        super(R.layout.item_teacher_task_activity, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RequestResBean item) {

		helper.setText(R.id.item_teacher_task_title_tv,"系统消息").
		setText(R.id.item_teacher_task_content_tv,"系统刚刚发布了一条消息。。。。");

        helper.setText(R.id.teacher_task_action_btn,"前往");

        switch (helper.getAdapterPosition()){
            case 0:
                helper.setText(R.id.item_teacher_task_title_tv,"签到").
                        setText(R.id.item_teacher_task_content_tv,"每日向老师报道");
                break;

            case 1:
                helper.setText(R.id.item_teacher_task_title_tv,"创新展览馆").
                        setText(R.id.item_teacher_task_content_tv,"浏览创新成果5分钟");
                break;

            case 2:
                helper.setText(R.id.item_teacher_task_title_tv,"视频课堂").
                        setText(R.id.item_teacher_task_content_tv,"浏览视频课堂1节课");
                break;
        }
    }


}
