package com.gxcommunication.sociallearn.modules.main_teacher.activity;

import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.main_teacher.adapter.TeacherTaslListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 老师主页  领取任务
 */
public class TeacherTaskActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    TeacherTaslListAdapter mAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_teacher_task;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        initHeadActionBar();
        setHeadActionTitle("领取任务");
        mAdapter = new TeacherTaslListAdapter(this, DateCacheUtil.getRequestList(10));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.teacher_task_1_btn, R.id.teacher_task_2_btn, R.id.teacher_task_3_btn, R.id.teacher_task_4_btn, R.id.teacher_task_5_btn, R.id.teacher_task_6_btn, R.id.teacher_task_7_btn, R.id.teacher_task_8_btn, R.id.teacher_task_9_btn, R.id.action_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.teacher_task_1_btn:

                break;
            case R.id.teacher_task_2_btn:

                break;
            case R.id.teacher_task_3_btn:

                break;
            case R.id.teacher_task_4_btn:

                break;
            case R.id.teacher_task_5_btn:

                break;
            case R.id.teacher_task_6_btn:

                break;
            case R.id.teacher_task_7_btn:

                break;
            case R.id.teacher_task_8_btn:

                break;
            case R.id.teacher_task_9_btn:

                break;
            case R.id.action_btn:
                break;
        }
    }
}
