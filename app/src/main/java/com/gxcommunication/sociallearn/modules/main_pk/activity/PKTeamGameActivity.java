package com.gxcommunication.sociallearn.modules.main_pk.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.example.DateCacheUtil;
import com.gxcommunication.sociallearn.modules.main_pk.adapter.PKTeamGameMemberAdapter;
import com.gxcommunication.sociallearn.modules.main_pk.adapter.PKTeamMatchMemberAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.adapter.StudyTopicListAdapter;
import com.gxcommunication.sociallearn.modules.selfevaluate.bean.StudyTopicMultiItemBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author hxc
 * PK  团体赛  团队PK
 */
public class PKTeamGameActivity extends BaseActivity {


    @BindView(R.id.myRecyclerView)
    RecyclerView myRecyclerView;
    @BindView(R.id.pk_team_my_tips_tv)
    TextView pkTeamMyTipsTv;
    @BindView(R.id.pk_team_my_name_tv)
    TextView pkTeamMyNameTv;
    @BindView(R.id.pk_team_enemy_tips_tv)
    TextView pkTeamEnemyTipsTv;
    @BindView(R.id.enemyRecyclerView)
    RecyclerView enemyRecyclerView;
    @BindView(R.id.pk_team_enemy_name_tv)
    TextView pkTeamEnemyNameTv;
    @BindView(R.id.topicRecyclerView)
    RecyclerView topicRecyclerView;

    PKTeamGameMemberAdapter memberAdapter;//我的队员
    PKTeamGameMemberAdapter memberOtherAdapter;//敌方队员

    StudyTopicListAdapter mAdapter;


    @Override
    protected int getLayoutId() {
        return R.layout.activity_pk_team_game;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("团队PK");

        memberAdapter = new PKTeamGameMemberAdapter(this,R.layout.item_pk_team_member_layout, DateCacheUtil.getRequestList(9));
        memberOtherAdapter = new PKTeamGameMemberAdapter(this,R.layout.item_pk_team_enemy_member_layout, DateCacheUtil.getRequestList(9));

        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter(memberAdapter);

        enemyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        enemyRecyclerView.setAdapter(memberOtherAdapter);


        mAdapter = new StudyTopicListAdapter(this, loadTempDate());
        topicRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        topicRecyclerView.setAdapter(mAdapter);
        new PagerSnapHelper().attachToRecyclerView(topicRecyclerView);
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                RecyclerView.LayoutManager layoutManager = topicRecyclerView.getLayoutManager();
                int firs = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                if (view.getId() == R.id.item_topic_next_iv) {
                    topicRecyclerView.scrollToPosition(firs + 1);

                }
            }
        });


    }

    private List<StudyTopicMultiItemBean> loadTempDate() {

        List<StudyTopicMultiItemBean> list = new ArrayList<>();
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Radio_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Multiple_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Judge_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));
        list.add(new StudyTopicMultiItemBean(StudyTopicMultiItemBean.Topic_Compute_Type));

        return list;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }


    private void showDialog() {

        View view = LayoutInflater.from(this).inflate(R.layout.dialog_pk_solo_result_layout, null);
        AlertDialog builder = new AlertDialog.Builder(this, R.style.NobackDialog).create();
        builder.setView(view);
        builder.setCancelable(true);
        view.findViewById(R.id.exam_remind_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
                finish();
            }
        });
        builder.show();
    }

}
