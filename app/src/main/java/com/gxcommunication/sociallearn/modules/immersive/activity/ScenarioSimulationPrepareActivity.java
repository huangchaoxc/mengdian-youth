package com.gxcommunication.sociallearn.modules.immersive.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gxcommunication.sociallearn.R;
import com.gxcommunication.sociallearn.base.BaseActivity;
import com.gxcommunication.sociallearn.utils.IntentUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author hxc
 * <p>
 * 场景模拟列 准备工作
 */
public class ScenarioSimulationPrepareActivity extends BaseActivity {

    @BindView(R.id.prepare_category_tv)
    TextView prepareCategoryTv;
    @BindView(R.id.prepare_add_principal_tv)
    TextView prepareAddPrincipalTv;
    @BindView(R.id.prepare_principal_head_iv)
    ImageView preparePrincipalHeadIv;
    @BindView(R.id.prepare_principal_nickname_tv)
    TextView preparePrincipalNicknameTv;
    @BindView(R.id.user_info_position_tv)
    TextView userInfoPositionTv;
    @BindView(R.id.prepare_add_tour_tv)
    TextView prepareAddTourTv;
    @BindView(R.id.prepare_tour_01_head_iv)
    ImageView prepareTour01HeadIv;
    @BindView(R.id.prepare_tour_01_nickname_tv)
    TextView prepareTour01NicknameTv;
    @BindView(R.id.prepare_tour_01_position_tv)
    TextView prepareTour01PositionTv;
    @BindView(R.id.prepare_tour_02_head_iv)
    ImageView prepareTour02HeadIv;
    @BindView(R.id.prepare_tour_02_nickname_tv)
    TextView prepareTour02NicknameTv;
    @BindView(R.id.prepare_tour_02_position_tv)
    TextView prepareTour02PositionTv;
    @BindView(R.id.user_info_sure_tv)
    TextView userInfoSureTv;
    @BindView(R.id.head_action_backimage)
    ImageView headActionBackimage;
    @BindView(R.id.head_action_title)
    TextView headActionTitle;
    @BindView(R.id.head_action_left_progressBar)
    ProgressBar headActionLeftProgressBar;
    @BindView(R.id.head_action_rel)
    RelativeLayout headActionRel;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_scenario_simulation_prepare;
    }

    @Override
    protected void init() {
        initView();
    }


    public void initView() {
        super.initHeadActionBar();
        head_action_title.setText("准备工作");

        userInfoSureTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentUtils.startActivity(ScenarioSimulationPrepareActivity.this, ScenarioSimulationMeterToolsActivity.class);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            default:
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    @OnClick({R.id.prepare_add_principal_tv, R.id.prepare_add_tour_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.prepare_add_principal_tv:
                showToast("添加值班负责人");
                break;
            case R.id.prepare_add_tour_tv:
                showToast("添加巡视人");
                break;
        }
    }
}
