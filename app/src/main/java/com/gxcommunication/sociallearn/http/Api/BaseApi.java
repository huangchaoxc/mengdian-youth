package com.gxcommunication.sociallearn.http.Api;


import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface BaseApi {

    /**
     *  首页弹出框粘贴板解析
     */
    String HOME_CLIPBOARD = "indexShow/convertClipboardByTpwd";

    //弹出框粘贴板解析淘口令（返商品信息）
    String HOME_PRODUCT_CLIPBOARD = "indexShow/analysisTbPwdByClipboard";


   // 根据原文案转口令接口
    String CONVERT_PRODUCT_URL =  "GoodsDetailed/getWordByNewTPwd";



    @GET
    @Streaming
    Observable<ResponseBody> downloadImg(@Url String imgUrl);
}
