package com.gxcommunication.sociallearn.http.service;


import com.gxcommunication.sociallearn.app.MyApplication;
import com.gxcommunication.sociallearn.utils.NetUtil;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Observer基类
 * 
 * @param <T>
 */
public abstract class HttpStringObserver<T> implements Observer<T > {

    /**
     * 可以统一处理返回值及相关操作
     *
     * @param result
     */
    @Override
    public  void onNext(T result){


    }


    /**
     * 请求成功回调
     *
     * @param result 服务端返回的数据
     */
    public abstract void onSuccess(T result);

    /**
     * 请求失败的回调 (非 200的情况)，开发者手动去触发
     *
     * @param moreInfo 错误信息数据
     */
    public void onFailed(int errorCode, String moreInfo) {
    }


    @Override
    public void onComplete() {

    }

    @Override
    public void onSubscribe(Disposable d) {
        if (NetUtil.isNetConnected(MyApplication.getInstance())){

        }else {
            //没有网络的时候，调用error方法，并且切断与上游的联系
            if (!d.isDisposed()) {
                d.dispose();
                onFailed(0, "无网络");
                return;
            }
        }
    }

    /**
     * 可以统一处理请求失败的错误
     *
     * @param e
     */
    @Override
    public abstract void onError(Throwable e);



}

