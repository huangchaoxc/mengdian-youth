package com.gxcommunication.sociallearn.http.Api;//package com.hxc.toolslibrary.http.Api;
//
//
//import com.hxc.toolslibrary.base.bean.RequestResBean;
//
//public interface StoreApi {
//
//
//
//    /**
//     *  获取菜单类型 比如 早餐、中餐、午餐
//     */
//    @GET("android/foodMenuSetting/getMenuName")
//    Observable<FoodMenuBean> getMenuName();
//
//
//    /**
//     *  根据菜单类型获取 菜品列表
//     */
//    @FormUrlEncoded
//    @POST("android/foodMenuSetting/getFoodByMenuId")
//    Observable<FoodAllListBean> getFoodByMenuId(@Field("menu_id") String menu_id);
//
//
//    /**
//     *  店铺 菜品类型
//     */
//    @GET("android/foodType/getList")
//    Observable<FoodTypeBean> getFoodType();
//
//    /**
//     *  店铺 菜品列表
//     */
//    @GET("android/food/getList")
//    Observable<FoodAllListBean> getFoodAllList();
//
//    /**
//     *  店铺 评论列表
//     */
//    @GET("android/evaluate/getList")
//    Observable<StoreCommentListBean> getStoreCommentList(@Query("page") String page);
//
//
//    /**
//     *  店铺 商家回复消费者的评论
//     */
//    @FormUrlEncoded
//    @POST("android/evaluateReply/save")
//    Observable<RequestResBean> evaluateReply(@Field("uid") String uid, @Field("evaluate_id") String evaluate_id, @Field("reply_content") String reply_content);
//
//    /**
//     *  店铺 商家删除评论回复
//     */
//    @FormUrlEncoded
//    @POST("android/evaluateReply/delete")
//    Observable<RequestResBean> deleteReply(@Field("uid") String uid, @Field("id") String id);
//
//    /**
//     *  店铺 评分
//     */
//    @GET("android/evaluate/shopEvaluateScore")
//    Observable<StoreEvaluateScore> getStoreEvaluateScore();
//
//
//    /**
//     *  下单
//     */
//    @FormUrlEncoded
//    @POST("android/order/save")
//    Observable<RequestResBean> sendOrder(@Field("uid") String uid, @Field("food_list") String foodList);
//
//}
