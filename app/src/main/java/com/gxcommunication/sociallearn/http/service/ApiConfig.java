package com.gxcommunication.sociallearn.http.service;


/**
 * Created by JinXinYi on 2018/1/7.
 */

public class ApiConfig {


    public static final String LONG_LONG_URL = "http://192.168.1.66:8103/";
    public static final String LONG_DALI_URL = "http://192.168.1.119:8103/";
    public static final String LONG_HUJIE_URL = "http://192.168.6.108:18080/";
    public static final String LONG_HD_URL = "http://192.168.1.175:8103/";
    public static final String LONG_URL_WEB_IP_HD = "http://119.45.227.100:18080";
    public static final String LONG_URL_WEB_IP = "http://112.74.185.208:18080";

    public static final String LONG_URL_WEB = "http://appserver.sqyd666.cn/";
    public static final String LONG_URL_WEB_1 = "http://appserver.sqyd666.cn";
    public static String BASE_URL = LONG_URL_WEB_IP;

    public static String BASE_IMAGE_URL = "http://qiniu.yuhuofei.it/";

    public static final int REQUEST_SUCCESS_CODE = 200;
    public static final int REQUEST_NO_DATA_CODE = 1;


//    /**
//     * 单个知识体系列表
//     */
//    @Headers({"baseUrl:normal"})
//    @GET("article/list/{page}/json")
//    Observable<BaseResp<KnowledgeClassifyListBean>> getKnowledgeClassifyList(@Path("page") int page, @Query("cid") int id);
//
//
//    /**
//     * 注册
//     */
//    @Headers({"baseUrl:normal"})
//    @POST("user/register")
//    @FormUrlEncoded
//    Observable<BaseResp<UserInfo>> register(@Field("username") String username, @Field("password") String password, @Field("repassword") String repassword);

}
