package com.gxcommunication.sociallearn.http.service;


import android.annotation.SuppressLint;
import android.app.Application;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gxcommunication.sociallearn.BuildConfig;
import com.gxcommunication.sociallearn.app.MyApplication;
import com.gxcommunication.sociallearn.http.Interceptor.AddCookiesInterceptor;
import com.gxcommunication.sociallearn.http.Interceptor.ReceivedCookiesInterceptor;
import com.gxcommunication.sociallearn.utils.LogHelper;
import com.hxc.toolslibrary.http.Interceptor.HeaderInterceptor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.nio.charset.StandardCharsets.UTF_8;


/**
 * Created by JinXinYi on 2018/1/7.
 */

public class ApiStore {

    private static Retrofit retrofit;
    private static String baseUrl = ApiConfig.BASE_URL;

    static {
        createProxy();
    }

    public static <T> T createApi(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public static void createProxy() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy.MM.dd HH:mm:ss").create();

        //设置缓存目录
        File cacheFile = new File(MyApplication.getInstance().getCacheDir(), "cache");
        Cache cache = new Cache(cacheFile, 1024 * 1024 * 100); //100Mb

        //配置OkHttpBuilder
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                })
                //信任证书
                // .sslSocketFactory(getSSLSocketFactory())
                //设置超时时间
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                // .addInterceptor(new BaseUrlInterceptor())
                .addInterceptor(new HttpLoggingInterceptor())
                .cache(cache)
                //错误重连
                .retryOnConnectionFailure(true)
                //(错误原因是验证证书时发现真正请求和服务器的证书域名不一致) 此代码为忽略hostname 的验证
                .hostnameVerifier((hostname, session) -> true)
                //Cookie 豆瓣API会无法调用,还没搞懂 使用下面的
                //.cookieJar(new CookiesManager())
                .addInterceptor(new LoggingInterceptor())
                .addInterceptor(new ReceivedCookiesInterceptor())
                .addInterceptor(new AddCookiesInterceptor())
                .addInterceptor(new HeaderInterceptor());


        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .client(builder.build())
                .build();
    }

    public static class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            //Chain 里包含了request和response
            Request request = chain.request();
            long t1 = System.nanoTime();//请求发起的时间
           // LogHelper.e(String.format("发送请求 %s on %s%n%s", request.url(), chain.connection(), request.headers()));
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();//收到响应的时间
            //不能直接使用response.body（）.string()的方式输出日志
            //因为response.body().string()之后，response中的流会被关闭，程序会报错，
            // 我们需要创建出一个新的response给应用层处理
            ResponseBody responseBody = response.peekBody(1024 * 1024);
            if (BuildConfig.DEBUG){
                LogHelper.e("-----------------------------------star-------------------------------------------");
                LogHelper.e(String.format("接收响应：[%s]   %n 请求耗时:%.1fms  %n返回json:%s %n%s",
                        response.request().url(),
                        (t2 - t1) / 1e6d,
                        responseBody.string(),
                        response.headers()
                ));

                printParams(request.body());
                LogHelper.e("-----------------------------------end-------------------------------------------%n");
            }

            return response;
        }
    }

    private static void printParams(RequestBody body) {
        Buffer buffer = new Buffer();
        try {
            body.writeTo(buffer);
            Charset charset = Charset.forName("UTF-8");
            MediaType contentType = body.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF_8);
            }
            String params = buffer.readString(charset);
            LogHelper.e("请求参数： | " + params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @SuppressLint("TrustAllX509TrustManager")
    private static SSLSocketFactory getSSLSocketFactory() {
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    try {
                        chain[0].checkValidity();
                    } catch (Exception e) {
                        throw new CertificateException("Certificate not valid or trusted.");
                    }
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[]{};
                }
            }};
            // Install the all-trusting trust manager
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            return null;
        }
    }

}
